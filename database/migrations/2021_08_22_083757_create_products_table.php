<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pro_name')->nullable();
            $table->string('pro_slug')->index();
            $table->integer('pro_category_id')->index()->default(0);
            $table->integer('pro_price')->default(0);
            $table->integer('pro_author_id')->default(0)->index();
            $table->tinyInteger('pro_sale')->default(0)->index()->comment('Giá sale');
            $table->tinyInteger('pro_status')->default(1)->index()->comment('Trạng thái')->comment('1: Kích hoạt');
            $table->tinyInteger('pro_hot')->default(0)->comment('Sản phẩm nổi bật')->comment('1: nổi bật');
            $table->tinyInteger('pro_inventory')->default(0)->comment('Tồn kho')->comment('1: Tồn kho');
            $table->tinyInteger('pro_out_stock')->default(0)->comment('Hết hàng')->comment('1: Hết hàng');
            $table->integer('pro_view')->default(0);
            $table->string('pro_description')->nullable();
            $table->string('pro_image')->nullable();
            $table->string('pro_description_seo')->nullable();
            $table->string('pro_keyword_seo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
