<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_name')->unique();
            $table->string('category_slug')->index();
            $table->char('category_icon')->nullable();
            $table->string('category_avatar')->nullable();
            $table->tinyInteger('status')->default(1)->index();
            $table->integer('category_total_product')->default(0);
            $table->string('category_title_seo')->nullable();
            $table->string('category_description_seo')->nullable();
            $table->string('category_keywork_seo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
