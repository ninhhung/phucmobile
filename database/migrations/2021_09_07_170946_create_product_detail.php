<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pro_de_parent_id');
            $table->string('pro_de_screen')->nullable()->comment('Màn hình');
            $table->string('pro_de_system')->nullable()->comment('Hệ điều hành');
            $table->string('pro_de_back_camera')->nullable()->comment('Camera sau');
            $table->string('pro_de_front_camera')->nullable()->comment('Camera trước');
            $table->string('pro_de_chip')->nullable()->comment('Chip');
            $table->string('pro_de_ram')->nullable()->comment('Ram');
            $table->string('pro_de_memory')->nullable()->comment('Bộ nhớ');
            $table->string('pro_de_sim')->nullable()->comment('Thẻ sim');
            $table->tinyInteger('pro_de_status')->default(1)->comment('Tình trạng')->comment('0: Cũ, 1: Hàng mới');
            $table->string('pro_de_input')->nullable()->comment('Đầu vào');
            $table->string('pro_de_output')->nullable()->comment('Đầu ra');
            $table->string('pro_de_long')->nullable()->comment('Độ dài');
            $table->json('pro_de_image_slide')->nullable()->comment('Hình ảnh sản phẩm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_detail');
    }
}
