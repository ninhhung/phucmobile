<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_cat_parent');
            $table->string('sub_cat_name')->unique();
            $table->tinyInteger('sub_cat_status')->default(1)->index();
            $table->string('sub_cat_title_seo')->nullable();
            $table->string('sub_cat_description_seo')->nullable();
            $table->string('sub_cat_keywork_seo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_category');
    }
}
