<footer class="footer">
    <section class="clearfix footer__top">
        <div class="footer__col">
            <ul class="f-listmenu">
                <li><a rel="nofollow" href="/lich-su-mua-hang">Lịch sử mua hàng</a></li>
                <li><a rel="nofollow" href="/bao-hanh">Chính sách bảo hành</a></li>
                <li><a href="javascript:void(0)" class="arrow showtaga">Xem thêm</a></li>
                <li class="hidden"><a rel="nofollow" href="/chinh-sach-bao-hanh-san-pham">Chính sách đổi trả</a></li>
                <li class="hidden"><a rel="nofollow" href="/giao-hang">Giao hàng & Thanh toán</a></li>
                <li class="hidden"><a rel="nofollow" href="/huong-dan-mua-hang">Hướng dẫn mua online</a></li>
                <li class="hidden"><a rel="nofollow" href="/noi-quy-cua-hang">Nội quy cửa hàng</a></li>
                <li class="hidden"><a rel="nofollow" href="/chat-luong-phuc-vu">Chất lượng phục vụ</a></li>
            </ul>
        </div>
        <div class="footer__col">
            <ul class="f-listmenu">
                <li><a rel="nofollow" href="http://mwg.vn">Giới thiệu cửa hàng</a></li>
                <li><a rel="nofollow" href="/lien-he">Gửi góp ý, khiếu nại</a></li>
                <li><a rel="nofollow" class="linkversion" href="http://www.Phucmobile.com/?sclient=mobile">Xem bản mobile</a></li>
            </ul>
        </div>
        <div class="footer__col">
            <div class="f-listtel">
                <p class="f-listtel__title">
                    <strong>Tổng đài hỗ trợ</strong>
                </p>
                <p class="f-listtel__content"><span>Gọi mua:</span> <a href="tel:18001060">1800.1060</a> (7:30 - 22:00)</p>
            </div>
        </div>
    </section>
    <div class="copyright">
        <section>
            <p>
                © 2021. Cửa hàng điện thoại Di Động Phức mobile - phucmobile.com
                Địa chỉ: thôn Thiện Đáp - xã Kim Xuyên - huyện Kim Thành - tỉnh Hải Dương. Điện thoại: 028 38125960. Email: cskh@Phucmobile.com.
            </p>
        </section>
    </div>
</footer>