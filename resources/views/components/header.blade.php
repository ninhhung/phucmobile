<header class="header  " data-sub="0">
    <div class="header__main">
        <div class="header_child">
            <div class="header-item-style">
                <a href="/" class="header__logo">
                    <img src="{{ asset('default-image/home-logo.svg') }}" alt="">
                </a>
            </div>
            <div class="header-item-style">
                <form action="/tim-kiem" onsubmit="return suggestSearch(event);" class="header__search">
                    <input id="skw" type="text" class="input-search" onkeyup="suggestSearch(event);" placeholder="Bạn tìm gì..." name="key" autocomplete="off" maxlength="100">
                    <button type="submit">
                        <i class="icon-search"></i>
                    </button>
                    <div id="search-result"></div>
                </form>
            </div>
            <div class="category-style">
                @if (isset($categories))
                @foreach ($categories as $category)
                <div class="header-item-child">
                    <a href="{{ route('get.list.product', [$category->category_slug, $category->id]) }}" class="header-menu-item menu-info">
                        <i class="{{ $category->category_icon }}"></i>
                        <span>{{ $category->category_name }}</span>
                    </a>
                </div>
                @endforeach
                @endif
            </div>
            @if (Auth::check())
            <div class="header-end-item">
                <div class="account-menu-item">
                    <a href="/cart" class="header__cart menu-info">
                        <div class="cart-float-right">
                            <i class="fa fa-user-circle-o"></i>
                            <span>{{ Auth::user()->name }}</span>
                        </div>
                    </a>
                </div>
                <div class="account-menu-item">
                    <a href="/cart" class="header__cart menu-info">
                        <div class="cart-float-right">
                            <i class="icon-cart"></i>
                            <span>Giỏ hàng</span>
                        </div>
                    </a>
                </div>
                <div class="account-menu-item">
                    <a href="{{ route('get.logout.user') }}" class="header__cart menu-info">
                        <div class="cart-float-right">
                            <i class="fa fa-sign-out"></i>
                            <span>Đăng xuất</span>
                        </div>
                    </a>
                </div>
            </div>
            @else
            <div class="header-end-login-logout">
                <div>
                    <i class="fa fa-sign-in"></i>
                    <a href="{{ route('get.register') }}">Đăng ký</a>
                </div>
                <div>
                    <i class="fa fa-user-circle"></i>
                    <a href="{{ route('get.login') }}">Đăng nhập</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</header>