<!DOCTYPE html>
<html lang="vi-VN">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('home/css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('home/css/mobile-detail.css') }}">
    <title>Phucmobile.com - &#x110;i&#x1EC7;n tho&#x1EA1;i, Laptop, Ph&#x1EE5; ki&#x1EC7;n, &#x110;&#x1ED3;ng h&#x1ED3; ch&#xED;nh h&#xE3;ng</title>
    <meta name="keywords" content="Th&#x1EBF; gi&#x1EDB;i di &#x111;&#x1ED9;ng, Thegioididong, &#x111;i&#x1EC7;n tho&#x1EA1;i di &#x111;&#x1ED9;ng, dtdd, smartphone, tablet, m&#xE1;y t&#xED;nh b&#x1EA3;ng, laptop, m&#xE1;y t&#xED;nh x&#xE1;ch tay, ph&#x1EE5; ki&#x1EC7;n, smartwatch, &#x111;&#x1ED3;ng h&#x1ED3;, tin c&#xF4;ng ngh&#x1EC7;" />
    <meta name="description" content="H&#x1EC7; th&#x1ED1;ng b&#xE1;n l&#x1EBB; &#x111;i&#x1EC7;n tho&#x1EA1;i di &#x111;&#x1ED9;ng, smartphone, m&#xE1;y t&#xED;nh b&#x1EA3;ng, tablet, laptop, ph&#x1EE5; ki&#x1EC7;n, smartwatch, &#x111;&#x1ED3;ng h&#x1ED3; ch&#xED;nh h&#xE3;ng m&#x1EDB;i nh&#x1EA5;t, gi&#xE1; t&#x1ED1;t, d&#x1ECB;ch v&#x1EE5; kh&#xE1;ch h&#xE0;ng &#x111;&#x1B0;&#x1EE3;c y&#xEA;u th&#xED;ch nh&#x1EA5;t VN" />
    <meta property="og:title" content="Phucmobile.com - &#x110;i&#x1EC7;n tho&#x1EA1;i, Laptop, Ph&#x1EE5; ki&#x1EC7;n, &#x110;&#x1ED3;ng h&#x1ED3; ch&#xED;nh h&#xE3;ng" />
    <meta property="og:description" content="H&#x1EC7; th&#x1ED1;ng b&#xE1;n l&#x1EBB; &#x111;i&#x1EC7;n tho&#x1EA1;i di &#x111;&#x1ED9;ng, smartphone, m&#xE1;y t&#xED;nh b&#x1EA3;ng, tablet, laptop, ph&#x1EE5; ki&#x1EC7;n, smartwatch, &#x111;&#x1ED3;ng h&#x1ED3; ch&#xED;nh h&#xE3;ng m&#x1EDB;i nh&#x1EA5;t, gi&#xE1; t&#x1ED1;t, d&#x1ECB;ch v&#x1EE5; kh&#xE1;ch h&#xE0;ng &#x111;&#x1B0;&#x1EE3;c y&#xEA;u th&#xED;ch nh&#x1EA5;t VN" />
    <meta content="INDEX,FOLLOW" name="robots" />
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="audience" content="General" />
    <meta name="resource-type" content="Document" />
    <meta name="distribution" content="Global" />
    <meta name="revisit-after" content="1 days" />
    <meta name="GENERATOR" content="Công ty Cổ phần Thế Giới Di Động" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link href="/favicon_TGDD.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="/favicon_TGDD.ico" rel="apple-touch-icon" />
    <link href="/favicon_TGDD.ico" rel="apple-touch-icon-precomposed" />
    <meta property="og:site_name" content="Phucmobile.com" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="fb:pages" content="214993791879039" />
    <meta http-equiv="x-dns-prefetch-control" content="on">
</head>

<body>
    <!-- HEADER -->
    @include('components.header')
    <div class="locationbox__overlay"></div>
    <div class="locationbox">
        <div class="locationbox__item locationbox__item--right" onclick="OpenLocation()">
            <p>Chọn địa chỉ nhận hàng</p>
            <a href="javascript:void(0)">Đóng</a>
        </div>
        <div class="locationbox__item" id="lc_title"><i class="icondetail-address-white"></i><span> Vui lòng đợi trong giây lát...</span></div>
        <div class="locationbox__popup" id="lc_pop--choose">
            <div class="locationbox__popup--cnt locationbox__popup--choose">
                <div class="locationbox__popup--chooseDefault">
                    <div class="lds-ellipsis">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
        <b id="h-provincename" style="display:none!important" data-provinceid="3">Hồ Chí Minh</b>
    </div>
    <div class="locationbox__popup new-popup hide" id="lc_pop--sugg">
        <div class="locationbox__popup--cnt locationbox__popup--suggestion new-locale">
            <div class="flex-block">
                <i class="icon-location"></i>
                <p>Hãy chọn <b>địa chỉ cụ thể</b> để chúng tôi cung cấp <b>chính xác</b> gi&#xE1; v&#xE0; khuy&#x1EBF;n m&#xE3;i</p>
            </div>
            <div class="btn-block">
                <a href="javascript:;" class="btn-location" onclick="OpenLocation()"><b>Chọn địa chỉ</b></a>
                <a href="javascript:;" class="btn-location gray" onclick="SkipLocation()"><b>Đóng</b></a>
            </div>
        </div>
    </div>
    <div class="locationbox__popup locationbox__popup_notify" style="display:none;">
        <div class="locationbox__popup--cnt locationbox__popup--suggestion new-locale">
            Do ảnh hưởng của dịch, một số tỉnh/thành tạm ngưng phục vụ tại siêu thị, chỉ kinh doanh online. <a target="_blank" href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ7cZ_I9J5MelOy1VyYHdPH2Sg21SN9e6utCpgg0Rl_LQzPK81IpHWPhoaAk5l6WPrgpXHZUwAGi-Xs/pubhtml">Xem danh sách</a> siêu thị
            <a onclick="closenotify()" class="close-notify"></a>
        </div>
    </div>
    <!-- CONTENT -->
    <section class="section-content">
        @yield('content')
    </section>

    <div class="preloader preall">
        <div class="loaderweb"></div>
    </div>
    <p id="gb-top-page" class="hide">↑</p>
    <div id="dldingtext">Bạn vui lòng chờ trong giây lát...</div>
    <!-- FOOTER -->
    @include('components.footer')
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="//cdn.tgdd.vn/mwgcart/mwgcore/js/bundle/globalTGDD.min.v202108230130.js" type="text/javascript"></script>
    <script>
        var rooturl = '.Phucmobile.com';
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'pageType': 'Home',
            'pagePlatform': 'Web',
            'pageStatus': 'Kinh doanh'
        })
    </script>
    <script async="async" defer="defer" src="//cdn.tgdd.vn/mwgcart/mwgcore/js/bundle/homeTGDD.min.v202108100850.js" type="text/javascript"></script>
    <script async="async" defer="defer" src="//cdn.tgdd.vn/mwgcart/mwgcore/js/bundle/homeGTM.min.v202107271040.js" type="text/javascript"></script>
</body>

</html>