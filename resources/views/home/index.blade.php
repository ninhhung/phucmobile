@extends('layouts.app')
@section('content')
<div class="bar-top">
    <aside class="homebanner">
        <div id="sync1" class="slider-banner owl-carousel">
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/dtdd/samsung-galaxy-z-flip-3" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45651&r='+ (new Date).getTime(), async: true, cache: false });"><img src="//cdn.tgdd.vn/2021/08/banner/fold3sl-830-300-830x300.png" alt="SS"></a>
            </div>
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/apple#iphone" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45169&r='+ (new Date).getTime(), async: true, cache: false });"><img loading="lazy" class="lazyload owl-lazy" data-src="//cdn.tgdd.vn/2021/07/banner/iphone-12-830-300-830x300.png" alt="iphone"></a>
            </div>
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/dtdd/oppo-reno6-z-5g" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45569&r='+ (new Date).getTime(), async: true, cache: false });"><img loading="lazy" class="lazyload owl-lazy" data-src="//cdn.tgdd.vn/2021/08/banner/renoz-830-300-830x300.png" alt="Reno6 Z"></a>
            </div>
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/dtdd/vivo-y72-5g" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45175&r='+ (new Date).getTime(), async: true, cache: false });"><img loading="lazy" class="lazyload owl-lazy" data-src="//cdn.tgdd.vn/2021/07/banner/830-300-830x300-17.png" alt="vivo"></a>
            </div>
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/dong-ho-thong-minh-apple-watch-s6" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45566&r='+ (new Date).getTime(), async: true, cache: false });"><img loading="lazy" class="lazyload owl-lazy" data-src="//cdn.tgdd.vn/2021/08/banner/830-300-830x300-2.png" alt="apple watch s6"></a>
            </div>
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/laptop-tuu-truong" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45154&r='+ (new Date).getTime(), async: true, cache: false });"><img loading="lazy" class="lazyload owl-lazy" data-src="//cdn.tgdd.vn/2021/08/banner/830-300-830x300-3.png" alt="Laptop Tựu Trường[break]Ưu Đãi X3"></a>
            </div>
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/dong-ho" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45506&r='+ (new Date).getTime(), async: true, cache: false });"><img loading="lazy" class="lazyload owl-lazy" data-src="//cdn.tgdd.vn/2021/08/banner/dhtt-830-300-830x300.png" alt="đồng hồ thời trang"></a>
            </div>
            <div class="item">
                <a aria-label="slide" data-cate="0" data-place="1546" href="/mua-online-gia-re#sac-du-phong" onclick="jQuery.ajax({ url: 'https://www.Phucmobile.com/bannertracking?bid=45123&r='+ (new Date).getTime(), async: true, cache: false });"><img loading="lazy" class="lazyload owl-lazy" data-src="//cdn.tgdd.vn/2021/07/banner/830-300-830x300-15.png" alt="phụ kiện"></a>
            </div>
        </div>
        <div id="sync2" class="slider-banner owl-carousel">
            <div class="item">
                <h3>
                    Galaxy Z Fold3 5G<br />Đặt Trước Rước Quà Khủng
                </h3>
            </div>
            <div class="item">
                <h3>
                    Sắm iPhone 12<br />Giảm Đến 3 Triệu
                </h3>
            </div>
            <div class="item">
                <h3>
                    OPPO Reno6 Z 5G<br />Giá 9.490.000đ, Quà Ngon
                </h3>
            </div>
            <div class="item">
                <h3>
                    Sắm Vivo Y72 5G<br />Giảm Ngay 400 Ngàn
                </h3>
            </div>
            <div class="item">
                <h3>
                    Apple Watch S6<br />Đồng Giảm Sốc 15%
                </h3>
            </div>
            <div class="item">
                <h3>
                    Laptop Tựu Trường<br />Ưu Đãi X3
                </h3>
            </div>
            <div class="item">
                <h3>
                    Đồng Hồ Thời Trang<br />Giảm Đến 45%
                </h3>
            </div>
            <div class="item">
                <h3>
                    Sắm Phụ Kiện<br />Giảm Đến 45%
                </h3>
            </div>
        </div>
    </aside>
</div>

<!--  Săn sale online -->
<div class="prd-promo" data-html-id="3429">
    <div class="prd-promo__top clearfix">
        <p class="prd-promo__title">S&#x103;n Sale Online M&#x1ED7;i Ng&#xE0;y</p>
    </div>
    @if (isset($productsHot))
    <div class="listproduct slider-promo owl-carousel" data-size="17">
        @foreach ($productsHot as $hot)
        <div class="item" data-id="229949" data-pos="1">
            <a href='' class="main-contain">
                <div class="item-label">
                </div>
                <div class="item-img">
                    <img data-src="{{ pare_url_file($hot->pro_image, 'products') }}" class="lazyload" alt="Samsung Galaxy Z Flip3 5G 128GB" width=210 height=210>
                </div>
                <div>
                    <p class="preorder">{{ $hot->pro_name }}</p>
                    <p><i class=" fa-arrow-down"></i> 30%</p>
                </div>
                <p class="item-txt-online">H&#xE0;ng s&#x1EAF;p v&#x1EC1;</p>
                <strong class="price">{{ number_format($hot->pro_price,0,',','.') }}&#x20AB;</strong>
                <p class="item-gift">{{ $hot->pro_description }}</p>
            </a>
        </div>
        @endforeach
    </div>
    @endif
</div>
<div class="box-common card">
    <div class="box-common__top clearfix">
        <h2 class="box-common__title">&#x110;I&#x1EC6;N THO&#x1EA0;I N&#x1ED4;I B&#x1EAC;T NH&#x1EA4;T</h2>
        <div class="box-common__link" data-size="3">
            <a class="readmore-btn" href="dtdd">Xem tất cả <b>183</b> &#x110;i&#x1EC7;n tho&#x1EA1;i</a>
        </div>
    </div>
    <div class="box-common__main">
        <div class="listproduct " data-size="10">
            <div class="item" data-id="226935" data-pos="1">
                <a href='/dtdd/samsung-galaxy-z-fold-3' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Samsung Galaxy Z Fold3 5G 256GB" data-id="226935" data-price="41990000.0" data-brand="Samsung" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/226935/samsung-galaxy-z-fold-3-silver-600x600.jpg" class="lazyload" alt="Samsung Galaxy Z Fold3 5G 256GB" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/label-moi-ra-mat-fnal.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_42 tgdd" />
                    </div>
                    <p class="preorder">Đặt trước đến 16/09</p>
                    <h3>Samsung Galaxy Z Fold3 5G 256GB</h3>
                    <p class="item-txt-online">H&#xE0;ng s&#x1EAF;p v&#x1EC1;</p>
                    <strong class="price">41.990.000&#x20AB;</strong>
                    <p class="item-gift">Tặng PMH 2 triệu, Bao da kèm S Pen, Gói đặc quyền cao cấp</p>


                </a>

            </div>
            <div class="item" data-id="228736" data-pos="2">
                <a href='/dtdd/iphone-12-128gb' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i iPhone 12 128GB" data-id="228736" data-price="22490000.0" data-brand="iPhone (Apple)" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/228736/iphone-12-violet-1-600x600.jpg" class="lazyload" alt="iPhone 12 128GB" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>iPhone 12 128GB</h3>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">24.990.000&#x20AB;</p>
                        <span class="percent">-10%</span>
                    </div>
                    <strong class="price">22.490.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>400.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">33</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="233241" data-pos="3">
                <a href='/dtdd/xiaomi-mi-11-lite-4g' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Xiaomi Mi 11 Lite" data-id="233241" data-price="7090000.0" data-brand="Xiaomi" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/233241/xiaomi-mi-11-lite-4g-pink-1-600x600.jpg" class="lazyload" alt="Xiaomi Mi 11 Lite" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_42 tgdd" />
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Xiaomi Mi 11 Lite</h3>
                    <div class="box-p">
                        <p class="price-old black">7.990.000&#x20AB;</p>
                        <span class="percent">-11%</span>
                    </div>
                    <strong class="price">7.090.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>300.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">515</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="230630" data-pos="4">
                <a href='/dtdd/vivo-y12s-4gb-128gb' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Vivo Y12s (4GB/128GB)" data-id="230630" data-price="3790000.0" data-brand="Vivo" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/230630/vivo-y12s-den-new-600x600-600x600.jpg" class="lazyload" alt="Vivo Y12s (4GB/128GB)" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Vivo Y12s (4GB/128GB)</h3>
                    <div class="box-p">
                        <p class="price-old black">4.290.000&#x20AB;</p>
                        <span class="percent">-11%</span>
                    </div>
                    <strong class="price">3.790.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>200.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">65</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="239747" data-pos="5">
                <a href='/dtdd/oppo-reno6-z-5g' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i OPPO Reno6 Z 5G" data-id="239747" data-price="9490000.0" data-brand="OPPO" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/239747/oppo-reno6-z-5g-black-1-600x600.jpg" class="lazyload" alt="OPPO Reno6 Z 5G" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>OPPO Reno6 Z 5G</h3>
                    <strong class="price">9.490.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>300.000₫</b></p>


                </a>

            </div>
            <div class="item" data-id="235440" data-pos="6">
                <a href='/dtdd/samsung-galaxy-a52-5g' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Samsung Galaxy A52 5G" data-id="235440" data-price="9790000.0" data-brand="Samsung" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/235440/samsung-galaxy-a52-5g-thumb-black-600x600-600x600.jpg" class="lazyload" alt="Samsung Galaxy A52 5G" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_42 tgdd" />
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Samsung Galaxy A52 5G</h3>
                    <div class="box-p">
                        <p class="price-old black">10.990.000&#x20AB;</p>
                        <span class="percent">-10%</span>
                    </div>
                    <strong class="price">9.790.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>300.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">205</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="240286" data-pos="7">
                <a href='/dtdd/vivo-y53s' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Vivo Y53s" data-id="240286" data-price="6990000.0" data-brand="Vivo" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/240286/vivo-y53s-den-600x600.jpg" class="lazyload" alt="Vivo Y53s" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Vivo Y53s</h3>
                    <strong class="price">6.990.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>300.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">15</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="241265" data-pos="8">
                <a href='/dtdd/realme-c21y' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Realme C21Y 4GB" data-id="241265" data-price="3690000.0" data-brand="Realme" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/241265/realme-c21y-black-600x600.jpg" class="lazyload" alt="Realme C21Y 4GB" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_42 tgdd" />
                    </div>
                    <h3>Realme C21Y 4GB</h3>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">3.990.000&#x20AB;</p>
                        <span class="percent">-7%</span>
                    </div>
                    <strong class="price">3.690.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>200.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">45</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="235971" data-pos="9">
                <a href='/dtdd/xiaomi-redmi-note-10-5g' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Xiaomi Redmi Note 10 5G 8GB" data-id="235971" data-price="5990000.0" data-brand="Xiaomi" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/235971/xiaomi-redmi-note-10-5g-xanh-bong-dem-1-600x600.jpg" class="lazyload" alt="Xiaomi Redmi Note 10 5G 8GB" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_42 tgdd" />
                    </div>
                    <h3>Xiaomi Redmi Note 10 5G 8GB</h3>
                    <strong class="price">5.990.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>300.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">274</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="237768" data-pos="10">
                <a href='/dtdd/vsmart-star-5-4gb-64gb' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="&#x110;i&#x1EC7;n tho&#x1EA1;i Vsmart Star 5 (4GB/64GB)" data-id="237768" data-price="2790000.0" data-brand="Vsmart" data-cate="&#x110;i&#x1EC7;n tho&#x1EA1;i" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/42/237768/vsmart-star-5-thumb-black-600x600.jpg" class="lazyload" alt="Vsmart Star 5 (4GB/64GB)" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-02.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_42 tgdd" />
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Vsmart Star 5 (4GB/64GB)</h3>
                    <div class="box-p">
                        <p class="price-old black">3.290.000&#x20AB;</p>
                        <span class="percent">-15%</span>
                    </div>
                    <strong class="price">2.790.000&#x20AB;</strong>
                    <p class="item-gift">Quà <b>200.000₫</b></p>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">28</p>
                    </div>


                </a>

            </div>
        </div>
    </div>
</div>

<div class="box-common" data-html-id="3465">
    <div class="box-common__top clearfix">
        <h2 class="box-common__title">PH&#x1EE4; KI&#x1EC6;N N&#x1ED4;I B&#x1EAC;T NH&#x1EA4;T</h2>
        <div class="box-common__link" data-size="5">
            <a class="readmore-btn accessory" href="/phu-kien">Xem tất cả Phụ kiện</a>
        </div>
    </div>
    <div class="box-common__main">
        <div class="listproduct slider-home owl-carousel" data-size="10">
            <div class="item" data-id="248452" data-pos="1">
                <a href='/tai-nghe/bluetooth-true-wireless-samsung-buds-2-r177n' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N" data-id="248452" data-price="2990000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/248452/bluetooth-true-wireless-samsung-buds-2-r177n-avatar2-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N" width=210 height=210>
                    </div>
                    <h3>Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancelling</span>
                        <span>Ambient Sound</span>
                    </div>
                    <p class="item-txt-online">H&#xE0;ng s&#x1EAF;p v&#x1EC1;</p>
                    <strong class="price">2.990.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="248455" data-pos="2">
                <a href='/tai-nghe/bluetooth-true-wireless-samsung-buds-2-r177n-trang' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N Tr&#x1EAF;ng" data-id="248455" data-price="2990000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/248455/bluetooth-true-wireless-samsung-buds-2-r177n-trang-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N Tr&#x1EAF;ng" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_54 tgdd" />
                    </div>
                    <h3>Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N Trắng</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancelling</span>
                        <span>Ambient Sound</span>
                    </div>
                    <p class="item-txt-online">H&#xE0;ng s&#x1EAF;p v&#x1EC1;</p>
                    <strong class="price">2.990.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="242034" data-pos="3">
                <a href='/tai-nghe/bluetooth-true-wireless-galaxy-buds-pro-bac' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Galaxy Buds Pro B&#x1EA1;c" data-id="242034" data-price="3990000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/242034/bluetooth-true-wireless-galaxy-buds-pro-bac-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Galaxy Buds Pro B&#x1EA1;c" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_54 tgdd" />
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth True Wireless Galaxy Buds Pro Bạc</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancellation</span>
                    </div>
                    <div class="box-p">
                        <p class="price-old black">4.990.000&#x20AB;</p>
                        <span class="percent">-20%</span>
                    </div>
                    <strong class="price">3.990.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="236026" data-pos="4">
                <a href='/tai-nghe/tai-nghe-bluetooth-airpods-pro-apple-mwp22' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth AirPods Pro Wireless Charge Apple MWP22" data-id="236026" data-price="4990000.0" data-brand="Apple" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/236026/airpods-pro-wireless-charge-apple-mwp22-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth AirPods Pro Wireless Charge Apple MWP22" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth AirPods Pro Wireless Charge Apple MWP22</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancellation</span>
                        <span>Adaptive EQ</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">6.790.000&#x20AB;</p>
                        <span class="percent">-26%</span>
                    </div>
                    <strong class="price">4.990.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">8</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="228893" data-pos="5">
                <a href='/tai-nghe/tai-nghe-bluetooth-true-wireless-lg-hbs-fn4' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless LG Tone Free HBS-FN4" data-id="228893" data-price="990000.0" data-brand="LG" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/228893/bluetooth-tws-lg-tone-free-hbs-fn4-den-thumb-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless LG Tone Free HBS-FN4" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth True Wireless LG Tone Free HBS-FN4</h3>
                    <div class="item-compare">
                        <span>Truy&#x1EC1;n ph&#xE1;t nh&#x1EA1;c MQA</span>
                        <span>Headphone Spatial Processing</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">2.790.000&#x20AB;</p>
                        <span class="percent">-64%</span>
                    </div>
                    <strong class="price">990.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">29</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="238231" data-pos="6">
                <a href='/tai-nghe/bluetooth-tws-samsung-galaxy-bud-r175' class=" main-contain" data-s="OnlineSavingCMS" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds&#x2B; R175" data-id="238231" data-price="1290000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/238231/bluetooth-tws-samsung-galaxy-bub-r175-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds&#x2B; R175" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth True Wireless Samsung Galaxy Buds+ R175</h3>
                    <div class="item-compare">
                        <span>Ambient Sound</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">2.140.000&#x20AB;</p>
                        <span class="percent">-39%</span>
                    </div>
                    <strong class="price">1.290.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">53</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="237679" data-pos="7">
                <a href='/sac-dtdd/hydrus-pj-jp196' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Pin s&#x1EA1;c d&#x1EF1; ph&#xF2;ng Polymer 10.000 mAh Hydrus PJ JP196" data-id="237679" data-price="294000.0" data-brand="Hydrus" data-cate="S&#x1EA1;c d&#x1EF1; ph&#xF2;ng" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/57/237679/hydrus-pj-jp196-ava-600x600.jpg" class="lazyload" alt="Hydrus PJ JP196" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Pin sạc dự phòng Polymer 10.000 mAh Hydrus PJ JP196</h3>
                    <p class="item-txt-online">Ch&#x1EC9; b&#xE1;n online</p>
                    <div class="box-p">
                        <p class="price-old black">490.000&#x20AB;</p>
                        <span class="percent">-40%</span>
                    </div>
                    <strong class="price">294.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="227187" data-pos="8">
                <a href='/loa-laptop/loa-bluetooth-lg-xboom-go-pl7-xanh-den' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Loa Bluetooth LG Xboom Go PL7 Xanh &#x110;en" data-id="227187" data-price="1890000.0" data-brand="LG" data-cate="Loa" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/2162/227187/loa-bluetooth-lg-xboom-go-pl7-xanh-den-600x600.jpg" class="lazyload" alt="Loa Bluetooth LG Xboom Go PL7" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Loa Bluetooth LG Xboom Go PL7 Xanh Đen</h3>
                    <div class="item-compare">
                        <span>30 W</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">4.490.000&#x20AB;</p>
                        <span class="percent">-57%</span>
                    </div>
                    <strong class="price">1.890.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">8</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="226142" data-pos="9">
                <a href='/sac-dtdd/sac-du-phong-polymer-10000mah-qc30-xmobile-p68d' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Pin s&#x1EA1;c d&#x1EF1; ph&#xF2;ng Polymer 10.000 mAh Type C PD QC3.0 Xmobile P68D" data-id="226142" data-price="420000.0" data-brand="Xmobile" data-cate="S&#x1EA1;c d&#x1EF1; ph&#xF2;ng" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/57/226142/sac-du-phong-polymer-10000mah-qc30-xmobile-p68d-avatar-1-600x600.jpg" class="lazyload" alt="Xmobile P68D" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Pin sạc dự phòng Polymer 10.000 mAh Type C PD QC3.0 Xmobile P68D</h3>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">700.000&#x20AB;</p>
                        <span class="percent">-40%</span>
                    </div>
                    <strong class="price">420.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-half"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">15</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="174748" data-pos="10">
                <a href='/sac-dtdd/sac-du-phong-polymer-10000mah-qc-3-evalu-pa-f1-air' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Pin s&#x1EA1;c d&#x1EF1; ph&#xF2;ng Polymer 10.000 mAh Type C QC3.0 eValu PA F1 Air" data-id="174748" data-price="360000.0" data-brand="eValu" data-cate="S&#x1EA1;c d&#x1EF1; ph&#xF2;ng" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/57/174748/sac-du-phong-polymer-10000mah-qc-3-evalu-pa-f1-air-avatar-1-600x600.jpg" class="lazyload" alt="eValu PA F1 Air" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Pin sạc dự phòng Polymer 10.000 mAh Type C QC3.0 eValu PA F1 Air</h3>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">600.000&#x20AB;</p>
                        <span class="percent">-40%</span>
                    </div>
                    <strong class="price">360.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">64</p>
                    </div>


                </a>

            </div>
        </div>
    </div>
</div>
<!-- End -->

<!-- SERVICE -->
<div class="box-common" data-html-id="3465">
    <div class="box-common__top clearfix">
        <h2 class="box-common__title">DỊCH VỤ SỬA CHỮA</h2>
        <div class="box-common__link" data-size="5">
            <a class="readmore-btn accessory" href="/phu-kien">Xem tất cả</a>
        </div>
    </div>
    <div class="box-common__main">
        <div class="listproduct slider-home owl-carousel" data-size="10">
            <div class="item" data-id="248452" data-pos="1">
                <a href='/tai-nghe/bluetooth-true-wireless-samsung-buds-2-r177n' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N" data-id="248452" data-price="2990000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/248452/bluetooth-true-wireless-samsung-buds-2-r177n-avatar2-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N" width=210 height=210>
                    </div>
                    <h3>Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancelling</span>
                        <span>Ambient Sound</span>
                    </div>
                    <p class="item-txt-online">H&#xE0;ng s&#x1EAF;p v&#x1EC1;</p>
                    <strong class="price">2.990.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="248455" data-pos="2">
                <a href='/tai-nghe/bluetooth-true-wireless-samsung-buds-2-r177n-trang' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N Tr&#x1EAF;ng" data-id="248455" data-price="2990000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/248455/bluetooth-true-wireless-samsung-buds-2-r177n-trang-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N Tr&#x1EAF;ng" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_54 tgdd" />
                    </div>
                    <h3>Tai nghe Bluetooth True Wireless Samsung Galaxy Buds 2 R177N Trắng</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancelling</span>
                        <span>Ambient Sound</span>
                    </div>
                    <p class="item-txt-online">H&#xE0;ng s&#x1EAF;p v&#x1EC1;</p>
                    <strong class="price">2.990.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="242034" data-pos="3">
                <a href='/tai-nghe/bluetooth-true-wireless-galaxy-buds-pro-bac' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Galaxy Buds Pro B&#x1EA1;c" data-id="242034" data-price="3990000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/242034/bluetooth-true-wireless-galaxy-buds-pro-bac-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Galaxy Buds Pro B&#x1EA1;c" width=210 height=210>
                        <img src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png" width="40" height="40" class="lazyload lbliconimg lbliconimg_54 tgdd" />
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth True Wireless Galaxy Buds Pro Bạc</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancellation</span>
                    </div>
                    <div class="box-p">
                        <p class="price-old black">4.990.000&#x20AB;</p>
                        <span class="percent">-20%</span>
                    </div>
                    <strong class="price">3.990.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="236026" data-pos="4">
                <a href='/tai-nghe/tai-nghe-bluetooth-airpods-pro-apple-mwp22' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth AirPods Pro Wireless Charge Apple MWP22" data-id="236026" data-price="4990000.0" data-brand="Apple" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/236026/airpods-pro-wireless-charge-apple-mwp22-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth AirPods Pro Wireless Charge Apple MWP22" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth AirPods Pro Wireless Charge Apple MWP22</h3>
                    <div class="item-compare">
                        <span>Active Noise Cancellation</span>
                        <span>Adaptive EQ</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">6.790.000&#x20AB;</p>
                        <span class="percent">-26%</span>
                    </div>
                    <strong class="price">4.990.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">8</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="228893" data-pos="5">
                <a href='/tai-nghe/tai-nghe-bluetooth-true-wireless-lg-hbs-fn4' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless LG Tone Free HBS-FN4" data-id="228893" data-price="990000.0" data-brand="LG" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/228893/bluetooth-tws-lg-tone-free-hbs-fn4-den-thumb-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless LG Tone Free HBS-FN4" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth True Wireless LG Tone Free HBS-FN4</h3>
                    <div class="item-compare">
                        <span>Truy&#x1EC1;n ph&#xE1;t nh&#x1EA1;c MQA</span>
                        <span>Headphone Spatial Processing</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">2.790.000&#x20AB;</p>
                        <span class="percent">-64%</span>
                    </div>
                    <strong class="price">990.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">29</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="238231" data-pos="6">
                <a href='/tai-nghe/bluetooth-tws-samsung-galaxy-bud-r175' class=" main-contain" data-s="OnlineSavingCMS" data-site="1" data-pro="3" data-cache="True" data-name="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds&#x2B; R175" data-id="238231" data-price="1290000.0" data-brand="Samsung" data-cate="Tai nghe" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/54/238231/bluetooth-tws-samsung-galaxy-bub-r175-ava-600x600.jpg" class="lazyload" alt="Tai nghe Bluetooth True Wireless Samsung Galaxy Buds&#x2B; R175" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Tai nghe Bluetooth True Wireless Samsung Galaxy Buds+ R175</h3>
                    <div class="item-compare">
                        <span>Ambient Sound</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">2.140.000&#x20AB;</p>
                        <span class="percent">-39%</span>
                    </div>
                    <strong class="price">1.290.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">53</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="237679" data-pos="7">
                <a href='/sac-dtdd/hydrus-pj-jp196' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Pin s&#x1EA1;c d&#x1EF1; ph&#xF2;ng Polymer 10.000 mAh Hydrus PJ JP196" data-id="237679" data-price="294000.0" data-brand="Hydrus" data-cate="S&#x1EA1;c d&#x1EF1; ph&#xF2;ng" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/57/237679/hydrus-pj-jp196-ava-600x600.jpg" class="lazyload" alt="Hydrus PJ JP196" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Pin sạc dự phòng Polymer 10.000 mAh Hydrus PJ JP196</h3>
                    <p class="item-txt-online">Ch&#x1EC9; b&#xE1;n online</p>
                    <div class="box-p">
                        <p class="price-old black">490.000&#x20AB;</p>
                        <span class="percent">-40%</span>
                    </div>
                    <strong class="price">294.000&#x20AB;</strong>


                </a>

            </div>
            <div class="item" data-id="227187" data-pos="8">
                <a href='/loa-laptop/loa-bluetooth-lg-xboom-go-pl7-xanh-den' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Loa Bluetooth LG Xboom Go PL7 Xanh &#x110;en" data-id="227187" data-price="1890000.0" data-brand="LG" data-cate="Loa" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/2162/227187/loa-bluetooth-lg-xboom-go-pl7-xanh-den-600x600.jpg" class="lazyload" alt="Loa Bluetooth LG Xboom Go PL7" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Loa Bluetooth LG Xboom Go PL7 Xanh Đen</h3>
                    <div class="item-compare">
                        <span>30 W</span>
                    </div>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">4.490.000&#x20AB;</p>
                        <span class="percent">-57%</span>
                    </div>
                    <strong class="price">1.890.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">8</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="226142" data-pos="9">
                <a href='/sac-dtdd/sac-du-phong-polymer-10000mah-qc30-xmobile-p68d' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Pin s&#x1EA1;c d&#x1EF1; ph&#xF2;ng Polymer 10.000 mAh Type C PD QC3.0 Xmobile P68D" data-id="226142" data-price="420000.0" data-brand="Xmobile" data-cate="S&#x1EA1;c d&#x1EF1; ph&#xF2;ng" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/57/226142/sac-du-phong-polymer-10000mah-qc30-xmobile-p68d-avatar-1-600x600.jpg" class="lazyload" alt="Xmobile P68D" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Pin sạc dự phòng Polymer 10.000 mAh Type C PD QC3.0 Xmobile P68D</h3>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">700.000&#x20AB;</p>
                        <span class="percent">-40%</span>
                    </div>
                    <strong class="price">420.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-half"></i>
                            <i class="icon-star-dark"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">15</p>
                    </div>


                </a>

            </div>
            <div class="item" data-id="174748" data-pos="10">
                <a href='/sac-dtdd/sac-du-phong-polymer-10000mah-qc-3-evalu-pa-f1-air' class=" main-contain" data-s="Nomal" data-site="1" data-pro="3" data-cache="False" data-name="Pin s&#x1EA1;c d&#x1EF1; ph&#xF2;ng Polymer 10.000 mAh Type C QC3.0 eValu PA F1 Air" data-id="174748" data-price="360000.0" data-brand="eValu" data-cate="S&#x1EA1;c d&#x1EF1; ph&#xF2;ng" data-box="BoxHome">
                    <div class="item-label">
                    </div>
                    <div class="item-img">
                        <img data-src="https://cdn.tgdd.vn/Products/Images/57/174748/sac-du-phong-polymer-10000mah-qc-3-evalu-pa-f1-air-avatar-1-600x600.jpg" class="lazyload" alt="eValu PA F1 Air" width=210 height=210>
                    </div>
                    <p class='result-label temp1'><img width='20' height='20' class='lazyload' alt='TRỢ GIÁ MÙA DỊCH' data-src='https://cdn.tgdd.vn/2020/10/content/icon1-50x50.png'><span>TRỢ GIÁ MÙA DỊCH</span></p>
                    <h3>Pin sạc dự phòng Polymer 10.000 mAh Type C QC3.0 eValu PA F1 Air</h3>
                    <p class="item-txt-online">Online gia&#x301; re&#x309;</p>
                    <div class="box-p">
                        <p class="price-old black">600.000&#x20AB;</p>
                        <span class="percent">-40%</span>
                    </div>
                    <strong class="price">360.000&#x20AB;</strong>
                    <div class="item-rating">
                        <p>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star"></i>
                            <i class="icon-star-dark"></i>
                        </p>
                        <p class="item-rating-total">64</p>
                    </div>


                </a>

            </div>
        </div>
    </div>
</div>
<!-- END SERVICE -->
@stop