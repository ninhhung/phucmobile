@extends('layouts.app')
@section('content')
<div class="box-sort ">
    <div class="box-checkbox extend  ">
        <a href="javascript:;" data-href="" data-type="sp2020" class="c-checkitem ">
            <span class="tick-checkbox"></span>
            <p>Giảm giá</p>
        </a>
        <a href="javascript:;" data-href="-tra-gop-0-phan-tram" data-type="installment0" class="c-checkitem ">
            <span class="tick-checkbox"></span>
            <p>Góp 0%</p>
        </a>
        <a href="javascript:;" data-href="" data-type="monopoly" class="c-checkitem">
            <span class="tick-checkbox"></span>
            <p>Độc quyền</p>
        </a>
        <a href="javascript:;" data-href="-moi-ra-mat" data-type="new" data-prop="0" data-newv2="False" class="c-checkitem ">
            <span class="tick-checkbox"></span>
            <p>Mới</p>
        </a>
        <div class="sort-select">
            <label for="">Xep theo:</label>
            <Select class="box-sort-select">
                <option value="">Noi bat</option>
                <option value="">% Giam</option>
                <option value="">Gia cao den thap</option>
                <option value="">Gia thap den cao</option>
            </Select>
        </div>
    </div>

</div>
<div class="clearfix"></div>
<div class="container-productbox">
    <div id="preloader">
        <div id="loader"></div>
    </div>
    <ul class="listproduct">
        @if (isset($products))
        @foreach ($products as $product)
        <li class=" item ajaxed __cate_42" data-index="1" data-id="226935" data-issetup="0" data-maingroup="13" data-subgroup="1491" data-type="3" data-vehicle="1" data-productcode="0131491002611" data-pos="1">
            <a href="{{ route('get.detail.product', [$product->pro_slug, $product->id]) }}" data-s="Nomal" data-site="1" data-pro="3" data-cache="True" data-sv="webtgdd-26-84" data-name="Điện thoại Samsung Galaxy Z Fold3 5G 256GB" data-id="226935" data-price="41990000.0" data-brand="Samsung" data-cate="Điện thoại" data-box="BoxCate" class="main-contain">
                <div class="item-label">
                </div>
                <div class="item-img item-img_42">
                    <img class="thumb lazyloaded" alt="Samsung Galaxy Z Fold3 5G 256GB" src="{{ pare_url_file($product->pro_image, 'products') }}">
                </div>
                <h3>
                    {{ $product->pro_name }}
                </h3>
                <p class="item-txt-online">Hàng sắp về</p>
                <strong class="price">{{ number_format($product->pro_price) }}₫</strong>
                <p class="item-gift">{{ $product->pro_description }}</p>
            </a>
            <div class="item-bottom">
                <a href="#" class="shiping"></a>
            </div>
        </li>
        @endforeach
        @endif
    </ul>
</div>
<div class="view-more ">
    <a href="javascript:;">Xem thêm điện thoại</a>
</div>

@stop