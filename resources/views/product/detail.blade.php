@extends('layouts.app')
@section('content')
<section class="detail">
    <ul class="breadcrumb">
        <li>
            <a href="/dtdd">Điện thoại</a>
            <meta property="position" content="1">
        </li>
        <li>
            <span>›</span>
            <a href="/dtdd-samsung">Điện thoại Samsung</a>
            <meta property="position" content="2">
        </li>
    </ul>
    <h1>Điện thoại Samsung Galaxy Note 20</h1>
    <div class="box02">
        <div class="box02__left">
            <div class="detail-rate">
                <p>
                    <i class="icondetail-star"></i>
                    <i class="icondetail-star"></i>
                    <i class="icondetail-star"></i>
                    <i class="icondetail-star-dark"></i>
                    <i class="icondetail-star-dark"></i>
                </p>
                <p class="detail-rate-total">118 <span>đánh giá</span></p>
            </div>
        </div>
        <div class="box02__right" data-id="218355" data-href="/dtdd/samsung-galaxy-note-20" data-img="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-062220-122200-fix-600x600.jpg" data-name="Điện thoại Samsung Galaxy Note 20">
            <i class="icondetail-sosanh"></i>
            So sánh
        </div>
    </div>
    <div class="like-fanpage" data-url="http://www.thegioididong.com/dtdd/samsung-galaxy-note-20?src=osp"><iframe src="//www.facebook.com/plugins/like.php?href=http://www.thegioididong.com/dtdd/samsung-galaxy-note-20?src=osp&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:150px; height:40px;" allowtransparency="true"></iframe></div>
    <div class="box_main">
        <div class="box_left">
            <div class="box01">
                <div class="box01__show">
                    <div class="show-tab active" data-gallery-id="featured-images-gallery" data-color-id="0">
                        <div class="detail-slider owl-carousel owl-loaded owl-drag">
                            <div class="owl-stage-outer owl-height" style="height: 394.138px;">
                                <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 13490px;">
                                    <div class="owl-item active" style="width: 710px;"><a href="javascript:void(0)" class="slider-item video-item" data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="161406" data-video-id="ZApix2DaDfo" data-index="0" data-time="0">
                                            <img src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/samsung-galaxy-note-20-thumbvideo-780x433.jpg" alt="Samsung Galaxy Note 20" width="710" height="394">
                                            <i class="icondetail-videoop iconvideo"></i>
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102363" data-video-id="" data-index="1" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/-samsung-galaxy-note-20-tinh-nang-noi-bat.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity: 1;" src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/-samsung-galaxy-note-20-tinh-nang-noi-bat.jpg">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102362" data-video-id="" data-index="2" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-thietke.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102354" data-video-id="" data-index="3" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-manhinh.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102349" data-video-id="" data-index="4" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-choigame.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102360" data-video-id="" data-index="5" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-spen.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102351" data-video-id="" data-index="6" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-ghichu.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102348" data-video-id="" data-index="7" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-camera.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102350" data-video-id="" data-index="8" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-chupdem.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102353" data-video-id="" data-index="9" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-laynetdong.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102356" data-video-id="" data-index="10" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-quay-8k.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102361" data-video-id="" data-index="11" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-super-steady.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102364" data-video-id="" data-index="12" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-zoom.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102355" data-video-id="" data-index="13" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-pin.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102357" data-video-id="" data-index="14" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-sac.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102359" data-video-id="" data-index="15" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-samsung-notes.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102358" data-video-id="" data-index="16" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-samsung-dex.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item " data-gallery-id="featured-images-gallery" data-color-id="0" data-picture-id="102352" data-video-id="" data-index="17" data-time="0">
                                            <img class="owl-lazy" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/Slider/vi-vn-samsung-galaxy-note-20-khangnuoc.jpg" width="710" height="394" alt="Samsung Galaxy Note 20" style="opacity:0;">
                                        </a></div>
                                    <div class="owl-item" style="width: 710px; display: none;"><a href="javascript:void(0)" class="slider-item slider-policy" data-gallery-id="featured-images-gallery" data-color-id="0" data-index="18">
                                            <img class="owl-lazy" data-src="//cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11" width="710" height="394" alt="Chính sách đổi trả" style="opacity:0;">
                                            <img class="si-ghtn hide" data-src="//cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/img_giaohang_tgdd-min.png" alt="Giao hàng tận nhà nhanh chóng">
                                            <div>
                                                <p>
                                                    Hư gì đổi nấy <b>12 tháng</b> tại 2105 siêu thị toàn quốc (miễn phí tháng đầu) <a href="https://www.thegioididong.com/chinh-sach-bao-hanh-san-pham"></a>
                                                    <a href="javascript:;" onclick="showPopupPolicy()" title="Chính sách đổi trả">
                                                        Xem chi tiết
                                                    </a>
                                                </p>
                                                <p>
                                                    Bảo hành <b>chính hãng điện thoại 1 năm</b>
                                                </p>
                                            </div>
                                        </a></div>
                                </div>
                            </div>
                            <div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"></button><button type="button" role="presentation" class="owl-next"></button></div>
                            <div class="owl-dots disabled"></div>
                        </div>
                        <div class="total-imgslider">
                            <a id="show-popup-featured-images-gallery" style="display: block" href="javascript:void(0)" data-is-360-gallery="False" class="read-full" data-gallery-id="featured-images-gallery" data-color-id="0">Xem tất cả điểm nổi bật</a>
                            <div class="counter">(1/19)</div>
                            <a class="note s3 hide" href="https://www.thegioididong.com/hoi-dap/tim-hieu-ve-cac-mat-kinh-cuong-luc-gorilla-glass-1172198" target="_blank"><span>Tìm hiểu:</span> Gorilla Glass</a>
                            <a class="note s4 hide" href="https://www.thegioididong.com/hoi-dap/man-hinh-super-amoled-plus-la-gi-905774" target="_blank"><span>Tìm hiểu:</span> Super AMOLED Plus</a>
                            <a class="note s5 hide" href="https://www.thegioididong.com/hoi-dap/kham-pha-vi-xu-ly-hang-dau-tu-samsung-exynos-990-1240378" target="_blank"><span>Tìm hiểu:</span> Chip Exyos 990</a>
                            <a class="note s9 hide" href="https://www.thegioididong.com/hoi-dap/che-do-chup-dem-night-mode-la-gi-907873" target="_blank"><span>Tìm hiểu:</span> Chế độ Night Mode</a>
                            <a class="note s11 hide" href="http://www.thegioididong.com/hoi-dap/cac-chuan-quay-phim-tren-dien-thoai-tablet-pho-bi-1174134?src=slider#8k" target="_blank"><span>Tìm hiểu:</span> Quay video 8K</a>
                            <a class="note s12 hide" href="https://www.thegioididong.com/hoi-dap/sieu-on-dinh-super-steady-tren-galaxy-la-gi-1232363" target="_blank"><span>Tìm hiểu:</span> Super Steady</a>
                            <a class="note s15 hide" href="https://www.thegioididong.com/hoi-dap/cong-nghe-sac-nhanh-tren-smartphone-755549" target="_blank"><span>Tìm hiểu:</span> Công nghệ sạc nhanh</a>
                            <a class="note s16 hide" href="https://www.thegioididong.com/hoi-dap/samsung-dex-la-gi-tien-dung-nhu-the-nao-1172144" target="_blank"><span>Tìm hiểu:</span> Samsung DeX</a>
                            <a class="note s18 hide" href="https://www.thegioididong.com/hoi-dap/chong-nuoc-va-chong-bui-tren-smart-phone-771130?src=slider#ip68" target="_blank"><span>Tìm hiểu:</span> Chuẩn IP68</a>
                        </div>
                    </div>
                </div>
                <div class="scrolling_inner">
                    <div class="box01__tab scrolling">
                        <div id="thumb-featured-images-gallery-0" class="item itemTab active " data-gallery-id="featured-images-gallery" data-color-id="0" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <i class="icondetail-noibat"></i>
                            </div>
                            <p>Điểm nổi bật</p>
                        </div>
                        <div id="thumb-videos-gallery-0" class="item itemTab  " data-gallery-id="videos-gallery" data-color-id="0" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <i class="icondetail-video"></i>
                            </div>
                            <p>2 Video</p>
                        </div>
                        <div id="thumb-color-images-gallery-7" class="item itemTab  " data-gallery-id="color-images-gallery" data-color-id="7" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <img data-src="//cdn.tgdd.vn/Products/Images/42/218355/200-note-20-3-org.png" alt="Xanh lá" width="41" height="41" loading="lazy" class=" lazyloaded" src="//cdn.tgdd.vn/Products/Images/42/218355/200-note-20-3-org.png">
                            </div>
                            <p>Xanh lá</p>
                        </div>
                        <div id="thumb-color-images-gallery-6" class="item itemTab  " data-gallery-id="color-images-gallery" data-color-id="6" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <img data-src="//cdn.tgdd.vn/Products/Images/42/218355/200-note-20-1-org.png" alt="Xám" width="41" height="41" loading="lazy" class=" lazyloaded" src="//cdn.tgdd.vn/Products/Images/42/218355/200-note-20-1-org.png">
                            </div>
                            <p>Xám</p>
                        </div>
                        <div id="thumb-color-images-gallery-16" class="item itemTab  " data-gallery-id="color-images-gallery" data-color-id="16" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <img data-src="//cdn.tgdd.vn/Products/Images/42/218355/200-note-20-2-org.png" alt="Vàng đồng" width="41" height="41" loading="lazy" class=" lazyloaded" src="//cdn.tgdd.vn/Products/Images/42/218355/200-note-20-2-org.png">
                            </div>
                            <p>Vàng đồng</p>
                        </div>
                        <div id="thumb-unbox-gallery-0" class="item itemTab  is-show-popup" data-gallery-id="unbox-gallery" data-color-id="0" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <i class="icondetail-box"></i>
                            </div>
                            <p>Hình mở hộp</p>
                        </div>
                        <div id="thumb-camera-image-gallery-0" class="item itemTab  " data-gallery-id="camera-image-gallery" data-color-id="0" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <i class="icondetail-graycamera"></i>
                            </div>
                            <p>Chụp từ camera</p>
                        </div>
                        <div id="thumb-360-degree-gallery-0" class="item itemTab  is-show-popup" data-gallery-id="360-degree-gallery" data-color-id="0" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <i class="icondetail-360do"></i>
                            </div>
                            <p>Hình 360 độ</p>
                        </div>
                        <div id="thumb-specification-gallery-0" class="item itemTab  is-show-popup" data-gallery-id="specification-gallery" data-color-id="0" data-is-full-spec="True" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <i class="icondetail-thongso"></i>
                            </div>
                            <p>Thông số kỹ thuật</p>
                        </div>
                        <div id="thumb-article-gallery-0" class="item itemTab  is-show-popup" data-gallery-id="article-gallery" data-color-id="0" data-is-full-spec="False" data-color-order-id="0" data-isfeatureimage="True">
                            <div class="item-border">
                                <i class="icondetail-danhgia"></i>
                            </div>
                            <p>Bài viết đánh giá</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-tab">
                <div class="bt-overlay"></div>
                <ul class="block-tab-top">
                    <li id="tab-featured-images-gallery-0" class="tab-item active" data-is-360-gallery="False" data-gallery-id="featured-images-gallery" data-color-id="0" data-thump-name="Điểm nổi bật">
                        Điểm nổi bật
                    </li>
                    <li id="tab-videos-gallery-0" class="tab-item " data-is-360-gallery="False" data-gallery-id="videos-gallery" data-color-id="0" data-thump-name="2 Video">
                        Video
                    </li>
                    <li id="tab-color-images-gallery-7" class="tab-item " data-is-360-gallery="False" data-gallery-id="color-images-gallery" data-color-id="7" data-thump-name="Xanh lá">
                        Xanh lá
                    </li>
                    <li id="tab-color-images-gallery-6" class="tab-item " data-is-360-gallery="False" data-gallery-id="color-images-gallery" data-color-id="6" data-thump-name="Xám">
                        Xám
                    </li>
                    <li id="tab-color-images-gallery-16" class="tab-item " data-is-360-gallery="False" data-gallery-id="color-images-gallery" data-color-id="16" data-thump-name="Vàng đồng">
                        Vàng đồng
                    </li>
                    <li id="tab-unbox-gallery-0" class="tab-item " data-is-360-gallery="False" data-gallery-id="unbox-gallery" data-color-id="0" data-thump-name="Hình mở hộp">
                        Hình mở hộp
                    </li>
                    <li id="tab-camera-image-gallery-0" class="tab-item " data-is-360-gallery="False" data-gallery-id="camera-image-gallery" data-color-id="0" data-thump-name="Chụp từ camera">
                        Chụp từ camera
                    </li>
                    <li id="tab-360-degree-gallery-0" class="tab-item " data-is-360-gallery="True" data-gallery-id="360-degree-gallery" data-color-id="0" data-thump-name="Hình 360 độ">
                        Hình 360 độ
                    </li>
                    <li id="tab-specification-gallery-0" class="tab-item " data-is-360-gallery="False" data-gallery-id="specification-gallery" data-color-id="0" data-thump-name="Thông số kỹ thuật">
                        Thông số kỹ thuật
                    </li>
                    <li id="tab-article-gallery-0" class="tab-item " data-is-360-gallery="False" data-gallery-id="article-gallery" data-color-id="0" data-thump-name="Bài viết đánh giá">
                        Bài viết đánh giá
                    </li>
                </ul>
                <div class="btn-closemenu close-tab">Đóng</div>
                <div class="block-tab-content">
                    <div class="content-t active not-load-content" id="tab-content-featured-images-gallery-0">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-videos-gallery-0">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-color-images-gallery-7">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-color-images-gallery-6">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-color-images-gallery-16">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-unbox-gallery-0">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-camera-image-gallery-0">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-360-degree-gallery-0">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-specification-gallery-0">
                    </div>
                    <div class="content-t  not-load-content" id="tab-content-article-gallery-0">
                    </div>
                </div>
            </div>
            <div class="popup-overlay">
                <div class="popup-overlay__popup-video-gallery">
                    <div class="close-popup"><i></i></div>
                    <div id="video-player"></div>
                </div>
            </div>
            <span style="display: none;" id="show-popup-unbox-gallery" data-is-360-gallery="false" class="read-full" data-gallery-id="unbox-gallery" data-color-id="0"></span>
            <span style="display: none;" id="show-popup-360-degree-gallery" data-is-360-gallery="true" class="read-full" data-gallery-id="360-degree-gallery" data-color-id="0"></span>
            <span style="display: none;" id="show-popup-specification-gallery" data-is-360-gallery="false" class="read-full" data-gallery-id="specification-gallery" data-color-id="0"></span>
            <span style="display: none;" id="show-popup-article-gallery" data-is-360-gallery="false" data-is-article-gallery="true" class="read-full" data-gallery-id="article-gallery" data-color-id="0"></span>
            <div class="popup-video-rating">
                <div class="popup-video-rating__content active">
                    <b class="popup-video-rating__content__title">Video có hữu ích cho việc mua hàng của bạn không?</b>
                    <span class="popup-video-rating__content_like-container popup-video-rating__content__action"><i class="iconvideorating-likeGlr"></i>Hữu ích</span>
                    <span class="popup-video-rating__content_dislike-container popup-video-rating__content__action"><i class="iconvideorating-unlikeGlr"></i>Không hữu ích</span>
                </div>
                <b class="popup-video-rating__thank-you">Cảm ơn bạn đã đánh giá video</b>
            </div>

            <div class="manu-info-popup">
                <div class="manu-info-popup__content">
                    <span class="manu-info-popup__content__btn-close"></span>
                    <p class="manu-info-popup__content__title">
                        Giới thiệu về hãng
                        <img width="70" height="30" data-src="//cdn.tgdd.vn/Brand/1/samsungnew-220x48-1.png" alt="Samsung" class="lazyload">
                    </p>
                    <div class="manu-info-popup__content__desc">
                        <p>(Hàn Quốc)</p>
                    </div>
                </div>
            </div>
            <script type="text/javascript" id="www-widgetapi-script" src="https://www.youtube.com/s/player/9da24d97/www-widgetapi.vflset/www-widgetapi.js" async=""></script>
            <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-WKQZL8"></script>
            <script src="https://www.youtube.com/iframe_api"></script>
            <script>
                setTimeout(function() {
                    var tag = document.createElement('script');
                    tag.src = "https://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                }, 100);
            </script>
            <div class="policy_intuitive cate42 scenarioNomal">
                <div class="policy">
                    <ul class="policy__list">
                        <li>
                            <div class="iconl">
                                <i class="icondetail-doimoi"></i>
                            </div>
                            <p>
                                Hư gì đổi nấy <b>12 tháng</b> tại 2105 siêu thị toàn quốc (miễn phí tháng đầu) <a href="https://www.thegioididong.com/chinh-sach-bao-hanh-san-pham"></a>
                                <a href="javascript:;" onclick="showPopupPolicy()" title="Chính sách đổi trả">
                                    Xem chi tiết
                                </a>
                            </p>
                        </li>
                        <li data-field="IsSameBHAndDT">
                            <div class="iconl">
                                <i class="icondetail-baohanh"></i>
                            </div>
                            <p>
                                Bảo hành <b>chính hãng điện thoại 1 năm</b>

                            </p>
                        </li>

                        <li>
                            <div class="iconl"><i class="icondetail-sachhd"></i></div>
                            <p>Bộ sản phẩm gồm: Hộp, Sạc, Sách hướng dẫn, Bút cảm ứng, Cây lấy sim, Cáp Type C - Type C, Củ sạc nhanh rời đầu Type C, Tai nghe dây đầu Type C <a href="javascript:" class="hinh-mo-hop-link">Xem hình</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="wrap_wrtp dmx-popup" id="popup-baohanh">
                <div class="pop">
                    <div class="hdpop dmx">
                        CHÍNH SÁCH BẢO HÀNH, ĐỔI TRẢ
                        <a href="javascript:closePopWrt()" class="closehd">
                            <span>✖</span>
                        </a>
                    </div>
                    <div class="overscroll">
                        <div class="warranty-box">
                            <h2 class="title">BẢO HÀNH CHÍNH HÃNG</h2>
                            <span>
                                Thân máy 1 năm, pin 1 năm , sạc 6 tháng
                            </span>
                            <p><a target="_blank" href="/bao-hanh/samsung">Xem điểm bảo hành Samsung</a></p>
                        </div>
                        <div class="change-box">
                            <h2 class="title">Chính sách đổi trả <span>Chỉ cần số điện thoại mua hàng, không cần giấy tờ.</span></h2>
                            <div class="block-change">

                                <h3 class="down" onclick="toggleBlock(this)">Lỗi do nhà sản xuất, Khách hàng chọn 1 trong 3 hình thức</h3>
                                <div class="content-insider show">
                                    <div>
                                        <p><strong>Bảo hành có cam kết trong 12 tháng (chỉ áp dụng cho sản phẩm chính, KHÔNG áp dụng cho phụ kiện kèm theo)</strong></p>
                                        <ul>
                                            <li>Bảo hành trong vòng 15 ngày (từ lúc Khách hàng mang sản phẩm đến bảo hành đến lúc nhận lại sản phẩm tối đa 15 ngày).</li>
                                            <li>Sản phẩm không bảo hành lại lần 2 trong 30 ngày kể từ ngày máy được bảo hành xong.</li>
                                            <li>Nếu TGDD/ĐMX vi phạm cam kết (bảo hành quá 15 ngày hoặc phải bảo hành lại sản phẩm lần nữa trong 30 ngày kể từ lần bảo hành trước), Khách hàng được áp dụng phương thức&nbsp;<strong>Hư gì đổi nấy ngay và luôn</strong>&nbsp;hoặc&nbsp;<strong>Hoàn tiền</strong>&nbsp;với mức phí giảm 50%.</li>
                                            <li>Từ tháng thứ 13 trở đi, không áp dụng bảo hành có cam kết, chỉ áp dụng bảo hành hãng nếu có.</li>
                                        </ul>
                                        <p><strong>Hư gì đổi nấy ngay &amp; luôn</strong></p>
                                        <ul>
                                            <li>Hư sản phẩm chính: Đổi sản phẩm mới (cùng model, cùng dung lượng, cùng màu sắc) miễn phí tháng đầu tiên, tháng thứ 2 đến tháng 12 chịu phí 10% hoá đơn/tháng. Nếu sản phẩm chính hết hàng thì áp dụng&nbsp;<strong>Bảo hành có cam kết</strong>&nbsp;hoặc&nbsp;<strong>Hoàn tiền</strong>&nbsp;với mức phí giảm 50%.</li>
                                            <li>Hư phụ kiện đi kèm: Đổi miễn phí trong vòng 12 tháng kể từ ngày mua sản phẩm chính bằng hàng phụ kiện TGDĐ/ĐMX đang kinh doanh mới với công năng tương đương. Nếu không có phụ kiện tương đương hoặc Khách hàng không thích thì áp dụng bảo hành hãng</li>
                                            <li>Lỗi phần mềm không áp dụng, mà chỉ khắc phục lỗi phần mềm.</li>
                                            <li>Trường hợp Khách hàng muốn đổi full box (nguyên thùng, nguyên hộp): ngoài việc áp dụng mức phí đổi trả thì Khách hàng sẽ trả thêm phí lấy full box tương đương 20% giá trị hóa đơn.</li>
                                        </ul>
                                        <p><strong>Hoàn tiền</strong></p>
                                        <ul>
                                            <li>Tháng đầu tiên kể từ ngày mua: phí 20% giá trị hóa đơn.</li>
                                            <li>Tháng thứ 2 đến tháng thứ 12: phí 10% giá trị hóa đơn/tháng.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="block-change">

                                <h3 class="" onclick="toggleBlock(this)">Sản phẩm không lỗi</h3>
                                <div class="content-insider ">
                                    <div>
                                        <p><strong>Khách hàng có thể trả hàng hoàn tiền:</strong></p>
                                        <ul>
                                            <li>Tháng đầu tiên kể từ ngày mua: phí 20% giá trị hóa đơn.</li>
                                            <li>Tháng thứ 2 đến tháng thứ 12: phí 10% giá trị hóa đơn/tháng.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="block-change">

                                <h3 class="" onclick="toggleBlock(this)">Sản phẩm lỗi do người dùng</h3>
                                <div class="content-insider ">
                                    <div>
                                        <p>Không bảo hành, đổi trả. TGDD/ĐMX hỗ trợ gửi hãng bảo hành, Khách hàng trả phí sửa chữa.</p>
                                    </div>
                                </div>
                            </div>
                            <p class="view-full">
                                Xem đầy đủ: <a href="https://www.thegioididong.com/chinh-sach-bao-hanh-san-pham" target="_blank">Chính sách bảo hành đổi trả</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>



            <div class="wrap_wrtp hide" id="popup-materialsfee">
                <div class="pop">

                </div>
            </div>


            <div class="box-available"></div>
            <div class="border7"></div>










            <div class="img-main">
                <img class=" lazyloaded" data-src="//cdn.tgdd.vn/Products/Images/42/218355/Kit/samsung-galaxy-note-20-note.jpg" alt="Điện thoại Samsung Galaxy Note 20" src="//cdn.tgdd.vn/Products/Images/42/218355/Kit/samsung-galaxy-note-20-note.jpg">
            </div>



            <div class="border7"></div>
            <div class="article content-t-wrap">
                <div class="article__content short">
                    <h3 class="article__content__title">Bài viết đánh giá</h3>
                    <h3>Tháng 8/2020,&nbsp;<a href="https://www.thegioididong.com/dtdd/samsung-galaxy-note-20" target="_blank" title="Tham khảo mẫu điện thoại tại Thế Giới Di Động" type="Tham khảo mẫu điện thoại tại Thế Giới Di Động">Galaxy Note 20</a>&nbsp;chính thức được lên kệ, với thiết kế camera trước nốt ruồi quen thuộc, cụm camera hình chữ nhật mới lạ cùng với vi xử lý Exynos 990 cao cấp của chính <a href="https://thegioididong.com/samsung" target="_blank" title="Tham khảo sản phẩm Samsung kinh doanh tại Thegioididong.com" type="Tham khảo sản phẩm Samsung kinh doanh tại Thegioididong.com">Samsung</a>&nbsp;chắc hẳn sẽ mang lại một trải nghiệm thú vị cùng hiệu năng mạnh mẽ.</h3>
                    <h3>Camera cụm hình chữ nhật độc đáo cùng thiết kế mạnh mẽ</h3>
                    <p><a href="https://www.thegioididong.com/dtdd" target="_blank" title="Tham khảo điện thoại kinh doanh tại Thế Giới Di Động">Điện thoại</a> sở hữu thiết kế khung kim loại chắc chắn, mặt lưng nhựa bóng bẩy, kiểu dáng mạnh mẽ với những góc cạnh vuông vức nhưng vẫn mang lại cảm giác cầm nắm thoải mái.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-235720-125702.jpg" onclick="return false;"><img alt="Thiết kế mặt lưng nhám sang trọng - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-235720-125702.jpg" class=" lazyloaded" title="Thiết kế mặt lưng nhám sang trọng - Samsung Galaxy Note 20" src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-235720-125702.jpg"></a></p>
                    <p>Camera của Galaxy Note 20&nbsp;được thiết kế trong cụm hình chữ nhật được đặt gọn ở phía sau bao gồm 1 camera chính 64 MP, camera góc siêu rộng 12 MP và camera tele 12 MP hỗ trợ người dùng dễ dàng lưu lại sắc nét những khoảng khắc đáng nhớ cùng gia đình và bạn bè.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-235520-015514.jpg" onclick="return false;"><img alt="Cụm 3 camera sau - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-235520-015514.jpg" class=" lazyloaded" title="Cụm 3 camera sau - Samsung Galaxy Note 20" src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-235520-015514.jpg"></a></p>
                    <p>Camera Galaxy Note 20 hỗ trợ zoom đến 30x ở chế độ chụp bình thường, trong khi đó, ở chế độ chụp ban đêm, máy vẫn hỗ trợ zoom đến 10x, chi tiết trong điều kiện thiếu sáng nhiều nhưng vẫn cho chất lượng hình tốt và chi tiết.</p>
                    <p></p>
                    <div class="slideArt">
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-224320-024303.jpg"><span>Ảnh chụp góc thường 1x trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-224320-024309.jpg"><span>Ảnh chụp zoom 5x trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-224320-024315.jpg"><span>Ảnh chụp zoom 10x trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                    </div>
                    <p></p>
                    <p>Với khả năng quay video chất lượng cao lên đến 8K siêu nét cùng các tính năng chống rung, lấy nét,... thì việc sở hữu nhiều thước phim xịn mịn bắt mắt là điều hoàn toàn dễ dàng trên chiếc smartphone này.</p>
                    <p></p>
                    <div class="slideArt">
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230720-020730.jpg"><span>Ảnh chụp camera sau trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230720-020723.jpg"><span>Ảnh chụp đêm trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                    </div>
                    <p></p>
                    <p>Samsung Note 20 sở hữu camera trước 10 MP trong thiết kế nốt ruồi quen thuộc giúp khung màn hình 6.7 inch được sử dụng tối đa giúp cho việc xem phim hay chơi game đã mắt và tập trung hơn.</p>
                    <p></p>
                    <div class="slideArt">
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230020-020036.jpg"><span>Camera trước 10 MP&nbsp;trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230720-020739.jpg"><span>Ảnh chụp camera selfie trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                        <div class="item"><img src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230720-020749.jpg"><span>Ảnh chụp camera selfie trên Galaxy Note 20</span></div>
                        <p></p>
                        <p></p>
                    </div>
                    <p></p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-230020-020036.jpg" onclick="return false;"><img alt="Camera trước 10 MP - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230020-020036.jpg" class="lazyload" title="Camera trước 10 MP - Samsung Galaxy Note 20"></a></p>
                    <h3><a href="https://www.thegioididong.com/hoi-dap/but-s-pen-dieu-lam-nen-ten-tuoi-dong-galaxy-note-839833" target="_blank" title="Bút SPen điều làm nên tên tuổi dòng Galaxy Note" type="Bút SPen điều làm nên tên tuổi dòng Galaxy Note">Bút S Pen</a>&nbsp;nhiều tiện ích, được nâng cấp nhiều tính năng hơn</h3>
                    <p>S Pen được xem là linh hồn của dòng&nbsp;Galaxy Note,&nbsp;siêu tiện lợi hỗ trợ ghi chú nhanh và đa tác vụ thông minh trên điện thoại như chụp ảnh màn hình, chụp cắt không gian bất kỳ trên màn hình, tạo ghi chú, phóng to / thu nhỏ hay hỗ trợ cử chỉ thông minh từ xa,...</p>
                    <p>Bên cạnh đó, ở Note 20, bút S Pen còn được cải tiến cho độ trễ giảm thiểu đáng kể cùng các cử chỉ điều khiển từ xa thông minh và tiện ích.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-125720-105754.jpg" onclick="return false;"><img alt="Bút S Pen kèm theo máy - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-125720-105754.jpg" class="lazyload" title="Bút S Pen kèm theo máy - Samsung Galaxy Note 20"></a></p>
                    <h3>Công nghệ màn hình <a href="https://www.thegioididong.com/hoi-dap/man-hinh-super-amoled-plus-la-gi-905774" target="_blank" title="Màn hình Super AMOLED Plus là gì?">Super AMOLED Plus</a>, độ phân giải Full HD+</h3>
                    <p>Màn hình Samsung Galaxy Note 20 sử dụng tấm nền Super AMOLED Plus 60Hz cho màu sắc chân thực, gần với thực tế mang đến những khung hình sống động và cực kì chi tiết, độ sáng màn hình cao hơn nên khi sử dụng dưới điều kiện nắng gắt cũng không ảnh hưởng nhiều đến chất lượng hiển thị.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-125820-105803.jpg" onclick="return false;"><img alt="Màn hình hiển thị siêu rõ nét - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-125820-105803.jpg" class="lazyload" title="Màn hình hiển thị siêu rõ nét - Samsung Galaxy Note 20"></a></p>
                    <p>Máy được tranh bị kính cảm ứng cường lực&nbsp;Gorilla Glass 5&nbsp;cùng&nbsp;chuẩn chống nước và chống bụi là IP68, giúp cho người dùng có thể yên tâm khi dùng điện thoại ngoài đường hay vô tình tiếp xúc với nước.</p>
                    <h3><a href="https://www.thegioididong.com/dtdd-bao-mat-van-tay" target="_blank" title="Tham khảo thêm về mẫu điện thoại có bảo mật vân tay" type="Tham khảo thêm về mẫu điện thoại có bảo mật vân tay">Bảo mật vân tay</a>&nbsp;trên màn hình nhanh chóng và tiện lợi</h3>
                    <p>Tính năng bảo mật tiên tiến chắc chắn không thể thiếu trên Note 20, với công nghệ mở khóa bằng vân tay ngay trên màn hình giúp bạn mở khóa nhanh máy chỉ với một lần chạm vô cùng nhanh chóng và tiện lợi.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-125820-105811.jpg" onclick="return false;"><img alt="Cài đặt bảo mật vân tay - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-125820-105811.jpg" class="lazyload" title="Cài đặt bảo mật vân tay - Samsung Galaxy Note 20"></a></p>
                    <h3>Hiệu năng mạnh mẽ chuẩn flagship</h3>
                    <p>Galaxy Note 20 còn có hiệu năng vượt trội với tốc độ xử lý mạnh mẽ, đáp ứng mọi thao tác tác vụ một cách nhanh chóng nhờ chip xử lý Exynos 990 8 nhân, RAM 8 GB/ROM 256 GB, người dùng có thể sử dụng nhiều tác vụ cùng lúc một cách dễ dàng, mượt mà.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-230420-020400.jpg" onclick="return false;"><img alt="Hiệu năng mạnh mẽ - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230420-020400.jpg" class="lazyload" title="Hiệu năng mạnh mẽ - Samsung Galaxy Note 20"></a></p>
                    <p>Hơn nữa, với điểm AnTuTu đạt gần 500.000 điểm, việc <a href="https://www.thegioididong.com/dtdd-choi-game" target="_blank" title="Tham khảo các mẫu điện thoại chơi game tại Thế Giới Di Động">điện thoại chơi game</a> ở mức cấu hình max setting&nbsp;là điều dễ dàng,&nbsp;hay render video cũng không làm khó được thiết bị này của Samsung.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-254320-094356.jpg" onclick="return false;"><img alt="Điểm số AnTutu - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-254320-094356.jpg" class="lazyload" title="Điểm số AnTutu - Samsung Galaxy Note 20"></a></p>
                    <h3>Dung lượng pin lớn, thoải mái sử dụng</h3>
                    <p>Note 20 sở hữu cho mình viên pin dung lượng 4300 mAh, tăng thời lượng sử dụng lên tới 23% so với thế hệ Note 10 trước.</p>
                    <p>Bên cạnh đó sản phẩm còn hỗ trợ sạc pin nhanh, với khả năng sạc nhanh 25w từ 0 đến 50% chỉ trong vòng 30 phút, giúp bạn rút ngắn được đáng kể thời gian chờ sạc cho thiết bị.</p>
                    <p><a class="preventdefault" href="https://www.thegioididong.com/images/42/218355/samsung-galaxy-note-20-230520-020535.jpg" onclick="return false;"><img alt="Pin lớn thoả sức sử dụng cả ngày dài - Samsung Galaxy Note 20" data-src="https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-230520-020535.jpg" class="lazyload" title="Pin lớn thoả sức sử dụng cả ngày dài - Samsung Galaxy Note 20"></a></p>
                    <p>Có thể thấy, Samsung Note 20 là một trong những siêu phẩm đáng sở hữu nhất trong năm 2020 với thiết kế thu hút ở cụm camera độc đáo, màu sắc mới lạ, màn hình siêu tràn viền cùng với những tính năng tiện ích nâng cấp của bút S Pen.</p>
                </div>
                <div class="bg-article"></div>
                <a href="javascript:void(0)" onclick="$('#thumb-article-gallery').trigger('click');" class="btn-detail jsArticle">
                    <span>Xem thêm</span>
                </a>
            </div>
            <div class="border7"></div>



            <div class="box-border">
                <div class="rating" id="danhgia">
                    <p class="rating__title">Đánh giá Điện thoại Samsung Galaxy Note 20</p>

                    <div class="rating-star left">
                        <div class="rating-left">
                            <div class="rating-top">
                                <p class="point">3</p>
                                <div class="list-star">
                                    <i class="icondetail-ratestar"></i>
                                    <i class="icondetail-ratestar"></i>
                                    <i class="icondetail-ratestar"></i>
                                    <i class="icondetail-darkstar"></i>
                                    <i class="icondetail-darkstar"></i>
                                </div>
                                <a href="/dtdd/samsung-galaxy-note-20/danh-gia" class="rating-total">118 đánh giá</a>

                            </div>
                            <ul class="rating-list">
                                <li>
                                    <div class="number-star">
                                        5
                                        <i class="icondetail-blackstar"></i>
                                    </div>
                                    <div class="timeline-star">
                                        <p class="timing" style="width: 34%"></p>
                                    </div>
                                    <a href="/dtdd/samsung-galaxy-note-20/danh-gia?s=5&amp;p=1" class="number-percent">34%</a>
                                </li>
                                <li>
                                    <div class="number-star">
                                        4
                                        <i class="icondetail-blackstar"></i>
                                    </div>
                                    <div class="timeline-star">
                                        <p class="timing" style="width: 19%"></p>
                                    </div>
                                    <a href="/dtdd/samsung-galaxy-note-20/danh-gia?s=4&amp;p=1" class="number-percent">19%</a>
                                </li>
                                <li>
                                    <div class="number-star">
                                        3
                                        <i class="icondetail-blackstar"></i>
                                    </div>
                                    <div class="timeline-star">
                                        <p class="timing" style="width: 19%"></p>
                                    </div>
                                    <a href="/dtdd/samsung-galaxy-note-20/danh-gia?s=3&amp;p=1" class="number-percent">19%</a>
                                </li>
                                <li>
                                    <div class="number-star">
                                        2
                                        <i class="icondetail-blackstar"></i>
                                    </div>
                                    <div class="timeline-star">
                                        <p class="timing" style="width: 9%"></p>
                                    </div>
                                    <a href="/dtdd/samsung-galaxy-note-20/danh-gia?s=2&amp;p=1" class="number-percent">9%</a>
                                </li>
                                <li>
                                    <div class="number-star">
                                        1
                                        <i class="icondetail-blackstar"></i>
                                    </div>
                                    <div class="timeline-star">
                                        <p class="timing" style="width: 19%"></p>
                                    </div>
                                    <a href="/dtdd/samsung-galaxy-note-20/danh-gia?s=1&amp;p=1" class="number-percent">19%</a>
                                </li>
                            </ul>


                        </div>

                    </div>

                    <div class="rating-img right">
                        <ul class="rating-img-list">
                            <li class="js-Showcmt" data-idx="1" onclick="goToRSlide(1)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/47718209/hinh-nen-dep-cho-dien-thoai-samsung-1BY7IT.jpg" alt="" src="https://cdn.tgdd.vn/comment/47718209/hinh-nen-dep-cho-dien-thoai-samsung-1BY7IT.jpg">
                            </li>
                            <li class="js-Showcmt" data-idx="2" onclick="goToRSlide(2)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/46435589/Screenshot_20210608-202638_DevicecareDVXOZ.jpg" alt="" src="https://cdn.tgdd.vn/comment/46435589/Screenshot_20210608-202638_DevicecareDVXOZ.jpg">
                            </li>
                            <li class="js-Showcmt" data-idx="3" onclick="goToRSlide(3)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/45721793/Screenshot_20210403-184617_YouTubeA6C1S.jpg" alt="" src="https://cdn.tgdd.vn/comment/45721793/Screenshot_20210403-184617_YouTubeA6C1S.jpg">
                            </li>
                            <li class="js-Showcmt" data-idx="4" onclick="goToRSlide(4)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/45013762/20201228_154506PAZRN.jpg" alt="" src="https://cdn.tgdd.vn/comment/45013762/20201228_154506PAZRN.jpg">
                            </li>
                            <li class="js-Showcmt" data-idx="5" onclick="goToRSlide(5)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/46578533/Screenshot_20210620-070407_DevicecareT29KZ.jpg" alt="" src="https://cdn.tgdd.vn/comment/46578533/Screenshot_20210620-070407_DevicecareT29KZ.jpg">
                            </li>
                            <li class="js-Showcmt" data-idx="6" onclick="goToRSlide(6)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/46547921/33FC3BE4-F73E-431F-A03A-0D0C89ED7CAFRLZPC.jpeg" alt="" src="https://cdn.tgdd.vn/comment/46547921/33FC3BE4-F73E-431F-A03A-0D0C89ED7CAFRLZPC.jpeg">
                            </li>
                            <li class="js-Showcmt" data-idx="7" onclick="goToRSlide(7)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/44351563/20201110_1137220AFOR.jpg" alt="" src="https://cdn.tgdd.vn/comment/44351563/20201110_1137220AFOR.jpg">
                            </li>
                            <li class="js-Showcmt" data-idx="8" onclick="goToRSlide(8)">
                                <img class=" lazyloaded" data-src="https://cdn.tgdd.vn/comment/43005639/20200824_144829V91C2.jpg" alt="" src="https://cdn.tgdd.vn/comment/43005639/20200824_144829V91C2.jpg">
                            </li>

                        </ul>
                    </div>


                </div>



                <div class="comment comment--all ratingLst">


                    <div class="comment__item par" id="r-47756995">
                        <div class="item-top">
                            <p class="txtname">Nguyễn Trọng Phát</p>

                            <p class="tickbuy">
                                <i class="icondetail-tickbuy"></i>
                                Đã mua tại TGDD
                            </p>

                        </div>
                        <div class="item-rate">
                            <div class="comment-star">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                            </div>


                        </div>

                        <div class="comment-content">
                            <p class="cmt-txt">Máy có vẻ yếu về sóng, wifi, pin....
                                Để không mà tốn pin hơn con s10+ đã dũng hơn 2 năm mà đang chia sẻ dldd.</p>
                        </div>




                        <div class="item-click">
                            <a href="javascript:likeRating(47756995);" class="click-like" data-like="0">
                                <i class="icondetail-likewhite"></i>
                                Hữu ích
                            </a>

                            <a href="javascript:showRatingCmtChild('r-47756995')" class="click-cmt">
                                <i class="icondetail-comment"></i>
                                <span class="cmtr">Thảo luận</span>
                            </a>
                            <a href="javascript:void(0)" class="click-use">
                                Đã dùng khoảng 4 tuần
                                <i class="icondetail-question"></i>

                                <div class="info-buying">
                                    <div class="info-buying-close"></div>
                                    <div class="info-buying-txt">
                                        <div class="txtitem">
                                            <p class="txt01">Mua ngày </p>
                                            <p class="txtdate">02/08/2021</p>
                                        </div>
                                        <div class="txtitem">
                                            <p class="txt01">Viết đánh giá</p>
                                            <p class="txtdate">01/09/2021</p>
                                        </div>
                                    </div>
                                    <div class="length-using">
                                        <div class="length-percent" style="width: 70%;"></div>
                                    </div>
                                    <p class="timeline-txt">Đã dùng <span>khoảng 4 tuần</span></p>
                                    <ul class="info-buying-list">
                                        <li><span></span>Ở thời điểm viết đánh giá, khách đã mua sản phẩm khoảng 4 tuần.</li>
                                        <li><span></span>Thời gian sử dụng thực tế có thể bằng hoặc ít hơn khoảng thời gian này.</li>
                                    </ul>
                                </div>

                            </a>

                            <div class="rr-47756995 reply hide">
                                <input placeholder="Nhập thảo luận của bạn">
                                <a href="javascript:ratingRelply(47756995);" class="rrSend">GỬI</a>
                                <div class="ifrl">
                                    <span>Khách</span> | <a href="javascript:rCmtEditName()">Nhập tên</a>
                                </div>
                            </div>

                        </div>

                    </div>



                    <div class="comment__item par" id="r-47731955">
                        <div class="item-top">
                            <p class="txtname">Lê văn quyền</p>

                            <p class="tickbuy">
                                <i class="icondetail-tickbuy"></i>
                                Đã mua tại TGDD
                            </p>

                        </div>
                        <div class="item-rate">
                            <div class="comment-star">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                            </div>


                        </div>

                        <div class="comment-content">
                            <p class="cmt-txt">Mọi người nói này nọ mình dùng hơn tháng thấy ok mà pin thì dc một ngày tùy vào ít hay nhiều chơi game lien quân thấy ok mà thấy nhiều người nói này nọ quá </p>
                        </div>




                        <div class="item-click">
                            <a href="javascript:likeRating(47731955);" class="click-like" data-like="0">
                                <i class="icondetail-likewhite"></i>
                                Hữu ích
                            </a>

                            <a href="javascript:showRatingCmtChild('r-47731955')" class="click-cmt">
                                <i class="icondetail-comment"></i>
                                <span class="cmtr">1 thảo luận</span>
                            </a>
                            <a href="javascript:void(0)" class="click-use">
                                Đã dùng khoảng 1 tháng
                                <i class="icondetail-question"></i>

                                <div class="info-buying">
                                    <div class="info-buying-close"></div>
                                    <div class="info-buying-txt">
                                        <div class="txtitem">
                                            <p class="txt01">Mua ngày </p>
                                            <p class="txtdate">26/07/2021</p>
                                        </div>
                                        <div class="txtitem">
                                            <p class="txt01">Viết đánh giá</p>
                                            <p class="txtdate">30/08/2021</p>
                                        </div>
                                    </div>
                                    <div class="length-using">
                                        <div class="length-percent" style="width: 70%;"></div>
                                    </div>
                                    <p class="timeline-txt">Đã dùng <span>khoảng 1 tháng</span></p>
                                    <ul class="info-buying-list">
                                        <li><span></span>Ở thời điểm viết đánh giá, khách đã mua sản phẩm khoảng 1 tháng.</li>
                                        <li><span></span>Thời gian sử dụng thực tế có thể bằng hoặc ít hơn khoảng thời gian này.</li>
                                    </ul>
                                </div>

                            </a>
                            <div class="box-history">
                                <i class="icondetail-dots icon-dots"></i>
                                <p class="txt-dots" onclick="rtHis(47731955)">Xem lịch sử đánh giá</p>
                            </div>

                            <div class="rr-47731955 reply hide">
                                <input placeholder="Nhập thảo luận của bạn">
                                <a href="javascript:ratingRelply(47731955);" class="rrSend">GỬI</a>
                                <div class="ifrl">
                                    <span>Khách</span> | <a href="javascript:rCmtEditName()">Nhập tên</a>
                                </div>
                            </div>

                        </div>

                    </div>



                    <div class="comment__item rp-47731955 childC__item hide" id="r-47733704">
                        <div class="item-top">
                            <p class="txtname">Nguyễn Đình Thắng</p>

                            <span class="qtv">QTV</span>

                        </div>
                        <div class="item-rate">


                        </div>

                        <div class="comment-content">
                            <p class="cmt-txt">Chào anh.<br>Bên em ghi nhận đánh giá của mình và sẽ chuyển đến bộ phận liên quan ạ&nbsp;<br>Chúc anh một ngày vui vẻ</p>
                        </div>




                        <div class="item-click">
                            <a href="javascript:likeRating(47733704);" class="click-like" data-like="0">
                                <i class="icondetail-likewhite"></i>
                                Hữu ích
                            </a>


                            <div class="rr-47733704 reply hide">
                                <input placeholder="Nhập thảo luận của bạn">
                                <a href="javascript:ratingRelply(47733704);" class="rrSend">GỬI</a>
                                <div class="ifrl">
                                    <span>Khách</span> | <a href="javascript:rCmtEditName()">Nhập tên</a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>



                <div class="comment-btn">

                    <a href="/dtdd/samsung-galaxy-note-20/danh-gia" class="comment-btn__item blue">Xem tất cả đánh giá</a>
                    <a href="javascript:showInputRating();" class="comment-btn__item">Viết đánh giá</a>
                </div>

                <div class="read-assess hide">
                    <form class="input frtip" name="fRatingComment">
                        <input type="hidden" name="hdfStar" id="hdfStar" value="0">
                        <input type="hidden" name="hdfRtIsMobile" id="hdfRtIsMobile" value="false">
                        <input type="hidden" name="hdfProductID" id="hdfProductID" value="218355">
                        <input type="hidden" name="hdfIsShare" id="hdfIsShare" value="0">
                        <input type="hidden" name="hdfSiteID" id="hdfSiteID" value="1">
                        <input type="hidden" name="hdfRtImg" id="hdfRtImg" class="hdfRtImg" value="">
                        <input type="hidden" name="hdfRtLink" id="hdfRtLink" value="https://www.thegioididong.com/dtdd/samsung-galaxy-note-20?src=osp">

                        <div class="read-assess__top">
                            <p class="read-assess__title">Đánh giá </p>
                            <div class="read-assess-close btn-closemenu" onclick="hideInputRating()">Đóng</div>
                        </div>

                        <div class="read-assess-main">

                            <div class="read-assess-form">
                                <div class="textarea">
                                    <textarea class="ct" name="fRContent" placeholder="Mời bạn chia sẻ thêm một số cảm nhận ..." onkeyup="countTxtRating()"></textarea>
                                    <div class="textarea-bottom clearfix">
                                        <a href="javascript:void(0)" class="send-img">
                                            <i class="icondetail-camera"></i>
                                            Gửi hình chụp thực tế
                                        </a>
                                        <input id="hdFileRatingUpload" type="file" class="hide" accept="image/x-png, image/gif, image/jpeg">
                                        <p class="mintext">0 ký tự (tối thiểu 80)</p>
                                        <ul class="resRtImg hide">
                                        </ul>
                                    </div>
                                </div>


                                <div class="select-star">
                                    <p class="txt01">Bạn cảm thấy sản phẩm này như thế nào? (chọn sao nhé):</p>
                                    <ul class="ul-star">
                                        <li data-val="1">
                                            <i class="icondetail-sltstar"></i>
                                            <p>Rất tệ</p>
                                        </li>
                                        <li data-val="2">
                                            <i class="icondetail-sltstar"></i>
                                            <p>Tệ</p>
                                        </li>
                                        <li data-val="3">
                                            <i class="icondetail-sltstar"></i>
                                            <p>Bình thường</p>
                                        </li>
                                        <li data-val="4">
                                            <i class="icondetail-sltstar"></i>
                                            <p>Tốt</p>
                                        </li>
                                        <li data-val="5">
                                            <i class="icondetail-sltstar"></i>
                                            <p>Rất tốt</p>
                                        </li>
                                    </ul>

                                </div>

                                <div class="txt-agree">
                                    <span></span>
                                    <p>Tôi sẽ giới thiệu sản phẩm này cho bạn bè người thân</p>
                                </div>

                                <div class="list-input">
                                    <input type="text" name="fRName" class="input-assess" placeholder="Họ và tên (bắt buộc)">
                                    <input type="text" name="fRPhone" class="input-assess" placeholder="Số điện thoại (bắt buộc)">
                                    <input type="text" name="fREmail" class="input-assess" placeholder="Email (không bắt buộc)">

                                </div>


                                <a class="submit-assess" href="javascript:submitRatingComment()">Gửi đánh giá ngay</a>
                                <span class="lbMsgRt hide"></span>

                                <p class="assess-txtbt">Để đánh giá được duyệt, quý khách vui lòng tham khảo <a href="/huong-dan-dang-binh-luan">Quy định duyệt đánh giá</a></p>
                            </div>

                        </div>


                    </form>
                </div>

                <div class="rRepPopup hide">
                    <div class="rRepPopup__top">
                        <p class="rRepPopup__title">Nhập thông tin</p>
                        <div class="rRepPopup-close c-btnclose" onclick="hideReplyConfirmPopup()">Đóng</div>
                    </div>
                    <div class="rCfmInfo" data-gender="">
                        <div class="cgd ed">
                            <span class="c_male" onclick="rSelGender(this, 1)"><i class="icondetail-rad"></i>&nbsp;Anh</span>
                            <span class="c_female" onclick="rSelGender(this, 2)"><i class="icondetail-rad"></i>&nbsp;Chị</span>
                        </div>
                        <input type="text" placeholder="Họ tên (bắt buộc)" class="cfmUserName">
                        <input type="text" placeholder="Email (để nhận phản hồi qua mail)" class="cfmUserEmail">
                        <input type="text" placeholder="Số điện thoại" class="cfmPhone">
                        <button type="submit" onclick="rCmtConfirmUser(true, true);">Cập nhật</button>
                    </div>
                </div>


                <div class="show-comment">
                    <div class="show-comment-top">
                        <p class="show-comment-title" onclick="showRtImgListPop()">
                            <i class="icondetail-window"></i>
                            Xem tất cả ảnh
                        </p>
                        <div class="comment-close btn-closemenu" onclick="closeRtGallery()">Đóng</div>
                    </div>

                    <div class="show-comment-main">
                        <div id="cmt_sync1" class="owl-carousel cmt-item-left">
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/47718209/hinh-nen-dep-cho-dien-thoai-samsung-1BY7IT.jpg" alt="">
                            </div>
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/46435589/Screenshot_20210608-202638_DevicecareDVXOZ.jpg" alt="">
                            </div>
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/45721793/Screenshot_20210403-184617_YouTubeA6C1S.jpg" alt="">
                            </div>
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/45013762/20201228_154506PAZRN.jpg" alt="">
                            </div>
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/46578533/Screenshot_20210620-070407_DevicecareT29KZ.jpg" alt="">
                            </div>
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/46547921/33FC3BE4-F73E-431F-A03A-0D0C89ED7CAFRLZPC.jpeg" alt="">
                            </div>
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/44351563/20201110_1137220AFOR.jpg" alt="">
                            </div>
                            <div class="cmt-img">
                                <img class="owl-lazy" data-src="https://cdn.tgdd.vn/comment/43005639/20200824_144829V91C2.jpg" alt="">
                            </div>

                        </div>

                        <div id="cmt_sync2" class="owl-carousel cmt-item-right">
                            <div class="comment__item" data-id="239940">
                                <div class="item-top">
                                    <p class="txtname">Duong</p>


                                    <p class="tickbuy">
                                        <i class="icondetail-tickbuy"></i>
                                        Đã mua tại TGDD
                                    </p>


                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star-dark"></i>
                                        <i class="icon-star-dark"></i>
                                        <i class="icon-star-dark"></i>
                                        <i class="icon-star-dark"></i>
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-47718209" data-id="47718209">
                                    <p class="cmt-txt">Mạng tệ quá. Sóng cực yếu cùng 1 vị trí ví von và iPhone note 20 . 2 máy kia full máy note dc 2-3 vạch</p>
                                </div>


                                <p class="support">
                                    <i class="icondetail-logoyl"></i>
                                    Đánh giá này đã được chuyển tới bộ phận hỗ trợ
                                </p>

                            </div>
                            <div class="comment__item" data-id="224611">
                                <div class="item-top">
                                    <p class="txtname">Chu du</p>


                                    <p class="tickbuy">
                                        <i class="icondetail-tickbuy"></i>
                                        Đã mua tại TGDD
                                    </p>


                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star-dark"></i>
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-46435589" data-id="46435589">
                                    <p class="cmt-txt hideRtCt">Mới mua về pin khá tệ sau đó mình cho ứng dụng ngủ sâu như trong hình tức là k cho UD chạy ngầm thì thấy khá là hiệu quả,pin từ lúc 7h sáng đi làm là 96 %, zalo, whatapp, fb liên tục, xem ít youtube, nghịch ít game đến tối pin còn 30% khá là thoải mái...còn k biết mọi ng dùng như nào có thể hao pin hơn nhưng vs mình từ sáng đi làm đến tối sạc là ok đủ dùng cho nhi cầu</p>
                                </div><span class="sRtXt sRtXt46435589" onclick="showFullRtCtPop('46435589')">Xem tiếp ▾ </span>



                            </div>
                            <div class="comment__item" data-id="214646">
                                <div class="item-top">
                                    <p class="txtname">Binh</p>


                                    <p class="tickbuy">
                                        <i class="icondetail-tickbuy"></i>
                                        Đã mua tại TGDD
                                    </p>


                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star-dark"></i>
                                        <i class="icon-star-dark"></i>
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-45721793" data-id="45721793">
                                    <p class="cmt-txt hideRtCt">Mới mua đc 5 ngày nhưng vào xem video trực tiếp youtube thì thường mất 5s đầu là hình ảnh nhảy loạn xạ vỡ ảnh sau 5s mới quay lại với video bình thường</p>
                                </div><span class="sRtXt sRtXt45721793" onclick="showFullRtCtPop('45721793')">Xem tiếp ▾ </span>


                                <p class="support">
                                    <i class="icondetail-logoyl"></i>
                                    Hỗ trợ kỹ thuật đã liên hệ hỗ trợ ngày 04/04/2021
                                </p>

                            </div>
                            <div class="comment__item" data-id="204771">
                                <div class="item-top">
                                    <p class="txtname">Nguyễn Hương</p>


                                    <p class="tickbuy">
                                        <i class="icondetail-tickbuy"></i>
                                        Đã mua tại TGDD
                                    </p>


                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star-dark"></i>
                                    </div>
                                    <div class="intro-future">
                                        <i class="icondetail-heart"></i>
                                        Sẽ giới thiệu cho bạn bè, người thân
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-45013762" data-id="45013762">
                                    <p class="cmt-txt hideRtCt">Không biết mọi người yêu cầu về sp note 20 này cao quá hay sao. Chứ cá nhân mình, về camera mình thấy chụp rất ổn, đẹp nếu bạn biết sử dụng tối đa các chức năng của nó, thiết kế đối với 1 ng ko thích màn cong như mình thì mình nhận thấy con này đẹp, mình k xài ốp, chỉ dán keo trong vì mình thích màu xanh của vỏ máy, tuy chất liệu nhựa nhưng nếu k ai biết thì k ai nghĩ nó nhựa đâu, vì bóng loáng và đẹp mà. Kích thướt vừa với bàn tay mình, mỏng hợp lý. Chip exynos hơi ngu 1 tí, nhưng vs 1 đứa chơi game PUBG và Liên Quân như mình thì cảm nhận là nó vẫn mướt mườn mượt. Mình chỉ k hài lòng về Pin thôi, lúc dùng camera nhiều pin sẽ nóng, mình hơi khó chịu tí, với pin ko trâu như mình nghĩ, full 100% pin thì mình dùng đc khoảng 10 tiếng nếu lướt fb và cả game. Và điều cuối nữa là mình cực thích cây bút nhạy hơn nhiều so vs note 10, trùng màu xanh vs vỏ cực thích luôn. Kèm theo ảnh mình chụp bằng cam thường cho mấy bạn bảo camera xấu nhận xét nha. Vì bạn mình thấy ảnh mình chụp đẹp nên mình cũng giới thiệu bạn mua 1 cái nè. </p>
                                </div><span class="sRtXt sRtXt45013762" onclick="showFullRtCtPop('45013762')">Xem tiếp ▾ </span>



                            </div>
                            <div class="comment__item" data-id="226640">
                                <div class="item-top">
                                    <p class="txtname">Nguyễn Minh Nhựt</p>




                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-46578533" data-id="46578533">
                                    <p class="cmt-txt hideRtCt">Pin ko hề lởm tí nào nha mn do mấy người ở trên ko biết gì hết, máy mới mua lúc nào chả thế, máy chưa quen với thói quen sử dụng của mình nên pin hao nhanh là đúng, em dùng đc tầm 2 tuần đâu vào đó rồi thì dùng liên tục cũng hơn 6 tiếng </p>
                                </div><span class="sRtXt sRtXt46578533" onclick="showFullRtCtPop('46578533')">Xem tiếp ▾ </span>



                            </div>
                            <div class="comment__item" data-id="226274">
                                <div class="item-top">
                                    <p class="txtname">Tài Nguyễn</p>




                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                    </div>
                                    <div class="intro-future">
                                        <i class="icondetail-heart"></i>
                                        Sẽ giới thiệu cho bạn bè, người thân
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-46547921" data-id="46547921">
                                    <p class="cmt-txt hideRtCt">Pin trâu. Onl liên tục phải mất 5-6 tiếng mới hết. Xài đến 25% là sạc k đến 1 tiếng là đầy. Chơi game ổn định mặc dù mình chơi khá nhiều(liên quân) và chỉnh đồ họa gần như là cao nhất. Màu xanh đẹp và khá lạ mắt </p>
                                </div><span class="sRtXt sRtXt46547921" onclick="showFullRtCtPop('46547921')">Xem tiếp ▾ </span>



                            </div>
                            <div class="comment__item" data-id="196591">
                                <div class="item-top">
                                    <p class="txtname">Đặng Kim Dung</p>




                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                    </div>
                                    <div class="intro-future">
                                        <i class="icondetail-heart"></i>
                                        Sẽ giới thiệu cho bạn bè, người thân
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-44351563" data-id="44351563">
                                    <p class="cmt-txt hideRtCt">Tuyệt vời quá. Thích nhất màu đồng nhìn sang gì đâu á. Chương trình tốt nên mình đã hốt ngay 1 em mà còn mãi mới có hàng😊
                                        Chụp ảnh phải nói là mê li các bác ạ. Nếu để nói chụp phong cảnh thì k 1 hãng nào cạnh tranh được với nhà Sam😍</p>
                                </div><span class="sRtXt sRtXt44351563" onclick="showFullRtCtPop('44351563')">Xem tiếp ▾ </span>



                            </div>
                            <div class="comment__item" data-id="181738">
                                <div class="item-top">
                                    <p class="txtname">Phạm thị hồng mến</p>




                                </div>
                                <div class="item-rate">
                                    <div class="comment-star">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star-dark"></i>
                                        <i class="icon-star-dark"></i>
                                        <i class="icon-star-dark"></i>
                                    </div>
                                </div>
                                <div class="comment-content noimg crtipu-43005639" data-id="43005639">
                                    <p class="cmt-txt hideRtCt">Mình vừa lấy ss note 20 ultra đc 3 ngày. Tất cả đều ok. Riêng phần chức năng chụp ảnh mình rất thất vọng. Mih là ng chụp cận cảnh rất nhìu mà từ khi dùng ss note 20 ultra là k chụp đc gì cả. Hình ảnh bị nhòe. Dung chức năng zoom tầm 15p thì máy rất rất là nóng. Chụp ảnh còn thua cả ss note 10 plus mình xài trc kia. Mong bên Samsung sẽ có biện pháp hoặc trả lời ra sao. </p>
                                </div><span class="sRtXt sRtXt43005639" onclick="showFullRtCtPop('43005639')">Xem tiếp ▾ </span>



                            </div>

                        </div>

                    </div>
                </div>
                <div class="gallery">
                    <div class="gallery__top">
                        <p class="gallery__title">11 ảnh từ khách hàng</p>
                        <div class="gallery-close btn-closemenu" onclick="closeRtGallery()">Đóng</div>
                    </div>
                    <div class="gallery__tab">
                        <a href="#all" class="act">Tất cả</a>
                        <a href="#sao5">5 sao</a>
                        <a href="#sao4">4 sao</a>
                        <a href="#sao3">3 sao</a>
                        <a href="#sao2">2 sao</a>
                        <a href="#sao1">1 sao</a>
                    </div>
                    <ul class="gallery__content">
                        <li class="sao1 rtAtt-239940" onclick="goToRSlide(1)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/47718209/hinh-nen-dep-cho-dien-thoai-samsung-1BY7IT.jpg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                            </div>
                        </li>
                        <li class="sao4 rtAtt-224611" onclick="goToRSlide(2)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/46435589/Screenshot_20210608-202638_DevicecareDVXOZ.jpg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star-dark"></i>
                            </div>
                        </li>
                        <li class="sao3 rtAtt-214646" onclick="goToRSlide(3)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/45721793/Screenshot_20210403-184617_YouTubeA6C1S.jpg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                            </div>
                        </li>
                        <li class="sao4 rtAtt-204771" onclick="goToRSlide(4)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/45013762/20201228_154506PAZRN.jpg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star-dark"></i>
                            </div>
                        </li>
                        <li class="sao5 rtAtt-226640" onclick="goToRSlide(5)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/46578533/Screenshot_20210620-070407_DevicecareT29KZ.jpg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                            </div>
                        </li>
                        <li class="sao5 rtAtt-226274" onclick="goToRSlide(6)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/46547921/33FC3BE4-F73E-431F-A03A-0D0C89ED7CAFRLZPC.jpeg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                            </div>
                        </li>
                        <li class="sao5 rtAtt-196591" onclick="goToRSlide(7)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/44351563/20201110_1137220AFOR.jpg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                            </div>
                        </li>
                        <li class="sao2 rtAtt-181738" onclick="goToRSlide(8)">
                            <img class="lazyload" data-src="https://cdn.tgdd.vn/comment/43005639/20200824_144829V91C2.jpg" alt="">
                            <div class="gallery-rating">
                                <i class="icon-star"></i>
                                <i class="icon-star"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                                <i class="icon-star-dark"></i>
                            </div>
                        </li>

                    </ul>
                </div>






            </div>





        </div>
        <div class="box_right">








            <div class="box04 box_normal">

                <div class="box04__txt">
                    Giá tại <a href="javascript:;" onclick="OpenLocation()">Hồ Chí Minh</a>
                </div>


                <div class="price-one">
                    <div class="box-price">
                        <p class="box-price-present">14.990.000₫ </p>
                        <p class="box-price-old">23.990.000₫</p>
                        <p class="box-price-percent">-37%</p>


                    </div>
                </div>















                <div class="block block-price1">


                    <div class="block__promo">
                        <div class="pr-top">
                            <p class="pr-txtb">Khuyến mãi trị giá 300.000₫</p>
                            <i class="pr-txt">Giá và khuyến mãi dự kiến áp dụng đến 23:00 15/09</i>
                        </div>
                        <div class="pr-content">
                            <div class="pr-item">
                                <div class="divb t1" data-promotion="776041" data-group="Tặng" data-discount="0" data-productcode="9090000004500       " data-tovalue="300000">
                                    <span class="nb">1</span>
                                    <div class="divb-right">
                                        <p><span class="red">Mua online thêm quà: </span>Tặng Phiếu mua hàng Gia dụng trị giá 300,000đ cho Khách hàng tại các tỉnh thành áp dụng chỉ thị 16 <a href="http://www.thegioididong.com/tin-tuc/chuong-trinh-khuyen-mai-tang-pmh-cac-tinh-mien-nam-1369801"> Xem chi tiết</a></p>
                                    </div>
                                </div>
                                <div class="divb t2" data-promotion="736367" data-group="WebNote" data-discount="0" data-productcode="" data-tovalue="1000">
                                    <span class="nb">2</span>
                                    <div class="divb-right">
                                        <p>Trả góp 0% thẻ tín dụng <a href="http://www.thegioididong.com/tin-tuc/mua-dien-thoai-samsung-galaxy-tra-gop-0-1364972"> Xem chi tiết</a></p>
                                    </div>
                                </div>
                                <div class="divb t2" data-promotion="771261" data-group="WebNote" data-discount="0" data-productcode="" data-tovalue="0">
                                    <span class="nb">3</span>
                                    <div class="divb-right">
                                        <p>Nhập mã DMX100 giảm 3% tối đa 100.000đ khi thanh toán quét QRcode qua App của ngân hàng <a href="http://www.thegioididong.com/tin-tuc/nhap-ma-giam-gia-vnpay-1377917" target="_blank">(click xem chi tiết)</a></p>
                                    </div>
                                </div>
                            </div>






                        </div>
                    </div>



                    <script>
                        var dataDeli = {};
                        dataDeli.ProductId = 218355;
                        dataDeli.ProductCode = "";
                        dataDeli.ProvinceId = 3;
                        dataDeli.ProvinceName = "";
                        dataDeli.DistrictId = 0;
                        dataDeli.DistrictName = "";
                        dataDeli.WardId = 0;
                        dataDeli.WardName = "";
                        dataDeli.Address = "";
                        dataDeli.IsCheckSubmit = true;
                    </script>
                    <span class="online-only">Hàng phải chuyển về</span>
                    <script>
                        setTimeout(function() {
                            document.querySelectorAll(".block-button").forEach(function(item) {
                                item.classList.remove('hide');
                            });
                            document.querySelectorAll(".isshowdelivery").forEach(function(item) {
                                item.classList.remove('hide');
                            });
                            document.querySelectorAll(".campaign>div>div>a").forEach(function(item) {
                                item.classList.remove('hide');
                            });
                        }, 100);
                    </script>





                    <div class="campaign c1 dt">
                        <b>Ưu đãi khi thanh toán</b>
                        <div>
                            <div>
                                <figure class="fecredit"><img src="//cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/logo/fecredit.png"></figure>
                                <b>Giảm <b>200.000₫</b></b>
                                <span>Sản phẩm từ 1Tr</span>
                                <div>
                                    <i onmouseover="ShowRuleCampaign($(this), 'fecredit')" onmouseout="HideRuleCampaign($(this))">?</i>

                                    <div id="fecredit">
                                        <p dir="ltr" role="presentation" style="font-variant-numeric: normal; font-variant-east-asian: normal; list-style-type: disc; font-size: 10pt; font-family: Roboto, sans-serif; color: rgb(36, 36, 36); background-color: transparent; vertical-align: baseline; white-space: pre; line-height: 1.848; margin-top: 6pt; margin-bottom: 0pt;"><span id="docs-internal-guid-de1a29cb-7fff-aa62-5a1b-eeb503c645dd"><span style="font-size: 10pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">- Giảm tối đa 200.000đ cho sản phẩm có giá thanh toán cuối cùng từ 1.000.000đ trở lên khi thanh toán qua Thẻ tín dụng FE.</span></span></p>
                                        <p dir="ltr" role="presentation" style="font-variant-numeric: normal; font-variant-east-asian: normal; list-style-type: disc; font-size: 10pt; font-family: Roboto, sans-serif; color: rgb(36, 36, 36); background-color: transparent; vertical-align: baseline; white-space: pre; line-height: 1.848; margin-top: 0pt; margin-bottom: 0pt;"><span id="docs-internal-guid-de1a29cb-7fff-aa62-5a1b-eeb503c645dd"><span style="font-size: 10pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">- Thời gian diễn ra chương trình: Từ ngày 23/8&nbsp; - 30/9/2021 (Có thể kết thúc sớm nếu hết ngân sách)<br>- Áp dụng cho thẻ tín dụng FE với các đầu BIN: </span></span>539597, 539146, 356554</p>
                                        <p dir="ltr" role="presentation" style="font-variant-numeric: normal; font-variant-east-asian: normal; list-style-type: disc; font-size: 10pt; font-family: Roboto, sans-serif; color: rgb(36, 36, 36); background-color: transparent; vertical-align: baseline; white-space: pre; line-height: 1.848; margin-top: 0pt; margin-bottom: 0pt;"><span id="docs-internal-guid-de1a29cb-7fff-aa62-5a1b-eeb503c645dd"><span style="font-size: 10pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">- Mỗi chủ thẻ được sử dụng ưu đãi 1 lần/1 ngày.</span></span></p>
                                        <p dir="ltr" role="presentation" style="font-variant-numeric: normal; font-variant-east-asian: normal; list-style-type: disc; font-size: 10pt; font-family: Roboto, sans-serif; color: rgb(36, 36, 36); background-color: transparent; vertical-align: baseline; white-space: pre; line-height: 1.848; margin-top: 0pt; margin-bottom: 0pt;"><span id="docs-internal-guid-de1a29cb-7fff-aa62-5a1b-eeb503c645dd"><span style="font-size: 10pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">- Không áp dụng trả góp qua thẻ tín dụng FE.</span></span></p>
                                        <p dir="ltr" role="presentation" style="font-variant-numeric: normal; font-variant-east-asian: normal; list-style-type: disc; font-size: 10pt; font-family: Roboto, sans-serif; color: rgb(36, 36, 36); background-color: transparent; vertical-align: baseline; white-space: pre; line-height: 1.848; margin-top: 0pt; margin-bottom: 0pt;"><span id="docs-internal-guid-de1a29cb-7fff-aa62-5a1b-eeb503c645dd"><span style="font-size: 10pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">- Không áp dụng kèm các khuyến mãi qua ngân hàng hoặc cổng thanh toán khác.</span></span></p>
                                    </div>
                                </div>
                                <a href="javascript:;" class="" data-siteid="1" onclick="CheckApply($(this), '/cart/single?productId=218355&amp;productCode=0131491002147&amp;gtype=fecredit', 1000000)">Mua ngay</a>
                                <div class="cpopup hide fecredit" onclick="$(this).addClass('hide')">
                                    <div>
                                        Chương trình giảm thêm 200.000₫ khi thanh toán qua FECredit chỉ áp dụng cho đơn hàng <b>từ 1.000.000₫ trở lên</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="crule hide"></div>
                    </div>
                    <script>
                        function ShowRuleCampaign($this, element) {
                            if ($this.hasClass('act')) {
                                $this.removeClass('act');
                                $('.crule').toggleClass('hide');
                            } else {
                                $('.crule').removeClass('hide');
                                $('.campaign>div>div>div>i.act').removeClass('act');
                                $this.toggleClass('act');
                            }

                            $('.crule').html($('#' + element).html());
                        }

                        function HideRuleCampaign($this) {
                            $this.removeClass('act');
                            $('.crule').toggleClass('hide');
                        }

                        function CheckApply($this, url, min) {
                            if ($('.choosepromo .label-radio.active').length > 0 && min > 0) {
                                var price = parseInt(23990000);
                                var discount = parseInt(9000000);
                                $('.choosepromo .label-radio.active').each(function() {
                                    var $parent = $(this).parent();
                                    var value = parseInt($parent.data('discount'));
                                    if (value > 0) {
                                        discount += $parent.data('ispercent') == 'True' ? (value * price / 100) : value;
                                    }
                                });

                                if (price - discount < min) {
                                    $this.next().removeClass('hide');
                                } else {
                                    //if ($this.data('siteid')==2) {
                                    //    gtm_trakking(url);
                                    //}
                                    window.location.href = url;
                                }
                            } else {
                                //if ($this.data('siteid') == 2) {
                                //    gtm_trakking(url);
                                //}
                                window.location.href = url;
                            }
                        }

                        function gtm_trakking(url) {
                            var obj = getModelTracking();
                            obj.orderType = "Mua ngay Vnpay";
                            var obj = getModelTracking();
                            if (url.indexOf("vnpayqr") != -1) {
                                obj.productPromoyionType = "Khuyến mãi thường";
                                obj.orderType = "Mua ngay Vnpay";
                            } else {
                                if (url.indexOf("onepayjcb") != -1) {
                                    obj.orderType = "Mua ngay Onepay JCB";
                                } else {
                                    if (url.indexOf("fecredit") != -1) {
                                        obj.orderType = "Mua ngay FeCredit";
                                    } else {
                                        obj.orderType = "Mua ngay Tpbank";
                                    }
                                }
                            }
                            gtm_ProductAddtoCart(obj);
                        }
                    </script>
                    <style>
                        .campaign {
                            margin-bottom: 10px;
                            padding-top: 12px;
                            border-top: 1px solid #E0E0E0;
                            white-space: nowrap;
                            position: relative;
                        }

                        .campaign>b {
                            display: block;
                            margin-bottom: 10px;
                            font-size: 16px;
                        }

                        .campaign>div>div {
                            width: calc(30% - 3px);
                            display: inline-block;
                            vertical-align: top;
                            padding: 10px 10px 8px 10px;
                            border: 1px solid #e0e0e0;
                            border-radius: 4px;
                            position: relative;
                        }

                        .campaign>div>div>* {
                            display: block;
                            margin-bottom: 2px;
                        }

                        .campaign>div>div>figure {
                            height: 16px;
                            position: relative;
                            margin-bottom: 6px;
                        }

                        .campaign>div>div>figure>img {
                            max-height: 100%;
                            position: absolute;
                            top: 0;
                            bottom: 0;
                            left: 0;
                            margin: auto;
                        }

                        .campaign>div>div>figure.jcb>img {
                            height: 15px;
                        }

                        .campaign>div>div>figure.tpbank>img {
                            height: 13px;
                        }

                        .campaign>div>div>figure.fecredit>img {
                            height: 8px;
                        }

                        .campaign>div>div>figure.vnpayqr>img {
                            height: 10px;
                        }

                        .campaign>div>div>b {
                            color: #2F80ED;
                            font-weight: normal;
                        }

                        .campaign>div>div>span {
                            font-size: 12px;
                            color: #666;
                        }

                        .campaign>div>div>a {
                            font-weight: normal;
                            color: #FB6E2E;
                            margin-bottom: 0;
                        }

                        .campaign>div>div>div {
                            width: 16px;
                            height: 16px;
                            position: absolute;
                            right: 10px;
                            top: 10px;
                        }

                        .campaign>div>div>div>i {
                            font-style: normal;
                            width: 16px;
                            height: 16px;
                            border-radius: 50%;
                            display: block;
                            background-color: #999;
                            color: #fff;
                            font-size: 10px;
                            text-align: center;
                            line-height: 16px;
                            position: relative;
                            cursor: pointer;
                        }

                        .campaign>div>div>div>i.act {
                            background-color: #2F80ED;
                        }

                        .campaign>div>div>div>i.act:before {
                            content: "";
                            position: absolute;
                            width: 12px;
                            height: 12px;
                            background: #e0e0e0;
                            transform: rotate(45deg);
                            top: 15px;
                            right: 2px;
                            box-shadow: -2px -2px 5px -4px;
                        }

                        .campaign>div>div>div>div {
                            display: none;
                        }

                        .campaign div.crule {
                            position: absolute;
                            right: 0;
                            top: 70px;
                            padding: 10px;
                            border-radius: 4px;
                            width: 100%;
                            background-color: #fff;
                            border: 1px solid #E0E0E0;
                            z-index: 5;
                            white-space: normal;
                            line-height: 1.5;
                            box-shadow: 0 0 8px #ccc;
                        }

                        .campaign div.crule>* {
                            margin-bottom: 10px;
                            color: #333;
                            white-space: normal;
                        }

                        .campaign .cpopup {
                            position: fixed !important;
                            left: 0 !important;
                            top: 0 !important;
                            width: 100%;
                            height: 100vh;
                            background: rgba(0, 0, 0, .85);
                            z-index: 10;
                        }

                        .campaign .cpopup>div {
                            width: 320px;
                            position: absolute;
                            left: 0;
                            right: 0;
                            top: 0;
                            bottom: 0;
                            margin: auto;
                            background-color: #fff;
                            border-radius: 4px;
                            padding: 20px;
                            line-height: 1.5;
                            text-align: center;
                            height: 105px;
                            display: block;
                            white-space: normal;
                        }

                        .campaign .cpopup>div:before {
                            content: "×";
                            position: absolute;
                            right: 7px;
                            top: -5px;
                            font-size: 26px;
                        }

                        .campaign.dt.c1>div>div>a {
                            float: right;
                        }

                        .campaign.dt.c3>div>div {
                            width: calc((100% / 3) - 3px);
                        }

                        .campaign.dt.c4>div:nth-child(2) {
                            overflow-x: scroll;
                            padding-bottom: 3px;
                        }

                        .campaign.dt.c4>div:nth-child(2)::-webkit-scrollbar {
                            height: 5px;
                            background-color: #fff;
                        }

                        .campaign.dt.c4>div:nth-child(2)::-webkit-scrollbar-thumb {
                            background-color: #999;
                            border-radius: 4px;
                        }

                        .campaign.mb>div {
                            overflow-x: scroll;
                            -webkit-overflow-scrolling: touch;
                            -ms-scroll-chaining: chained;
                        }

                        .campaign.mb>div>div {
                            width: calc(43% - 3px);
                        }

                        .campaign.mb.c1>div>div>a {
                            display: block;
                            padding-top: 2px;
                        }

                        .campaign.c1>div>div {
                            width: calc(100% - 3px);
                        }

                        .campaign.c1>div>div>* {
                            display: inline-block;
                            vertical-align: middle;
                            margin-right: 2px;
                        }

                        .campaign.c1>div>div>div {
                            position: relative;
                            left: 0;
                            top: 0;
                        }

                        .campaign.c1>div>div>figure {
                            margin-bottom: 0;
                        }

                        .campaign.c1>div>div>figure.fecredit {
                            height: 20px;
                        }

                        .campaign.c1>div>div>figure>img {
                            position: relative;
                        }

                        .campaign.c2>div>div {
                            width: calc((100% / 2) - 3px);
                        }
                    </style>







                    <div class="block-button normal">
                        <a href="javascript:void(0);" class="btn-buynow jsBuy">MUA NGAY</a>

                        <a href="/tra-gop/dtdd/samsung-galaxy-note-20?m=credit" class="btn-ins pay-nganluong full ">
                            TRẢ GÓP QUA THẺ
                            <span>Visa, Mastercard, JCB</span>
                        </a>
                    </div>
                    <div id="app">
                        <section>
                            <!--fragment#4015adb289#head-->
                            <div fragment="4015adb289" id="box-input-product"><input id="product-id-input" type="number" placeholder="id sản phẩm"><input id="product-type-input" type="number" placeholder="kịch bản"><button id="btn-add-product"> Thêm vào giỏ hàng </button></div>
                            <!--fragment#e2b074b3ae#head-->
                            <div class="popup-detail" fragment="e2b074b3ae">
                                <div class="bg-detail"></div>
                                <div class="choose-promo">
                                    <div class="close-popup"><i></i></div>
                                    <div class="namePro"><span class="namePro__product-name">Điện thoại Samsung Galaxy A31</span><span> 6.490.000₫ <strike>6.990.000₫</strike></span></div>
                                    <div class="choose-colorPro">
                                        <!---->
                                        <!---->
                                        <div class="choose-amount"><span>Chọn số lượng: </span>
                                            <div class="choosenumber">
                                                <div class="minus" style="pointer-events: none;"><i style="background-color: rgb(204, 204, 204);"></i></div>
                                                <div class="number">1</div>
                                                <div class="plus" style="pointer-events: all;"><i style="background-color: rgb(40, 138, 214);"></i><i style="background-color: rgb(40, 138, 214);"></i></div><input type="hidden">
                                            </div>
                                        </div>
                                    </div>
                                    <!---->
                                    <!----><span><input type="hidden" value="true"></span><span class="plChoose-proColor"></span><button class="ordered">Đặt mua</button><a href="https://www.thegioididong.com/cart" target="_blank" class="seecart" style="display: none;">Xem giỏ hàng</a>
                                </div>
                            </div>
                            <div class="popup-detail" fragment="e2b074b3ae">
                                <div class="bg-detail"></div>
                                <div class="choose-promo">
                                    <div class="close-popup"><i></i></div>
                                    <div class="addPrsuccess"><i class="cartnew-tick"></i> <span>Sản phẩm đã được thêm vào giỏ hàng</span></div>
                                    <div class="namePro-success">
                                        <div class="imgSu"><span class="imgPro__helper"></span><img src="" alt="" loading="lazy"></div>
                                        <p><span></span><span>
                                                <!----><small>Số lượng: 0</small>
                                            </span><span>0₫</span></p>
                                    </div><button class="ordered">Xem giỏ hàng</button>
                                </div>
                            </div>
                            <!--fragment#e2b074b3ae#tail-->
                            <!--fragment#4015adb289#tail-->
                            <div class="error-handler">
                                <div class="popup-promo">
                                    <div class="bg-promo"></div>
                                    <div class="alert-promo">
                                        <div class="close-popup"><i></i></div><span></span>
                                        <p class=""></p><a href="javascript:void(0)" class="">Đồng ý</a>
                                        <div class="resize-triggers">
                                            <div class="expand-trigger">
                                                <div style="width: 1px; height: 1px;"></div>
                                            </div>
                                            <div class="contract-trigger"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="popup-max1Pro">
                                    <div class="popup-bot">
                                        <div class="close-max1Pro"><i></i><i></i></div><span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="loading-cart"><span class="cswrap"><span class="csdot"></span><span class="csdot"></span><span class="csdot"></span></span></div>
                        </section>
                    </div>
                    <script>
                        var isMobile = 0;
                        var versionjs = '84e0e9a5185f7e413f79';
                        var linkCss = '//cdn.tgdd.vn/mwgcart/vue-pro';
                        var linkJs = `//cdn.tgdd.vn/mwgcart/vue-pro/js/index2.${versionjs}.js`;
                        window.provinceId = 3;
                        window.index3State = {
                            productId: 218355,
                            productCode: '0131491002147',
                            productName: '&#x110;i&#x1EC7;n tho&#x1EA1;i Samsung Galaxy Note 20',
                            productType: 1,
                            isTransContact: false,
                            isUserManual: false,
                            lstProductCodeMoreColors: "",
                            bringProductsName: '',
                            indexGiftAdds: []
                        };

                        function getScript(source, callback) {
                            //document.getElementById("cartloading").style.display = 'block';
                            var el = document.createElement('script');
                            el.onload = callback;
                            el.src = source;
                            el.async = true;
                            document.body.appendChild(el);
                        }

                        function getCss(source) {
                            var link = document.createElement('link');
                            link.rel = 'stylesheet';
                            link.type = 'text/css';
                            link.href = source;
                            document.head.appendChild(link);
                        }



                        function updateListSelectedPromotionsInPageDetail(listSelecteGiftPromotions) {
                            window.index2State.indexGiftAdds = listSelecteGiftPromotions;
                            if (listSelecteGiftPromotions.length > 0) {
                                var classParent = listSelecteGiftPromotions[0].productType === 1 ? ".box_normal" : ".box_saving"
                                $(classParent + ' .label-radio').removeClass('active');
                                for (var i = 0; i < listSelecteGiftPromotions.length; i++) {
                                    var item = listSelecteGiftPromotions[i];
                                    $(classParent + ' .label-radio[data-promotion="' + item.promotionId + '"]:eq(' + item.promotionGiftIndex + ')').addClass('active');
                                }
                            }
                        }

                        function choosePromotion($this, type) {
                            var box = type == 1 ? ".box_normal" : ".box_saving";
                            var $parent = $this.parent().parent();
                            $parent.find('.label-radio').removeClass('active');
                            $this.addClass('active');

                            var text = '';
                            $(box + ' .label-radio.active').each(function() {
                                text += $(this).html() + ' và ';
                            });

                            $(box + ' .showpromo-radio').removeClass('hide');
                            $(box + ' .showpromo-radio span').html(text.substring(0, text.length - 4));

                            if (typeof window.index2State !== "undefined") {
                                window.index2State.indexGiftAdds = [];
                                $(box + ' .label-radio.active').each(function() {
                                    window.index2State.indexGiftAdds.push({
                                        promotionId: $this.data('promotion'),
                                        promotionGiftIndex: $this.data('index'),
                                        productType: type
                                    });
                                });
                            }

                            //Tính lại hiển thị text hiển thị km chọn cho kb saving
                            //if (type == 2) {
                            //    $('.box_saving').addClass('choosed');
                            //    $('.box_saving.choosed .bs_rule').css('padding-bottom', ($('.box_saving.choosed .showpromo-radio').height() + 14) + 'px');
                            //    $('.box_saving.choosed .showpromo-radio').css('bottom', ($('.box_saving.choosed .block-button').height() + 10) + 'px');
                            //}
                        }

                        function CallBackVue() {
                            window.index2State = window.index3State;
                            var list = document.querySelectorAll(".normal .jsBuy");
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].className.indexOf('skip') !== -1)
                                    continue;
                                list[i].href = "javascript:void(0);";
                                list[i].addEventListener("click", function() {
                                    if (!dataDeli.IsCheckSubmit) {
                                        //CheckDelivery();
                                        return false;
                                    } else {
                                        window.index2State.productType = 1;
                                        $("#btn-add-product").click();
                                        return false;
                                    }
                                });
                            }

                            var listsaving = document.querySelectorAll(".saving .jsBuy");
                            for (var i = 0; i < listsaving.length; i++) {
                                if (listsaving[i].className.indexOf('skip') !== -1)
                                    continue;
                                listsaving[i].href = "javascript:void(0);";
                                listsaving[i].addEventListener("click", function() {
                                    if (!dataDeli.IsCheckSubmit) {
                                        //CheckDelivery();
                                        return false;
                                    } else {
                                        window.index2State.productType = 2;
                                        $("#btn-add-product").click();
                                        return false;
                                    }
                                });
                            }

                            var listsaving = document.querySelectorAll(".onlineonly .jsBuy");
                            for (var i = 0; i < listsaving.length; i++) {
                                if (listsaving[i].className.indexOf('skip') !== -1)
                                    continue;
                                listsaving[i].href = "javascript:void(0);";
                                listsaving[i].addEventListener("click", function() {
                                    if (!dataDeli.IsCheckSubmit) {
                                        return false;
                                    } else {
                                        window.index2State.productType = 4;
                                        $("#btn-add-product").click();
                                        return false;
                                    }
                                });
                            }
                            //document.getElementById("cartloading").style.display = 'none';
                        }

                        setTimeout(function() {
                            getScript(linkJs, function() {
                                console.log('getScript ' + versionjs);
                            });

                            if (isMobile == 1) {
                                getCss(linkCss + "/css/mobile/global-detail.css?v=" + versionjs);
                                getCss(linkCss + "/css/mobile/popup-detail.css?v=" + versionjs);
                                getCss(linkCss + "/css/mobile/alert.css?v=" + versionjs);
                            } else {
                                getCss(linkCss + "/css/desktop/global-detail.css?v=" + versionjs);
                                getCss(linkCss + "/css/desktop/popup-detail.css?v=" + versionjs);
                                getCss(linkCss + "/css/desktop/alert.css?v=" + versionjs);
                            }
                        }, 100);
                    </script>



                    <p class="callorder">Gọi đặt mua <a href="tel:18001060">1800.1060</a> (7:30 - 22:00)</p>












                    <div class="promoadd " data-max="3">
                        <p class="promoadd__ttl">
                            <em><b class="bonus_count">3</b> ưu đãi thêm</em>
                            <span>Dự kiến áp dụng đến 23:00 30/09</span>
                        </p>
                        <ul class="promoadd__list">

                            <li class="" data-tovalue="100" data-promotion="775925">
                                <i class="icondetail-tick"></i>
                                <p>Tặng suất mua xe đạp Giảm đến 20% (không kèm KM khác) <a href="https://www.dienmayxanh.com/khuyen-mai/xe-dap-giam-den-20-khi-mua-kem-tv-tu-lanh-may-g-1377323" target="_blank">(click xem chi tiết)</a></p>
                            </li>
                            <li class="" data-tovalue="80" data-promotion="775935">
                                <i class="icondetail-tick"></i>
                                <p>Mua kèm Pin sạc dự phòng AVA/AVA+ giảm 40%</p>
                            </li>
                            <li class="" data-tovalue="10" data-promotion="772506">
                                <i class="icondetail-tick"></i>
                                <p>Mua Đồng hồ thời trang giảm 40% cho Đồng hồ dưới 3 triệu, giảm 30% cho Đồng hồ từ 3 triệu trở lên</p>
                            </li>
                        </ul>

                    </div>


                </div>
            </div>
            <div class="border7"></div>




            <p class="parameter__title">Cấu hình Điện thoại Samsung Galaxy Note 20</p>
            <div class="parameter">
                <ul class="parameter__list 218355 active">
                    <li data-index="0" data-prop="0">
                        <p class="lileft">Màn hình:</p>
                        <div class="liright">
                            <span class="comma">Super AMOLED Plus</span>
                            <span class="comma">6.7"</span>
                            <span class="">Full HD+</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">Hệ điều hành:</p>
                        <div class="liright">
                            <span class="">Android 10</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">Camera sau:</p>
                        <div class="liright">
                            <span class="">Chính 12 MP &amp; Phụ 64 MP, 12 MP</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">Camera trước:</p>
                        <div class="liright">
                            <span class="">10 MP</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">Chip:</p>
                        <div class="liright">
                            <span class="">Exynos 990</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">RAM:</p>
                        <div class="liright">
                            <span class="">8 GB</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">Bộ nhớ trong:</p>
                        <div class="liright">
                            <span class="">256 GB</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">SIM:</p>
                        <div class="liright">
                            <span class="comma">2 Nano SIM hoặc 1 Nano SIM + 1 eSIM</span>
                            <span class="">Hỗ trợ 4G</span>
                        </div>
                    </li>
                    <li data-index="0" data-prop="0">
                        <p class="lileft">Pin, Sạc:</p>
                        <div class="liright">
                            <span class="comma">4300 mAh</span>
                            <span class="">25 W</span>
                        </div>
                    </li>
                </ul>
                <span class="parameter__link__list-files__item">
                    <a href="https://cdn.tgdd.vn/Products/PDF/3/218355/HDSD/Huong-Dan-Su-Dung-Tieng-Viet.pdf" target="_blank" class="parameter__link active">
                        <i class="icondetail-document"></i>
                        Hướng Dẫn Sử Dụng Tiếng Việt
                    </a>
                    <span>[PDF, 5.3MB]</span>
                </span>
                <span class="btn-detail btn-short-spec ">
                    <span>Xem thêm cấu hình chi tiết</span>
                </span>
            </div>
        </div>

        <!-- USER REVIEW -->
        <div class="wrap_comment" title="Bình luận sản phẩm" id="comment" cmtcategoryid="2" siteid="1" detailid="218355" cateid="" urlpage="" pagesize="5" data-js="https://cdn.tgdd.vn/comment/Scripts/CommentDesktop.min.v202105111612.js" data-ismobile="False" data-jsovrcmt="/js/Comment/overrideScript.min.js?v=202105110200">
            <div class="tltCmt"> <textarea id="txtEditor" name="" cols="" rows="" class="parent_input txtEditor" placeholder="Mời bạn để lại bình luận..." onclick="cmtaddcommentclick();" onkeyup="cmtaddcommentclick();"></textarea>
                <div class="sortcomment">
                    <div class="statistical hide" id="notifycmtmsg">Bình luận mới vừa được thêm vào. <a href="javascript:void(0)">Click để xem</a></div>
                </div>
                <div class="clr"></div>
                <div class="midcmt"> <span class="totalcomment">1.174 bình luận <span class="tech" onclick="showCmtTech();"><i class="iconcom-uncheck"></i>Xem bình luận kỹ thuật</span> </span>
                    <div class="s_comment">
                        <form method="post" onsubmit="return false;"> <input class="cmtKey" type="text" placeholder="Tìm theo nội dung, người gửi..." onkeyup="cmtSearch(event);"> <i class="iconcom-search"></i> </form>
                    </div>
                    <div class="fts_comment" data-val="0"> <span class="c_lbl">Sắp xếp theo</span> <span class="c_ods c_ods_d" onclick="cmt_selOdSeach(0)"><i class="iconcom-radcheck"></i>&nbsp;Độ chính xác</span> <span class="c_ods c_ods_m" onclick="cmt_selOdSeach(1)"><i class="iconcom-raduncheck"></i>&nbsp;Mới nhất</span> </div>
                </div>
            </div>
            <ul class="listcomment">
                <li class="comment_ask" id="47801133">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>t</div><strong onclick="selCmt(47801133)">Trần Quốc Tú</strong>
                        </a></div>
                    <div class="question"> Điện thoại này có ở tp.tây ninh không vậy ? . Dịch thế này mình muốn mua trả góp thì sao</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47801133)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47801133)">21 giờ trước </a></div>
                    <div class="listreply" id="r47801133">
                        <div class="reply " id="47801203" data-parent="47801133">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47801203)">Kim Thy</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh&nbsp;<br>Dạ khu vực mình hiện hỗ trợ trả góp&nbsp; qua thẻ tín dụng thôi anh nha. Dạ cho em hỏi là mình muốn góp qua thẻ tín dụng hay công ty tài chính ạ.&nbsp; vÀ hiện sản phẩm hỗ trợ giao hàng tận nơi tại khu vực của mình anh nha.&nbsp;<br>Thông tin đến anh.&nbsp;</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47801203,47801133)">Trả lời</a><a href="javascript:satisfiedCmt(47801203);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47801203);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47801203)">21 giờ trước </a>
                                <div id="wrs47801203" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Kim Thy</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47801203)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47800370">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>t</div><strong onclick="selCmt(47800370)">Nguyễn Hải Duy Thanh</strong>
                        </a></div>
                    <div class="question"> Muốn mua note 20 nhưng bên công ty tài chánh ko hoạt động sao mua</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47800370)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47800370)">21 giờ trước </a></div>
                    <div class="listreply" id="r47800370">
                        <div class="reply " id="47800474" data-parent="47800370">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47800474)">Trần Lê Phương</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh&nbsp;<br>Dạ anh có thể góp qua thẻ nha anh, Dạ muốn trả góp qua thẻ thì thẻ của mình là dạng thẻ Credit thì mới trả góp được ạ. Trường hợp nếu thẻ của mình chỉ là thẻ Debit (ATM, thẻ ghi nợ thông thường) thì không trả góp qua thẻ được ạ. Nếu thẻ của mình là thẻ Credit thì cho em xin tên ngân hàng, loại thẻ (Visa,MasterCard,JCB...),hạn mức tín dụng còn lại để tư vấn gói lãi suất phù hợp ạ.&nbsp;&nbsp;<br>Xin thông tin đến anh.&nbsp;</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47800474,47800370)">Trả lời</a><a href="javascript:satisfiedCmt(47800474);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47800474);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47800474)">21 giờ trước </a>
                                <div id="wrs47800474" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Trần Lê Phương</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47800474)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47790524">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>N</div><strong onclick="selCmt(47790524)">Nam</strong>
                        </a></div>
                    <div class="question"> Đt ni tệ nhất mà t từng mua. Đt gọi mess zalo giật giật nghe k được. Vọg lại. Tiếng nghe đc tiếng mất. Như cc. Đem bh hãg xog cũg bị lại như vậy.</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47790524)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47790524)">1 ngày trước </a></div>
                    <div class="listreply" id="r47790524">
                        <div class="reply " id="47790550" data-parent="47790524">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47790550)">Ánh Tuyết</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh&nbsp; <br>Dạ mong anh thông cảm về trường hợp này, mình có thể thông tin lại số điện thoại để được chuyển bộ phận CSKH hỗ trợ anh nha <br>Mong nhận phản hồi từ anh.&nbsp;</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47790550,47790524)">Trả lời</a><a href="javascript:satisfiedCmt(47790550);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47790550);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47790550)">1 ngày trước </a>
                                <div id="wrs47790550" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Ánh Tuyết</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47790550)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47788961">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>T</div><strong onclick="selCmt(47788961)">Minh Triết</strong>
                        </a></div>
                    <div class="question"> cho mình hỏi mình đang ở quận 6 đang cần gấp để làm việc và thấy hàng phải chuyển về thì bao lâu mới có hàng vậy shop?</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47788961)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47788961)">1 ngày trước </a></div>
                    <div class="listreply" id="r47788961">
                        <div class="reply " id="47788992" data-parent="47788961">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47788992)">Nguyễn Đình Thắng</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh.<br>Nếu quan tâm sản phẩm mình vui lòng cung cấp thông tin khu vực mình đang sinh sống cụ thể (phường/xã) để bên em hỗ trợ kiểm tra xem sản phẩm có hỗ trợ giao hàng về khu vực của mình không ạ&nbsp;<br>Mong nhận phản hồi từ anh&nbsp;</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47788992,47788961)">Trả lời</a><a href="javascript:satisfiedCmt(47788992);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47788992);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47788992)">1 ngày trước </a>
                                <div id="wrs47788992" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Nguyễn Đình Thắng</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47788992)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="reply " id="47793278" data-parent="47788961">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div>T</div><strong onclick="selCmt(47793278)">Minh Triet</strong>
                                </a></div>
                            <div class="cont"> @Nguyễn Đình Thắng: dạ em ở Phường 13, Quận 6 ạ</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47793278,47788961)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47793278)">1 ngày trước </a></div>
                        </div>
                        <div class="reply " id="47793316" data-parent="47788961">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47793316)">Đỗ Minh Phương</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Dạ em kiểm tra khu vực này có hỗ trợ giao hàng tận nhà ạ, hiện hôm nay mình đặt hàng, dự kiến trước 17:00 ngày 08/09 sẽ nhận hàng anh nha. Mình có thể tham khảo ạ. Sẽ không thể có hàng ngay trong ngày được anh nhé.<br>Thông tin đến anh.&nbsp;</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47793316,47788961)">Trả lời</a><a href="javascript:satisfiedCmt(47793316);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47793316);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47793316)">1 ngày trước </a>
                                <div id="wrs47793316" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Đỗ Minh Phương</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47793316)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47780094">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>L</div><strong onclick="selCmt(47780094)">Linh</strong>
                        </a></div>
                    <div class="question"> Bên shop k bán note 20 untra5g nữa à. Tìm mãi k thấy</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47780094)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47780094)">2 ngày trước </a></div>
                    <div class="listreply" id="r47780094">
                        <div class="reply " id="47780120" data-parent="47780094">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47780120)">Nhứt Vinh</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh<br>Dạ hiện&nbsp;Điện thoại Samsung Galaxy Note 20 Ultra 5G bên em đã ngừng kinh doanh rồi ạ nên bên em chưa thể hỗ trợ giúp anh được ạ, mong anh thông cảm.<br>Thông tin đến anh.</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47780120,47780094)">Trả lời</a><a href="javascript:satisfiedCmt(47780120);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47780120);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47780120)">2 ngày trước </a>
                                <div id="wrs47780120" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Nhứt Vinh</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47780120)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47775513">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>D</div><strong onclick="selCmt(47775513)">Mai Hữu Duy</strong>
                        </a></div>
                    <div class="question"> Khu vực TX Duyên Hải, Trà Vinh còn hàng không ạ?</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47775513)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47775513)">2 ngày trước </a></div>
                    <div class="listreply" id="r47775513">
                        <div class="reply " id="47775544" data-parent="47775513">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47775544)">Lê Minh Triền</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Dạ em chào anh.<br>Dạ với sản phẩm này thì bên em hiện hết hàng ở khu vực mình rồi anh nha.Vì ảnh hưởng bởi dịch nên bên em chưa hỗ trợ giao hàng về mình được anh nha. Mong anh thông cảm giúp em.&nbsp;&nbsp;<br>Thông tin đến anh.</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47775544,47775513)">Trả lời</a><a href="javascript:satisfiedCmt(47775544);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47775544);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47775544)">2 ngày trước </a>
                                <div id="wrs47775544" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Lê Minh Triền</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47775544)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47765385">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>m</div><strong onclick="selCmt(47765385)">T&amp;#38;#38;#226;M</strong>
                        </a></div>
                    <div class="question"> Không biết khi sử dụng ốp lưng thì có thể thanh toán bằng samsung pay không shop ?</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47765385)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47765385)">2 ngày trước </a></div>
                    <div class="listreply" id="r47765385">
                        <div class="reply " id="47765462" data-parent="47765385">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47765462)">Ánh Tuyết</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào chị <br>Dạ được chị nha, mình có thể thanh toán bằng samsung pay ạ<br>Thông tin đến chị.</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47765462,47765385)">Trả lời</a><a href="javascript:satisfiedCmt(47765462);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47765462);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47765462)">2 ngày trước </a>
                                <div id="wrs47765462" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Ánh Tuyết</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47765462)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47757583">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>C</div><strong onclick="selCmt(47757583)">Cat</strong>
                        </a></div>
                    <div class="question"> Mua tra gop co dc huong khuyen mai ko</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47757583)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47757583)">3 ngày trước </a></div>
                    <div class="listreply" id="r47757583">
                        <div class="reply " id="47757592" data-parent="47757583">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47757592)">Phi Phụng</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh,<br>Dạ không biết mình quan tâm góp qua công ty tài chính hay qua thẻ tín dụng và&nbsp; mình ở khu vực nào ạ.<br>Mong phản hồi từ anh.</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47757592,47757583)">Trả lời</a><a href="javascript:satisfiedCmt(47757592);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47757592);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47757592)">3 ngày trước </a>
                                <div id="wrs47757592" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Phi Phụng</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47757592)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47753778">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>T</div><strong onclick="selCmt(47753778)">Tu</strong>
                        </a></div>
                    <div class="question"> Chào bạn Hiện tại, máy còn ở chi nhánh Nhơn Trạch, Đồng Nai không bạn?</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47753778)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47753778)">3 ngày trước </a></div>
                    <div class="listreply" id="r47753778">
                        <div class="reply " id="47753787" data-parent="47753778">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47753787)">Trần Lê Phương</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh&nbsp;<br>Dạ rất tiếc theo em kiểm tra thì khu vực này đã&nbsp; hết hàng và khu vực này hiện đóng cửa nên bên em chỉ hỗ trợ giao hàng tận nơi ạ .&nbsp;<br>Dạ mình vui lòng vào webiste điền thông tin số nhà đầy đủ và chọn mua hàng , hình thức thanh toán giúp em ạ, nếu website hiển thị hỗ trợ giao được thì nhân viên sẽ giao ạ, và nhà mình không nằm trong vùng cách ly, phong tỏa thì hỗ trợ ạ. em cám ơn ạ.&nbsp;&nbsp;<br>Xin thông tin đến anh.&nbsp;</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47753787,47753778)">Trả lời</a><a href="javascript:satisfiedCmt(47753787);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47753787);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47753787)">3 ngày trước </a>
                                <div id="wrs47753787" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Trần Lê Phương</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47753787)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <li class="comment_ask" id="47747959">
                    <div class="rowuser"><a href="javascript:void(0)">
                            <div>T</div><strong onclick="selCmt(47747959)">Tính</strong>
                        </a></div>
                    <div class="question"> Có giao tại Long Hưng B, Lấp Vò Đồng Tháp được không vậy?</div>
                    <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtaddreplyclick(47747959)">Trả lời</a><a href="javascript:void(0)" class="time" onclick="cmtReport(47747959)">3 ngày trước </a></div>
                    <div class="listreply" id="r47747959">
                        <div class="reply " id="47747993" data-parent="47747959">
                            <div class="rowuser"><a href="javascript:void(0)">
                                    <div class="c"><i class="iconcom-avactv"></i></div><strong onclick="selCmt(47747993)">Phi Phụng</strong><b class="qtv">Quản trị viên</b>
                                </a></div>
                            <div class="cont">Chào anh,<br>Dạ hiện mình đặt mua dự kiến&nbsp;Giao trước 12h ngày mai (01/09) anh nha.Để đặt mua sản phẩm mình vui lòng chọn sản phẩm,chọn khu vực để xem giá và khuyến mãi chính xác tại khu vực của mình , Bấm vào nút mua ngay, điền các thông tin đặt hàng như tên và số điện thoại, chọn giao tận nơi điền đầy đủ chính xác địa chỉ nhà, click đặt hàng , chọn hình thức thanh toán hệ thống sẽ hỗ trợ thông tin cụ thể đến mình&nbsp;anh nha.<br>Thông tin đến anh.</div>
                            <div class="actionuser" data-cl="0"><a href="javascript:void(0)" class="respondent" onclick="cmtChildAddReplyClick(47747993,47747959)">Trả lời</a><a href="javascript:satisfiedCmt(47747993);" class="favor satis cmthl"><i class="iconcom-like"></i>Hài lòng<span></span></a><a href="javascript:unsatisfiedCmt(47747993);" class="favor satis cmtkhl"><i class="iconcom-unlike"></i>Không hài lòng<span></span></a><a href="javascript:void(0)" class="time" onclick="cmtReport(47747993)">3 ngày trước </a>
                                <div id="wrs47747993" class="wrapsatis" style="display: none;">
                                    <div class="wrsct"><i class="iconcom-close" onclick="closeSatis();"></i><span>Thegioididong.com rất tiếc đã làm bạn chưa hài lòng. Mời bạn góp ý để <b>QTV Phi Phụng</b> phục vụ tốt hơn:</span><textarea placeholder="Nhập nội dung góp ý" type="text" class="ustCt"></textarea><span>Hãy để lại thông tin để được hỗ trợ khi cần thiết (không bắt buộc):</span><input placeholder="Họ tên" type="text" class="ustName"><input placeholder="Số điện thoại" type="text" class="ustPhone"><a href="javascript:sendUnSatisfied(47747993)">GỬI</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputreply hide"></div>
                </li>
                <div class="pagcomment"><span class="active">1</span><a onclick="listcomment(2,1,2);return false;" title="trang 2">2</a><a onclick="listcomment(2,1,3);return false;" title="trang 3">3</a><a onclick="listcomment(2,1,4);return false;" title="trang 4">4</a><span>...</span><a onclick="listcomment(2,1,118);return false;" title="trang 118">118</a><a onclick="listcomment(2,1,2);return false;" title="trang 2">»</a></div>
            </ul><textarea id="txtEditorExt" name="" cols="" rows="" class="txtEditor" placeholder="Mời Bạn để lại bình luận..."></textarea>
            <div class="ajaxcomment hide">
                <div id="loadcomment"></div>
            </div>
        </div>
        <!-- END USER REVIEW -->

    </div>
    <form class="gallery-rating-popup popup-image-rating-container" data-type="ImageGallery" data-link="">
        <div class="gallery-rating-popup__content">
            <div class="gallery-rating-popup__content__title">Phản hồi không hài lòng <span class="gallery-rating-popup__content__title__close"><i class="iconrating-close"></i></span></div>
            <div class="gallery-rating-popup__content__body">
                <textarea name="inputContent" rows="3" placeholder="Mời bạn góp ý để chúng tôi phục vụ tốt hơn" required=""></textarea>
                <span class="gallery-rating-popup__content__body__note">Hãy để lại thông tin để được hỗ trợ khi cần thiết (Không bắt buộc):</span>
                <div class="gallery-rating-popup__content__body__genders">
                    <label class="gallery-rating-popup__content__body__genders__option" data-value="male"><i></i>Anh</label>
                    <label class="gallery-rating-popup__content__body__genders__option" data-value="female"><i></i>Chị</label>
                    <input type="hidden" name="inputGender">
                </div>
                <input type="text" name="inputFullName" placeholder="Họ tên">
                <input type="tel" name="inputPhoneNumber" placeholder="Số điện thoại" data-invalid-message="">
                <input type="text" name="inputEmail" placeholder="Email">
                <button class="gallery-rating-popup__content__body__btn-submit" type="submit">Gửi góp ý<span>Cam kết bảo mật thông tin cá nhân</span></button>
                <label class="gallery-rating-popup__content__body__rating-success">Cám ơn bạn đã đánh giá.</label>
            </div>
        </div>
    </form>


    <form class="gallery-rating-popup popup-video-rating-container" data-type="VideoGallery" data-link="">
        <div class="gallery-rating-popup__content">
            <div class="gallery-rating-popup__content__title">Phản hồi không hài lòng <span class="gallery-rating-popup__content__title__close"><i class="iconrating-close"></i></span></div>
            <div class="gallery-rating-popup__content__body">
                <textarea name="inputContent" rows="3" placeholder="Mời bạn góp ý để chúng tôi phục vụ tốt hơn" required=""></textarea>
                <span class="gallery-rating-popup__content__body__note">Hãy để lại thông tin để được hỗ trợ khi cần thiết (Không bắt buộc):</span>
                <div class="gallery-rating-popup__content__body__genders">
                    <label class="gallery-rating-popup__content__body__genders__option" data-value="male"><i></i>Anh</label>
                    <label class="gallery-rating-popup__content__body__genders__option" data-value="female"><i></i>Chị</label>
                    <input type="hidden" name="inputGender">
                </div>
                <input type="text" name="inputFullName" placeholder="Họ tên">
                <input type="tel" name="inputPhoneNumber" placeholder="Số điện thoại" data-invalid-message="">
                <input type="text" name="inputEmail" placeholder="Email">
                <button class="gallery-rating-popup__content__body__btn-submit" type="submit">Gửi góp ý<span>Cam kết bảo mật thông tin cá nhân</span></button>
                <label class="gallery-rating-popup__content__body__rating-success">Cám ơn bạn đã đánh giá.</label>
            </div>
        </div>
    </form>



    <form class="article-rating__form" style="display: none;">
        <input type="hidden" name="inputPhoneNumber" value="">
        <input type="hidden" name="inputGender" value="3">
        <input type="hidden" name="inputFullName" value="Bạn">
        <input type="hidden" name="inputProductId" value="218355">
        <input type="hidden" name="inputAge">
        <input type="hidden" name="inputContent">
        <input type="hidden" name="inputLink" value="http://www.thegioididong.com/dtdd/samsung-galaxy-note-20?src=osp">
    </form>

    <div class="gallery-loading">
        <span class="gallery-loading__cswrap">
            <span class="gallery-loading__cswrap__csdot"></span>
            <span class="gallery-loading__cswrap__csdot"></span>
            <span class="gallery-loading__cswrap__csdot"></span>
        </span>
    </div>

    <div class="popup-addsp">
        <div class="bg-popup"></div>
        <div class="close-popup" onclick="ClosePopup(this)">
            <aside>
                <i></i>
                <span>Đóng</span>
            </aside>
        </div>
        <div class="compare-popup">
            <h4>Điện thoại đã xem gần nhất</h4>
            <ul class="pro-compare pro-compare_viewed">
            </ul>
            <h4>Hoặc nhập tên để tìm</h4>
            <form id="searchproductcompare" onsubmit="return false">
                <div class="find-sp">
                    <input type="text" placeholder="Nhập tên điện thoại để tìm" onkeyup="SearchProductCompare(this)">
                    <i class="icon-findcp"></i>
                </div>
                <ul class="pro-compare pro-compare_search"></ul>
            </form>
        </div>
    </div>
    <div class="errorcompare" style="display:none;"></div>

    <!--#region FAQComment-->
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "FAQPage",
            "@id": "https://www.thegioididong.com/dtdd/samsung-galaxy-note-20#comment",
            "url": "https://www.thegioididong.com/dtdd/samsung-galaxy-note-20",
            "commentCount": 10,
            "mainEntity": [{
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Điện thoại này có ở tp.tây ninh không vậy ? . Dịch thế này mình muốn mua trả góp thì sao",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào anh Dạ khu vực mình hiện hỗ trợ trả góp  qua thẻ tín dụng thôi anh nha. Dạ cho em hỏi là mình muốn góp qua thẻ tín dụng hay công ty tài chính ạ.  vÀ hiện sản phẩm hỗ trợ giao hàng tận nơi tại khu vực của mình anh nha. Thông tin đến anh. "
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Muốn mua note 20 nhưng bên công ty tài chánh ko hoạt động sao mua",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào anh Dạ anh có thể góp qua thẻ nha anh, Dạ muốn trả góp qua thẻ thì thẻ của mình là dạng thẻ Credit thì mới trả góp được ạ. Trường hợp nếu thẻ của mình chỉ là thẻ Debit (ATM, thẻ ghi nợ thông thường) thì không trả góp qua thẻ được ạ. Nếu thẻ của mình là thẻ Credit thì cho em xin tên ngân hàng, loại thẻ (Visa,MasterCard,JCB...),hạn mức tín dụng còn lại để tư vấn gói lãi suất phù hợp ạ.  Xin thông tin đến anh. "
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Đt ni tệ nhất mà t từng mua. Đt gọi mess zalo giật giật nghe k được. Vọg lại. Tiếng nghe đc tiếng mất. Như cc. Đem bh hãg xog cũg bị lại như vậy.",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào anh  Dạ mong anh thông cảm về trường hợp này, mình có thể thông tin lại số điện thoại để được chuyển bộ phận CSKH hỗ trợ anh nha Mong nhận phản hồi từ anh. "
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "cho mình hỏi mình đang ở quận 6 đang cần gấp để làm việc và thấy hàng phải chuyển về thì bao lâu mới có hàng vậy shop?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Dạ em kiểm tra khu vực này có hỗ trợ giao hàng tận nhà ạ, hiện hôm nay mình đặt hàng, dự kiến trước 17:00 ngày 08/09 sẽ nhận hàng anh nha. Mình có thể tham khảo ạ. Sẽ không thể có hàng ngay trong ngày được anh nhé.Thông tin đến anh. "
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Bên shop k bán note 20 untra5g nữa à. Tìm mãi k thấy",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào anhDạ hiện Điện thoại Samsung Galaxy Note 20 Ultra 5G bên em đã ngừng kinh doanh rồi ạ nên bên em chưa thể hỗ trợ giúp anh được ạ, mong anh thông cảm.Thông tin đến anh."
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Khu vực TX Duyên Hải, Trà Vinh còn hàng không ạ?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Dạ em chào anh.Dạ với sản phẩm này thì bên em hiện hết hàng ở khu vực mình rồi anh nha.Vì ảnh hưởng bởi dịch nên bên em chưa hỗ trợ giao hàng về mình được anh nha. Mong anh thông cảm giúp em.  Thông tin đến anh."
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Không biết khi sử dụng ốp lưng thì có thể thanh toán bằng samsung pay không shop ?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào chị Dạ được chị nha, mình có thể thanh toán bằng samsung pay ạThông tin đến chị."
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Mua tra gop co dc huong khuyen mai ko",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào anh,Dạ không biết mình quan tâm góp qua công ty tài chính hay qua thẻ tín dụng và  mình ở khu vực nào ạ.Mong phản hồi từ anh."
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Chào bạn Hiện tại, máy còn ở chi nhánh Nhơn Trạch, Đồng Nai không bạn?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào anh Dạ rất tiếc theo em kiểm tra thì khu vực này đã  hết hàng và khu vực này hiện đóng cửa nên bên em chỉ hỗ trợ giao hàng tận nơi ạ . Dạ mình vui lòng vào webiste điền thông tin số nhà đầy đủ và chọn mua hàng , hình thức thanh toán giúp em ạ, nếu website hiển thị hỗ trợ giao được thì nhân viên sẽ giao ạ, và nhà mình không nằm trong vùng cách ly, phong tỏa thì hỗ trợ ạ. em cám ơn ạ.  Xin thông tin đến anh. "
                }
            }, {
                "@type": "Question",
                "upvoteCount": 0,
                "name": "Có giao tại Long Hưng B, Lấp Vò Đồng Tháp được không vậy?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "upvoteCount": 0,
                    "text": "Chào anh,Dạ hiện mình đặt mua dự kiến Giao trước 12h ngày mai (01/09) anh nha.Để đặt mua sản phẩm mình vui lòng chọn sản phẩm,chọn khu vực để xem giá và khuyến mãi chính xác tại khu vực của mình , Bấm vào nút mua ngay, điền các thông tin đặt hàng như tên và số điện thoại, chọn giao tận nơi điền đầy đủ chính xác địa chỉ nhà, click đặt hàng , chọn hình thức thanh toán hệ thống sẽ hỗ trợ thông tin cụ thể đến mình anh nha.Thông tin đến anh."
                }
            }]
        }
    </script>
    <!--#endregion-->
    <!--#region Product-->
    <script type="application/ld+json" id="productld">
        {
            "@context": "https://schema.org",
            "@type": "Product",
            "name": "Điện thoại Samsung Galaxy Note 20",
            "image": ["https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-062220-122200-fix-600x600.jpg"],
            "description": "Mua online điện thoại Samsung Galaxy Note 20 chính hãng, giá rẻ trả góp 0%, giảm ngay 9 triệu (1-30/9). Giao nhanh, đem nhiều mẫu chọn, cà thẻ tại nhà",
            "sku": "218355",
            "mpn": "218355",
            "brand": {
                "@type": "Thing",
                "name": ["Samsung"]
            },
            "review": [{
                "@type": "Review",
                "author": "Nguyễn Trọng Phát",
                "datePublished": "9/1/2021 5:58:14 AM: dd/MM/yyyy",
                "reviewBody": "Máy có vẻ yếu về sóng, wifi, pin....\nĐể không mà tốn pin hơn con s10+ đã dũng hơn 2 năm mà đang chia sẻ dldd.",
                "image": null,
                "reviewRating": {
                    "@type": "Rating",
                    "bestRating": 5,
                    "ratingValue": 2.0
                }
            }, {
                "@type": "Review",
                "author": "Lê văn quyền",
                "datePublished": "8/30/2021 3:06:17 PM: dd/MM/yyyy",
                "reviewBody": "Mọi người nói này nọ mình dùng hơn tháng thấy  ok mà pin thì dc một ngày tùy vào ít  hay nhiều  chơi game lien quân  thấy ok mà thấy  nhiều người  nói này nọ quá ",
                "image": null,
                "reviewRating": {
                    "@type": "Rating",
                    "bestRating": 5,
                    "ratingValue": 5.0
                }
            }],
            "aggregateRating": {
                "@type": "AggregateRating",
                "bestRating": 5,
                "worstRating": 1,
                "ratingValue": 3.0,
                "ratingCount": 118
            },
            "additionalProperty": [{
                "@type": "PropertyValue",
                "name": "Công nghệ màn hình",
                "value": "Super AMOLED Plus"
            }, {
                "@type": "PropertyValue",
                "name": "Độ phân giải",
                "value": "Full HD+ (1080 x 2400 Pixels)"
            }, {
                "@type": "PropertyValue",
                "name": "Màn hình rộng",
                "value": "6.7\" - Tần số quét 60 Hz"
            }, {
                "@type": "PropertyValue",
                "name": "Độ sáng tối đa",
                "value": "1050 nits"
            }, {
                "@type": "PropertyValue",
                "name": "Mặt kính cảm ứng",
                "value": "Kính cường lực Corning Gorilla Glass 5"
            }, {
                "@type": "PropertyValue",
                "name": "Độ phân giải",
                "value": "Chính 12 MP & Phụ 64 MP, 12 MP"
            }, {
                "@type": "PropertyValue",
                "name": "Quay phim",
                "value": "8K 4320p@24fps"
            }, {
                "@type": "PropertyValue",
                "name": "Đèn Flash",
                "value": "Có"
            }, {
                "@type": "PropertyValue",
                "name": "Tính năng",
                "value": "Quay Siêu chậm (Super Slow Motion)"
            }, {
                "@type": "PropertyValue",
                "name": "Độ phân giải",
                "value": "10 MP"
            }, {
                "@type": "PropertyValue",
                "name": "Tính năng",
                "value": "Xóa phông"
            }, {
                "@type": "PropertyValue",
                "name": "Hệ điều hành",
                "value": "Android 10"
            }, {
                "@type": "PropertyValue",
                "name": "Chip xử lý (CPU)",
                "value": "Exynos 990"
            }, {
                "@type": "PropertyValue",
                "name": "Tốc độ CPU",
                "value": "2 nhân 2.73 GHz, 2 nhân 2.5 GHz & 4 nhân 2.0 Ghz"
            }, {
                "@type": "PropertyValue",
                "name": "Chip đồ họa (GPU)",
                "value": "Mali-G77 MP11"
            }, {
                "@type": "PropertyValue",
                "name": "RAM",
                "value": "8 GB"
            }, {
                "@type": "PropertyValue",
                "name": "Bộ nhớ trong",
                "value": "256 GB"
            }, {
                "@type": "PropertyValue",
                "name": "Bộ nhớ còn lại (khả dụng) khoảng",
                "value": "223 GB"
            }, {
                "@type": "PropertyValue",
                "name": "Danh bạ",
                "value": "Không giới hạn"
            }, {
                "@type": "PropertyValue",
                "name": "Mạng di động",
                "value": "Hỗ trợ 4G"
            }, {
                "@type": "PropertyValue",
                "name": "SIM",
                "value": "2 Nano SIM hoặc 1 Nano SIM + 1 eSIM"
            }, {
                "@type": "PropertyValue",
                "name": "Wifi",
                "value": "Dual-band (2.4 GHz/5 GHz)"
            }, {
                "@type": "PropertyValue",
                "name": "GPS",
                "value": "GLONASS"
            }, {
                "@type": "PropertyValue",
                "name": "Bluetooth",
                "value": "A2DP"
            }, {
                "@type": "PropertyValue",
                "name": "Cổng kết nối/sạc",
                "value": "2 đầu Type-C"
            }, {
                "@type": "PropertyValue",
                "name": "Jack tai nghe",
                "value": "Type-C"
            }, {
                "@type": "PropertyValue",
                "name": "Kết nối khác",
                "value": "NFC"
            }, {
                "@type": "PropertyValue",
                "name": "Dung lượng pin",
                "value": "4300 mAh"
            }, {
                "@type": "PropertyValue",
                "name": "Loại pin",
                "value": "Li-Ion"
            }, {
                "@type": "PropertyValue",
                "name": "Hỗ trợ sạc tối đa",
                "value": "25 W"
            }, {
                "@type": "PropertyValue",
                "name": "Sạc kèm theo máy",
                "value": "18 W"
            }, {
                "@type": "PropertyValue",
                "name": "Công nghệ pin",
                "value": "Sạc pin nhanh"
            }, {
                "@type": "PropertyValue",
                "name": "Bảo mật nâng cao",
                "value": "Mở khoá khuôn mặt"
            }, {
                "@type": "PropertyValue",
                "name": "Tính năng đặc biệt",
                "value": "Âm thanh AKG"
            }, {
                "@type": "PropertyValue",
                "name": "Kháng nước, bụi",
                "value": "IP68"
            }, {
                "@type": "PropertyValue",
                "name": "Ghi âm",
                "value": "Có (microphone chuyên dụng chống ồn)"
            }, {
                "@type": "PropertyValue",
                "name": "Xem phim",
                "value": "3GP"
            }, {
                "@type": "PropertyValue",
                "name": "Nghe nhạc",
                "value": "Midi"
            }, {
                "@type": "PropertyValue",
                "name": "Thiết kế",
                "value": "Nguyên khối"
            }, {
                "@type": "PropertyValue",
                "name": "Chất liệu",
                "value": "Khung kim loại & Mặt lưng nhựa"
            }, {
                "@type": "PropertyValue",
                "name": "Kích thước, khối lượng",
                "value": "Dài 161.6 mm - Ngang 72.5 mm - Dày 8.3 mm - Nặng 192 g"
            }, {
                "@type": "PropertyValue",
                "name": "Thời điểm ra mắt",
                "value": "08/2020"
            }],
            "offers": {
                "@type": "Offer",
                "url": "https://www.thegioididong.com/dtdd/samsung-galaxy-note-20",
                "priceCurrency": "VND",
                "price": 14990000.0,
                "priceValidUntil": "01/31/2021",
                "itemCondition": "https://schema.org/NewCondition",
                "availability": "https://schema.org/InStock",
                "seller": {
                    "@id": "https://www.thegioididong.com/#Organization"
                }
            }
        }
    </script>
    <input type="hidden" id="jsonProductGTM" value="{&quot;@context&quot;:&quot;https://schema.org&quot;,&quot;@type&quot;:&quot;Product&quot;,&quot;name&quot;:&quot;Điện thoại Samsung Galaxy Note 20&quot;,&quot;image&quot;:[&quot;https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-062220-122200-fix-600x600.jpg&quot;],&quot;description&quot;:&quot;Mua online điện thoại Samsung Galaxy Note 20 chính hãng, giá rẻ trả góp 0%, giảm ngay 9 triệu (1-30/9). Giao nhanh, đem nhiều mẫu chọn, cà thẻ tại nhà&quot;,&quot;sku&quot;:&quot;218355&quot;,&quot;mpn&quot;:&quot;218355&quot;,&quot;brand&quot;:{&quot;@type&quot;:&quot;Thing&quot;,&quot;name&quot;:[&quot;Samsung&quot;]},&quot;review&quot;:[{&quot;@type&quot;:&quot;Review&quot;,&quot;author&quot;:&quot;Nguyễn Trọng Phát&quot;,&quot;datePublished&quot;:&quot;9/1/2021 5:58:14 AM: dd/MM/yyyy&quot;,&quot;reviewBody&quot;:&quot;Máy có vẻ yếu về sóng, wifi, pin....\nĐể không mà tốn pin hơn con s10+ đã dũng hơn 2 năm mà đang chia sẻ dldd.&quot;,&quot;image&quot;:null,&quot;reviewRating&quot;:{&quot;@type&quot;:&quot;Rating&quot;,&quot;bestRating&quot;:5,&quot;ratingValue&quot;:2.0}},{&quot;@type&quot;:&quot;Review&quot;,&quot;author&quot;:&quot;Lê văn quyền&quot;,&quot;datePublished&quot;:&quot;8/30/2021 3:06:17 PM: dd/MM/yyyy&quot;,&quot;reviewBody&quot;:&quot;Mọi người nói này nọ mình dùng hơn tháng thấy  ok mà pin thì dc một ngày tùy vào ít  hay nhiều  chơi game lien quân  thấy ok mà thấy  nhiều người  nói này nọ quá &quot;,&quot;image&quot;:null,&quot;reviewRating&quot;:{&quot;@type&quot;:&quot;Rating&quot;,&quot;bestRating&quot;:5,&quot;ratingValue&quot;:5.0}}],&quot;aggregateRating&quot;:{&quot;@type&quot;:&quot;AggregateRating&quot;,&quot;bestRating&quot;:5,&quot;worstRating&quot;:1,&quot;ratingValue&quot;:3.0,&quot;ratingCount&quot;:118},&quot;additionalProperty&quot;:[{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Công nghệ màn hình&quot;,&quot;value&quot;:&quot;Super AMOLED Plus&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Độ phân giải&quot;,&quot;value&quot;:&quot;Full HD+ (1080 x 2400 Pixels)&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Màn hình rộng&quot;,&quot;value&quot;:&quot;6.7\&quot; - Tần số quét 60 Hz&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Độ sáng tối đa&quot;,&quot;value&quot;:&quot;1050 nits&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Mặt kính cảm ứng&quot;,&quot;value&quot;:&quot;Kính cường lực Corning Gorilla Glass 5&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Độ phân giải&quot;,&quot;value&quot;:&quot;Chính 12 MP &amp; Phụ 64 MP, 12 MP&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Quay phim&quot;,&quot;value&quot;:&quot;8K 4320p@24fps&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Đèn Flash&quot;,&quot;value&quot;:&quot;Có&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Tính năng&quot;,&quot;value&quot;:&quot;Quay Siêu chậm (Super Slow Motion)&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Độ phân giải&quot;,&quot;value&quot;:&quot;10 MP&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Tính năng&quot;,&quot;value&quot;:&quot;Xóa phông&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Hệ điều hành&quot;,&quot;value&quot;:&quot;Android 10&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Chip xử lý (CPU)&quot;,&quot;value&quot;:&quot;Exynos 990&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Tốc độ CPU&quot;,&quot;value&quot;:&quot;2 nhân 2.73 GHz, 2 nhân 2.5 GHz &amp; 4 nhân 2.0 Ghz&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Chip đồ họa (GPU)&quot;,&quot;value&quot;:&quot;Mali-G77 MP11&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;RAM&quot;,&quot;value&quot;:&quot;8 GB&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Bộ nhớ trong&quot;,&quot;value&quot;:&quot;256 GB&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Bộ nhớ còn lại (khả dụng) khoảng&quot;,&quot;value&quot;:&quot;223 GB&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Danh bạ&quot;,&quot;value&quot;:&quot;Không giới hạn&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Mạng di động&quot;,&quot;value&quot;:&quot;Hỗ trợ 4G&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;SIM&quot;,&quot;value&quot;:&quot;2 Nano SIM hoặc 1 Nano SIM + 1 eSIM&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Wifi&quot;,&quot;value&quot;:&quot;Dual-band (2.4 GHz/5 GHz)&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;GPS&quot;,&quot;value&quot;:&quot;GLONASS&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Bluetooth&quot;,&quot;value&quot;:&quot;A2DP&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Cổng kết nối/sạc&quot;,&quot;value&quot;:&quot;2 đầu Type-C&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Jack tai nghe&quot;,&quot;value&quot;:&quot;Type-C&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Kết nối khác&quot;,&quot;value&quot;:&quot;NFC&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Dung lượng pin&quot;,&quot;value&quot;:&quot;4300 mAh&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Loại pin&quot;,&quot;value&quot;:&quot;Li-Ion&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Hỗ trợ sạc tối đa&quot;,&quot;value&quot;:&quot;25 W&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Sạc kèm theo máy&quot;,&quot;value&quot;:&quot;18 W&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Công nghệ pin&quot;,&quot;value&quot;:&quot;Sạc pin nhanh&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Bảo mật nâng cao&quot;,&quot;value&quot;:&quot;Mở khoá khuôn mặt&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Tính năng đặc biệt&quot;,&quot;value&quot;:&quot;Âm thanh AKG&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Kháng nước, bụi&quot;,&quot;value&quot;:&quot;IP68&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Ghi âm&quot;,&quot;value&quot;:&quot;Có (microphone chuyên dụng chống ồn)&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Xem phim&quot;,&quot;value&quot;:&quot;3GP&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Nghe nhạc&quot;,&quot;value&quot;:&quot;Midi&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Thiết kế&quot;,&quot;value&quot;:&quot;Nguyên khối&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Chất liệu&quot;,&quot;value&quot;:&quot;Khung kim loại &amp; Mặt lưng nhựa&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Kích thước, khối lượng&quot;,&quot;value&quot;:&quot;Dài 161.6 mm - Ngang 72.5 mm - Dày 8.3 mm - Nặng 192 g&quot;},{&quot;@type&quot;:&quot;PropertyValue&quot;,&quot;name&quot;:&quot;Thời điểm ra mắt&quot;,&quot;value&quot;:&quot;08/2020&quot;}],&quot;offers&quot;:{&quot;@type&quot;:&quot;Offer&quot;,&quot;url&quot;:&quot;https://www.thegioididong.com/dtdd/samsung-galaxy-note-20&quot;,&quot;priceCurrency&quot;:&quot;VND&quot;,&quot;price&quot;:14990000.0,&quot;priceValidUntil&quot;:&quot;01/31/2021&quot;,&quot;itemCondition&quot;:&quot;https://schema.org/NewCondition&quot;,&quot;availability&quot;:&quot;https://schema.org/InStock&quot;,&quot;seller&quot;:{&quot;@id&quot;:&quot;https://www.thegioididong.com/#Organization&quot;}}}">
    <!--#endregion-->
    <!--#region BreadcrumbList-->
    <script type="application/ld+json" id="breadcrumb">
        {
            "@type": "BreadcrumbList",
            "@context": "https://schema.org",
            "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "https://www.thegioididong.com",
                    "name": "Thế Giới Di Động"
                }
            }, {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "https://www.thegioididong.com/dtdd",
                    "name": "Điện thoại"
                }
            }, {
                "@type": "ListItem",
                "position": 3,
                "item": {
                    "@id": "https://www.thegioididong.com/dtdd-samsung",
                    "name": "Điện thoại Samsung"
                }
            }]
        }
    </script>
    <!--#endregion-->
    <!--#region Video-->
    <!--#endregion-->
    <!--#region Organization-->
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "@id": "https://www.thegioididong.com/#Organization",
            "name": "Thế Giới Di Động",
            "url": "https://www.thegioididong.com/dtdd",
            "logo": {
                "@type": "ImageObject",
                "url": "https://cdn.thegioididong.com/v2015/ContentMwg/logo/logo.png"
            },
            "contactPoint": [{
                "@type": "ContactPoint",
                "telephone": "18001060",
                "contactType": "sales",
                "areaServed": "VN",
                "availableLanguage": ["EN", "VN"]
            }, {
                "@type": "ContactPoint",
                "telephone": "18001763",
                "contactType": "technical support",
                "areaServed": "VN",
                "availableLanguage": ["EN", "VN"]
            }, {
                "@type": "ContactPoint",
                "telephone": "18001062",
                "contactType": "customer support",
                "areaServed": "VN",
                "availableLanguage": ["EN", "VN"]
            }],
            "sameAs": ["https://www.facebook.com/thegioididongcom/", "https://vn.linkedin.com/company/thegioidong-dienmayxanh", "https://www.youtube.com/user/TGDDVideoReviews", "https://www.instagram.com/watch.thegioididong/", "https://vt.tiktok.com/RH9SSp/", "https://vi.wikipedia.org/wiki/Thegioididong.com"]
        }
    </script>
    <!--#endregion-->
    <input type="hidden" id="DisPriceScenrioGTM" value="0.0">
    <input type="hidden" id="PercentScenrioGTM" value="0">
    <input type="hidden" id="PriceOriginGTM" value="23990000.0">
    <input type="hidden" id="DisPriceGTM" value="14990000.0">
    <input type="hidden" id="PercentGTM" value="37">
    <input type="hidden" id="productReviewVideoGTM" value="No">
    <input type="hidden" id="productAvailabilityGTM" value="No">
    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'pageType': 'Product',
            'pagePlatform': 'Web',
            'pageCategoryLv1': 'Điện thoại',
            'pageCategoryLv2': 'Điện thoại Samsung',
            'pageCategoryLv3': '',
            'pageStatus': 'Kinh doanh'
        })
    </script>
</section>
@stop