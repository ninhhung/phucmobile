@extends('admin::layouts.master')
@section('content')
<div class="page-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}" title="Trang chu">Trang chủ</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.get.list.category') }}" title="Danh muc">Danh mục</a></li>
        <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
    </ol>
</div>
<div class="">
    @include('admin::category.form')
</div>
@stop