<form action="" method="POST">@csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Tên danh mục</label>
        <input type="text" class="form-control" id="category_name" name="category_name" value="{{ old('category_name', isset($category->category_name) ? $category->category_name : '') }}" aria-describedby="category_name" placeholder="Danh muc ...">
        @if ($errors->has('category_name'))
        <div class="error-text">
            {{ $errors->first('category_name') }}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Icon</label>
        <input type="text" class="form-control" id="category_icon" name="category_icon" aria-describedby="category_icon" value="{{ old('category_icon', isset($category->category_icon) ? $category->category_icon : '') }}" placeholder="Icon ...">
        @if ($errors->has('category_icon'))
        <div class="error-text">
            {{ $errors->first('category_icon') }}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Tiêu đề danh mục</label>
        <input type="text" class="form-control" id="category_title_seo" name="category_title_seo" value="{{ old('category_title_seo', isset($category->category_title_seo) ? $category->category_title_seo : '') }}" aria-describedby="category_title_seo" placeholder="Tieu de ...">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Mô tả</label>
        <input type="text" class="form-control" id="category_description_seo" name="category_description_seo" value="{{ old('category_description_seo', isset($category->category_description_seo) ? $category->category_description_seo : '') }}" aria-describedby="category_description_seo" placeholder="Nhap mo ta ...">
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Nổi bật</label>
    </div>
    <button type="submit" class="btn btn-primary">Lưu</button>
</form>