@extends('admin::layouts.master')
@section('content')
<div class="page-header">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="#">Danh mục</a></li>
    <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
  </ol>
</div>
<div class="table-responsive">
  <h2>Quản lý danh mục <a href="{{ route('admin.get.create.category') }}" class="pull-right"><i class="fa fa-plus-circle"></i></a></h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Tên danh mục</th>
        <th>Tiêu đề</th>
        <th>Trạng thái</th>
        <th>Thao tác</th>
      </tr>
    </thead>
    <tbody>
      @if (!empty($categories))
      @foreach($categories as $category)
      <tr>
        <td>{{ $category->id }}</td>
        <td>{{ $category->category_name }}</td>
        <td>{{ $category->category_title_seo }}</td>
        <td>
          <a href="{{ route('admin.get.action.category', ['active', $category->id]) }}" class="label {{ $category->getStatus($category->status)['class'] }}">{{ $category->getStatus($category->status)['name'] }}</a>
        </td>
        <td>
          <a href="{{ route('admin.get.edit.category', $category->id) }}"><i class="fa fa-pen"></i></a> &nbsp;|&nbsp;
          <a href="{{ route('admin.get.action.category',['delete', $category->id]) }}"><i class="fa fa-trash-alt"></i></a>
        </td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>
@stop