@extends('admin::layouts.master')
@section('content')
<div class="page-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item"><a href="#">Đơn hàng</a></li>
        <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
    </ol>
</div>
<div class="table-responsive">
    <h2>Quản lý đơn hàng</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Mã đơn</th>
                <th>Tiêu đề</th>
                <th>Trạng thái</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@stop