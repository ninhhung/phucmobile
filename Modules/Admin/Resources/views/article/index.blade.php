@extends('admin::layouts.master')
@section('content')
<div class="page-header">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="#">Bài viết</a></li>
    <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
  </ol>
</div>
<div class="row">
  <div class="col-sm-12">
    <form class="form-inline" action="" style="margin-bottom: 20px">
      <div class="form-group">
        <input type="text" class="form-control" id="a_name" name="a_name" placeholder="Tên sản phẩm ..." value="{{ \Request::get('a_name') }}">
      </div>

      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
    </form>
  </div>
</div>
<div class="table-responsive">
  <h2>Quản lý sản phẩm <a href="{{ route('admin.get.create.article') }}" class="pull-right" title="Thêm mới"><i class="fa fa-plus-circle"></i></a></h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Tên bài viết</th>
        <th>Mô tả</th>
        <th>Trạng thái</th>
        <th>Ngày tạo</th>
        <th>Thao tác</th>
      </tr>
    </thead>
    <tbody>
      @if (isset($articles))
      @foreach($articles as $article)
      <tr>
        <td>{{ $article->id }}</td>
        <td>
          {{ $article->a_name }}
        </td>
        <td>{{ $article->a_description }}</td>
        <td>
          <a href="{{ route('admin.get.action.article', ['active', $article->id]) }}" class="label {{ $article->getStatus($article->a_status)['class'] }}">{{ $article->getStatus($article->a_status)['name'] }}</a>
        </td>
        <td>
          {{ $article->created_at }}
        </td>
        <td>
          <a href="{{ route('admin.get.edit.article', $article->id) }}" title="Sửa"><i class="fa fa-pen"></i></a> &nbsp;|&nbsp;
          <a href="{{ route('admin.get.action.article',['delete', $article->id]) }}" title="Xóa"><i class="fa fa-trash-alt"></i></a>
        </td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>
@stop