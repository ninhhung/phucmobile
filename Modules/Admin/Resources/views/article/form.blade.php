<form action="" method="POST">@csrf
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="form-group">
                <label for="a_name">Tên bài viết:</label>
                <input type="text" class="form-control" id="a_name" name="a_name" value="{{ old('a_name', isset($article->a_name) ? $article->a_name : '') }}" aria-describedby="a_name" placeholder="Nhập tên bài viết ...">
                @if ($errors->has('a_name'))
                <div class="error-text">
                    {{ $errors->first('a_name') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="a_description">Mô tả:</label>
                <textarea class="form-control" name="a_description" id="a_description" cols="30" rows="3" placeholder="Mô tả sản phẩm ...">{{ old('a_name', isset($article->a_description) ? $article->a_description : '') }}</textarea>
                @if ($errors->has('a_description'))
                <div class="error-text">
                    {{ $errors->first('a_description') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="a_content">Nội dung:</label>
                <textarea class="form-control" name="a_content" id="a_content" cols="30" rows="3" placeholder="Nội dung sản phẩm ...">{{ old('a_name', isset($article->a_content) ? $article->a_content : '') }}</textarea>
                @if ($errors->has('a_content'))
                <div class="error-text">
                    {{ $errors->first('a_content') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="a_title_seo">Tiêu đề</label>
                <input type="text" class="form-control" id="a_title_seo" name="a_title_seo" value="{{ old('a_title_seo', isset($article->a_title_seo) ? $article->a_title_seo : '') }}" aria-describedby="a_title_seo" placeholder="Nhap mo ta ...">
            </div>
            <div class="form-group">
                <label for="a_description_seo">Miêu tả</label>
                <input type="text" class="form-control" id="a_description_seo" name="a_description_seo" value="{{ old('a_description_seo', isset($article->a_description_seo) ? $article->a_description_seo : '') }}" aria-describedby="a_description_seo" placeholder="Nhap mo ta ...">
            </div>
            <button type="submit" class="btn btn-primary">Lưu</button>
        </div>
    </div>
</form>