@extends('admin::layouts.master')
@section('content')
<div class="page-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}" title="Trang chủ">Trang chủ</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.get.list.article') }}" title="Bài viết">Bài viết</a></li>
        <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
    </ol>
</div>
<div class="">
    @include('admin::article.form')
</div>
@stop