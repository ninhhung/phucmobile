<form action="" method="POST" enctype="multipart/form-data">@csrf
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="pro_name">Tên sản phẩm:</label>
                <input type="text" class="form-control" id="pro_name" name="pro_name" value="{{ old('pro_name', isset($product->pro_name) ? $product->pro_name : '') }}" aria-describedby="pro_name" placeholder="Nhập tên sản phẩm ...">
                @if ($errors->has('pro_name'))
                <div class="error-text">
                    {{ $errors->first('pro_name') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="pro_description">Mô tả:</label>
                <textarea class="form-control" name="pro_description" id="pro_description" cols="30" rows="3" placeholder="Mô tả sản phẩm ...">{{ old('pro_name', isset($product->pro_description) ? $product->pro_description : '') }}</textarea>
                @if ($errors->has('pro_description'))
                <div class="error-text">
                    {{ $errors->first('pro_description') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="pro_content">Nội dung:</label>
                <textarea class="form-control" name="pro_content" id="pro_content" cols="30" rows="3" placeholder="Nội dung sản phẩm ...">{{ old('pro_name', isset($product->pro_content) ? $product->pro_content : '') }}</textarea>
                @if ($errors->has('pro_content'))
                <div class="error-text">
                    {{ $errors->first('pro_content') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Meta title</label>
                <input type="text" class="form-control" id="pro_title_seo" name="pro_title_seo" value="{{ old('pro_title_seo', isset($product->pro_title_seo) ? $product->pro_title_seo : '') }}" aria-describedby="pro_title_seo" placeholder="Nhap mo ta ...">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Meta description</label>
                <input type="text" class="form-control" id="pro_description_seo" name="pro_description_seo" value="{{ old('pro_description_seo', isset($product->pro_description_seo) ? $product->pro_description_seo : '') }}" aria-describedby="pro_description_seo" placeholder="Nhap mo ta ...">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="pro_category_id">Loại sản phẩm:</label>
                <select name="pro_category_id" class="form-control" id="pro_category_id">
                    <option value="">Chọn loại sản phẩm</option>
                    @if (isset($categories))
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ old('pro_category_id', isset($product->pro_category_id) ? $product->pro_category_id : '') == $category->id ? "selected='selected'" : "" }}>{{$category->category_name}}</option>
                    @endforeach
                    @endif
                </select>
                @if ($errors->has('pro_category_id'))
                <div class="error-text">
                    {{ $errors->first('pro_category_id') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="pro_price">Giá sản phẩm:</label>
                <input type="number" class="form-control" placeholder="Giá sản phẩm ..." name="pro_price" id="pro_price" value="{{ old('pro_price', isset($product->pro_price) ? $product->pro_price : '') }}">
                @if ($errors->has('pro_price'))
                <div class="error-text">
                    {{ $errors->first('pro_price') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="pro_sale">Giá khuyến mãi:</label>
                <input type="number" class="form-control" placeholder="Giá khuyến mãi ..." name="pro_sale" id="pro_sale" value="{{ old('pro_sale', isset($product->pro_sale) ? $product->pro_sale : '') }}">
            </div>
            <div class="form-group">
                <img src="{{ pare_url_file($product->pro_image, 'products') }}" alt="Default image" class="img-form-add-edit" id="image_preview">
            </div>
            <div class="form-group">
                <label for="pro_image">Hình ảnh sản phẩm:</label>
                <input type="file" class="form-control" id="pro_image" name="pro_image" aria-describedby="pro_image" placeholder="">
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Nổi bật</label>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Lưu</button>
</form>
@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('pro_content');
</script>
@stop