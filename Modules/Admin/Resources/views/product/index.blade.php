@extends('admin::layouts.master')
@section('content')
<div class="page-header">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="#">Sản phẩm</a></li>
    <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
  </ol>
</div>
<div class="row">
  <div class="col-sm-12">
    <form class="form-inline" action="" style="margin-bottom: 20px">
      <div class="form-group">
        <input type="text" class="form-control" id="pro_name" name="pro_name" placeholder="Tên sản phẩmphẩm ..." value="{{ \Request::get('pro_name') }}">
      </div>
      <div class="form-group">
        <select name="cate" id="category_name" class="form-control">
          <option value="">Danh mục</option>
          @if (isset($categories))
          @foreach ($categories as $category)
          <option value="{{ $category->id }}" {{ \Request::get('cate') == $category->id ? "selected='selected'" : "" }}>{{$category->category_name}}</option>
          @endforeach
          @endif
        </select>
      </div>
      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
    </form>
  </div>
</div>
<div class="table-responsive">
  <h2>Quản lý sản phẩm <a href="{{ route('admin.get.create.product') }}" class="pull-right" title="Them moi"><i class="fa fa-plus-circle"></i></a></h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Tên sản phẩm</th>
        <th>Loại sản phẩm</th>
        <th>Hình ảnh sản phẩm</th>
        <th>Trạng thái</th>
        <th>Nổi bật</th>
        <th>Thao tác</th>
      </tr>
    </thead>
    <tbody>
      @if (isset($products))
      @foreach($products as $product)
      <tr>
        <td>{{ $product->id }}</td>
        <td>
          {{ $product->pro_name }}
          <ul style="padding-left:15px">
            <li><span><i class="fas fa-dollar-sign"></i> 12.000.000</span></li>
            <li><span><i class="fas fa-dollar-sign"></i> 0 %</span></li>
          </ul>
        </td>
        <td>{{ isset($product->category->category_name) ? $product->category->category_name : '[N\A]' }}</td>
        <td><img src="{{ pare_url_file($product->pro_image, 'products') }}" alt="Hinh anh san pham" class="pro_img_responsive"></td>
        <td>
          <a href="{{ route('admin.get.action.product', ['active', $product->id]) }}" class="label {{ $product->getStatus($product->pro_status)['class'] }}">{{ $product->getStatus($product->pro_status)['name'] }}</a>
        </td>
        <td>
          <a href="{{ route('admin.get.action.product', ['hot', $product->id]) }}" class="label {{ $product->getHot($product->pro_hot)['class'] }}">{{ $product->getHot($product->pro_hot)['name'] }}</a>
        </td>
        <td>
          <a href="{{ route('admin.get.edit.product', $product->id) }}" title="Sua"><i class="fa fa-pen"></i></a> &nbsp;|&nbsp;
          <a href="{{ route('admin.get.action.product',['delete', $product->id]) }}" title="Xoa"><i class="fa fa-trash-alt"></i></a>
        </td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>
@stop