@extends('admin::layouts.master')
@section('content')
<div class="page-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item"><a href="#">Thành viên</a></li>
        <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
    </ol>
</div>
<div class="table-responsive">
    <h2>Quản lý thành viên</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Tên thành viên</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Avatar</th>
                <th>Trạng thái</th>
                <th>Ngày tạo</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($users))
            @foreach ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->address }}</td>
                <td>{{ $user->avatar }}</td>
                <td>
                    <a href="{{ route('admin.get.action.user', ['active', $user->id]) }}" class="label {{ $user->getStatus($user->status)['class'] }}">{{ $user->getStatus($user->status)['name'] }}</a>
                </td>
                <td>{{ $user->created_at }}</td>
                <td>
                    <a href=""><i class="fa fa-pen"></i></a> &nbsp;|&nbsp;
                    <a href=""><i class="fa fa-trash-alt"></i></a>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
@stop