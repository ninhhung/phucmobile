<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\RequestProduct;
use Illuminate\Contracts\Support\Renderable;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $products = Product::with('category:id,category_name');
        if ($request->pro_name) $products->where('pro_name', 'like', '%' . $request->pro_name . '%');
        if ($request->cate) $products->where('pro_category_id', $request->cate);
        $products = $products->orderByDesc('id')->paginate(10);
        $categories = $this->getCategories();
        $viewData = [
            'products' => $products,
            'categories' => $categories,
        ];
        return view('admin::product.index', $viewData);
    }
    public function create()
    {
        $categories = $this->getCategories();
        return view('admin::product.create', compact('categories'));
    }

    public function store(RequestProduct $request)
    {
        $this->inserOrUpdate($request);
        return redirect()->back();
    }

    public function getCategories()
    {
        return Category::all();
    }

    public function inserOrUpdate($requestProduct, $id = '')
    {
        $product = new Product();

        if ($id) $product = Product::find($id);
        $product->pro_name              = $requestProduct->pro_name;
        $product->pro_slug              = str_slug($requestProduct->pro_name);
        $product->pro_description       = $requestProduct->pro_description;
        $product->pro_content           = $requestProduct->pro_content;
        $product->pro_title_seo         = $requestProduct->pro_title_seo ? $requestProduct->pro_title_seo : $requestProduct->pro_name;
        $product->pro_description_seo   = $requestProduct->pro_description_seo ? $requestProduct->pro_description_seo : $requestProduct->pro_name;
        $product->pro_category_id       = $requestProduct->pro_category_id;
        $product->pro_price             = $requestProduct->pro_price;
        $product->pro_sale              = $requestProduct->pro_sale;

        if ($requestProduct->hasFile('pro_image')) {
            $file = upload_image('pro_image', 'products');
            if (isset($file['name'])) {
                $product->pro_image = $file['name'];
            }
        }
        $product->save();
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = $this->getCategories();
        return view('admin::product.update', compact('product', 'categories'));
    }

    public function update(RequestProduct $requestProduct, $id)
    {
        $this->inserOrUpdate($requestProduct, $id);
        return redirect()->back();
    }

    public function action(Request $request, $action, $id)
    {
        if ($action) {
            $product = Product::find($id);
            switch ($action) {
                case 'delete':
                    $product->delete();
                    break;

                case 'active':
                    $product->pro_status = !($product->pro_status);
                    $product->save();
                    break;

                case 'hot':
                    $product->pro_hot = !($product->pro_hot);
                    $product->save();
                    break;

                default:
                    # code...
                    break;
            }
        }
        return redirect()->back();
    }
}
