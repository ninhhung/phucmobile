<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Support\Renderable;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $users = User::whereRaw(1);
        $users = $users->orderBy('id', 'DESC')->paginate(10);
        $viewData = [
            'users' => $users
        ];
        return view('admin::user.index', $viewData);
    }

    public function action(Request $request, $action, $id)
    {
        if ($action) {
            $user = User::find($id);
            switch ($action) {
                case 'delete':
                    $user->delete();
                    break;

                case 'active':
                    $user->status = !($user->status);
                    $user->save();
                    break;
                default:
                    # code...
                    break;
            }
        }
        return redirect()->back();
    }
}
