<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\RequestArticle;
use Illuminate\Contracts\Support\Renderable;

class AdminArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $articles = Article::whereRaw(1);
        if ($request->a_name) $articles->where('a_name','like','%'.$request->a_name.'%');
        $articles = $articles->paginate(10);
        $viewData = [
            'articles' => $articles
        ];
        return view('admin::article.index', $viewData);
    }

    public function create()
    {
        return view('admin::article.create');
    }

    public function store(RequestArticle $request)
    {
        $this->insertOrUpdate($request);
        return redirect()->back();
    }

    public function edit($id)
    {
        $article = Article::find($id);
        return view('admin::article.update', compact('article'));
    }

    public function update(RequestArticle $request, $id)
    {
        $this->insertOrUpdate($request, $id);
        return redirect()->back();
    }

    public function insertOrUpdate($requestArticle, $id='') {
        $article = new Article();

        if ($id) $article            = Article::find($id);
        $article->a_name             = $requestArticle->a_name;
        $article->a_slug             = str_slug($requestArticle->a_name);
        $article->a_description      = $requestArticle->a_description;
        $article->a_content          = $requestArticle->a_content;
        $article->a_title_seo        = $requestArticle->a_title_seo ? $requestArticle->a_title_seo : $requestArticle->a_name;
        $article->a_description_seo  = $requestArticle->a_description_seo ? $requestArticle->a_description_seo : $requestArticle->a_description;
        $article->save();
    }

    public function action(Request $request, $action, $id) {
        if ($action) {
            $article = Article::find($id);
            switch ($action) {
                case 'delete':
                    $article->delete();
                    break;

                case 'active':
                    $article->a_status = !($article->a_status);
                    $article->save();
                    break;

                case 'hot':
                    $article->a_hot = !($article->a_hot);
                    $article->save();
                    break;
                
                default:
                    # code...
                    break;
            }
        }
        return redirect()->back();
    }
}
