<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\RequestCategory;
use Illuminate\Contracts\Support\Renderable;

class AdminCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $categories = Category::select('id', 'category_name', 'category_title_seo', 'status')->get();
        $viewData = [
            'categories' => $categories
        ];
        return view('admin::category.index', $viewData);
    }

    public function create()
    {
        return view('admin::category.create');
    }

    public function store(RequestCategory $requestCategory)
    {
        $this->inserOrUpdate($requestCategory);
        return redirect()->back();
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin::category.update', compact('category'));
    }

    public function update(RequestCategory $request, $id)
    {
        $this->inserOrUpdate($request, $id);
        return redirect()->back();
    }

    public function inserOrUpdate($requestCategory, $id = '')
    {
        $code = 1;
        try {
            $category = new Category();
            if ($id) {
                $category = Category::find($id);
            }
            $category->category_name = $requestCategory->category_name;
            $category->category_slug = str_slug($requestCategory->category_name);
            $category->category_icon = $requestCategory->category_icon;
            $category->category_title_seo = $requestCategory->category_title_seo ? $requestCategory->category_title_seo : $requestCategory->category_name;
            $category->category_description_seo = $requestCategory->category_description_seo;
            $category->save();
        } catch (\Exception $e) {
            $code = 0;
            Log::error('[Error: insertOrUpdate Category]' . $e->getMessage());
        }
        return $code;
    }

    public function action(Request $request, $action, $id)
    {
        if ($action) {
            $category = Category::find($id);
            switch ($action) {
                case 'delete':
                    $category->delete();
                    break;
                case 'active':
                    $category->status = !($category->status);
                    $category->save();
                    break;

                default:
                    # code...
                    break;
            }
        }
        return redirect()->back();
    }
}
