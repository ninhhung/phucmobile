<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['namespace' => 'App\Http\Controllers\Auth'], function () {
    Route::get('/dang-ky', 'RegisterController@getRegister')->name('get.register');
    Route::post('/dang-ky', 'RegisterController@postRegister')->name('post.register');

    Route::get('/dang-nhap', 'LoginController@getLogin')->name('get.login');
    Route::post('/dang-nhap', 'LoginController@postLogin')->name('post.login');

    Route::get('/dang-xuat', 'LoginController@getLogout')->name('get.logout.user');
});

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('danh-muc/{slug}-{id}', 'App\Http\Controllers\CategoryController@getListProduct')->name('get.list.product');
Route::get('san-pham/{slug}-{id}', 'App\Http\Controllers\ProductDetailController@productDetail')->name('get.detail.product');

Route::prefix('shopping', function () {
    Route::get('/add/{id}', 'ShoppingCartController@addProduct')->name('add.shopping.cart');
});
