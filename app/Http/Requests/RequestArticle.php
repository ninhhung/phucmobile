<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'a_name' => 'required|unique:articles,a_name,'.$this->id,
            'a_description' => 'required',
            'a_content' => 'required',

        ];
    }

    public function messages() {
        return [
            'a_name.required' => 'Ten bai viet khong duoc de trong',
            'a_name.unique' => 'Ten bai viet da ton tai',
            'a_description.required' => 'Mo ta khong duoc de trong',
            'a_content.required' => 'Noi dung bai viet khong duoc de trong',
        ];
    }
}
