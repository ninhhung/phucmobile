<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pro_name' => 'required|unique:products,pro_name,'.$this->id,
            'pro_category_id' => 'required',
            'pro_description' => 'required',
            'pro_price' => 'required',
            'pro_content' => 'required',

        ];
    }

    public function messages() {
        return [
            'pro_name.required' => 'Ten san pham khong duoc de trong',
            'pro_name.unique' => 'Ten san pham da ton tai',
            'pro_description.required' => 'Mo ta khong duoc de trong',
            'pro_category_id.required' => 'Loai san pham khong duoc de trong',
            'pro_price.required' => 'Gia san pham khong duoc de trong',
            'pro_content.required' => 'Noi dung san pham khong duoc de trong',
        ];
    }
}
