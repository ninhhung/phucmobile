<?php

namespace App\Http\Controllers;

use App\Models\Product;

class HomeController extends FrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hotProduct = Product::where([
            'pro_hot'       => Product::HOT_ON,
            'pro_status'        => Product::STATUS_PUBLIC
        ])->limit(5)->get();
        $viewData = [
            'productsHot'   => $hotProduct
        ];
        return view('home.index', $viewData);
    }
}
