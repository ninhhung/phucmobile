<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    const STATUS_PUBLIC = 1;
    const STATUS_PRIVATE = 0;
    const HOT_ON = 1;
    const HOT_OFF = 0;

    public $statusArr = [
        1 => [
            'name' => 'Enable',
            'class' => 'label-success'
        ],
        0 => [
            'name' => 'Disable',
            'class' => 'label-danger'
        ]
    ];

    public $hot = [
        1 => [
            'name' => 'Noi bat',
            'class' => 'label-success'
        ],
        0 => [
            'name' => 'Khong',
            'class' => 'label-danger'
        ]
    ];

    public function getStatus() {
        return array_get($this->statusArr, $this->pro_status, '[N\A]');
    }

    public function getHot() {
        return array_get($this->hot, $this->pro_hot, '[N\A]');
    }

    public function category() {
        return $this->belongsTo(Category::class, 'pro_category_id');
    }
}
