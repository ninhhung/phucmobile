<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [''];
    const STATUS_PUBLIC = 1;
    const STATUS_PRIVATE = 0;

    public $statusArr = [
        1 => [
            'name' => 'Enable',
            'class' => 'label-success'
        ],
        0 => [
            'name' => 'Disable',
            'class' => 'label-danger'
        ]
    ];

    public function getStatus() {
        return array_get($this->statusArr, $this->status, '[N\A]');
    }
}
