<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    public $hot = [
        1 => [
            'name' => 'Noi bat',
            'class' => 'label-success'
        ],
        0 => [
            'name' => 'Khong',
            'class' => 'label-danger'
        ]
    ];

    public function getStatus() {
        return array_get($this->hot, $this->a_status, '[N\A]');
    }
}
